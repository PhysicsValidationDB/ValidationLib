/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author wenzel
 */
public class ResultUnitTest {

    public ResultUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     *
     * @throws IOException
     */
    @Test
    public void testCreateResult() throws IOException {
        /*
        JsonReader reader = new JsonReader(new FileReader("/home/wenzel/NetBeansProjects/ValidationLib/src/test/java/ValidationLib/hr.json"));
        Result result = Result.create(reader);
        System.out.println("+++++++++++++++++++++++++++++++++++++++");
        System.out.println(result.toJSON());
        System.out.println("+++++++++++++++++++++++++++++++++++++++" + result.getMctoollnk() + "   " + result.getMctoolkw());
         */
        DictionaryService ds = DictionaryService.getInstance();

        //JsonReader reader2 = new JsonReader(new FileReader("/home/wenzel/NetBeansProjects/ValidationLib/src/test/java/ValidationLib/scxcs_1.json"));
        //JsonReader reader2 = new JsonReader(new InputStreamReader(uploadedFile.getInputstream()))) 
        InputStream inputStream = new FileInputStream("/home/wenzel/NetBeansProjects/ValidationLib/src/test/java/ValidationLib/scxcs_1.json");
        //Reader inputStreamReader = new InputStreamReader(inputStream);
        JsonReader reader2 = new JsonReader(new InputStreamReader(inputStream));
        reader2.beginArray();
        int resultNum2 = 0;
        while (reader2.hasNext()) {
            Gson gson = new GsonBuilder().create();
            Result result2 = null;
            try {
                result2 = Result.create(reader2);
                System.out.println("*************************************************************************************************");
                System.out.println(result2.toJSON());
                System.out.println("*************************************************************************************************");
                //resultAdapter.insert(result);
            } catch (JsonSyntaxException e) {
                System.out.println("********************" + e.toString());
            }
            resultNum2++;
        }
        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&:  "+resultNum2);
//        Result result2 = Result.create(reader2);
        // System.out.println("+++++++++++++++++++++++++++++++++++++++");
        // System.out.println(result2.toJSON());

        /*
        JsonReader reader3 = new JsonReader(new FileReader("/home/wenzel/NetBeansProjects/ValidationLib/src/test/java/ValidationLib/10.4/HARP-protonBe12.0GeV-bertini.json"));
        reader3.beginArray();
        int resultNum = 0;
        while (reader3.hasNext()) {
            Gson gson = new GsonBuilder().create();
            Result result3 = null;
            try {
                result3 = Result.create(reader3);
                System.out.println("*************************************************************************************************");
                System.out.println(result3.toJSON());
                System.out.println("*************************************************************************************************");
                //resultAdapter.insert(result);
            } catch (JsonSyntaxException e) {
                System.out.println("********************" + e.toString());
            }

            // Read data into object model
            resultNum++;
            try {
                //  Result result = Result.create(reader);
            } catch (IllegalArgumentException e) {
                System.out.println(e.toString());
                System.out.println(resultNum);
            }
        }
         */
    }

}
