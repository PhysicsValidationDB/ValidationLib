/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class ParticleServiceAdapterImpl extends ServiceAdapter implements ParticleServiceAdapter {
    private static ParticleServiceAdapterImpl instance;

    /**
     *
     */
    private ParticleServiceAdapterImpl() {
    }

    public static ParticleServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new ParticleServiceAdapterImpl();
        }
        return instance;
    }
    /**
     *
     * @param id
     * @return
     */
    @Override
    public Particle getById(Integer id) {
        String sql = "SELECT PNAME FROM PUBLIC.PARTICLE WHERE PDGID=? ;";
        PreparedStatement ps;
        Particle dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Particle.create(id,  result.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParticleServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Particle> getAll() {

        String sql = "SELECT PDGID,PNAME FROM PUBLIC.PARTICLE ORDER BY PDGID;";
        List<Particle> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                list.add(Particle.create(result.getInt(1), result.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParticleServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    /**
     *
     * @param dt
     * @return
     */
    @Override
    public int insert(Particle dt) {
        int executeUpdate = 0;
        try {
            String sql = "INSERT INTO PUBLIC.PARTICLE (PDGID,PNAME) VALUES (?,?);";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getPdgid());
            ps.setString(2, dt.getPname());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ParticleServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    /**
     *
     * @param dt
     * @return
     */
    @Override
    public int delete(Particle dt) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.PARTICLE WHERE PDGID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getPdgid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ParticleServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

}
