/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.util.List;

/**
 *
 * @author wenzel
 */
public interface ReferenceServiceAdapter {

    /**
     *
     * @param id
     * @return
     */
    public Reference getById(Integer id);

    /**
     *
     * @param inspireid
     * @return
     */
    public Reference getByInspireId(Integer inspireid);

    /**
     * @return
     */
    public List<Reference> getAll();

    /**
     *
     * @param refs
     */
    public void insertReference(Reference refs);

    /**
     *
     * @param refs
     */
    public void updateReference(Reference refs);

    /**
     *
     * @param refs
     */
    public void deleteReference(Reference refs);

    /**
     *
     * returns the number of result sets associated with this references
     *
     * @return 
     */
    public Integer NrOfResultSets(Reference refs);
}
