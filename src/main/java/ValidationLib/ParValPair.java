/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */

 /*

 */
package ValidationLib;

import java.util.Objects;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 *
 * @author wenzel
 */
public class ParValPair implements Comparable<ParValPair> {

    private String ParName;
    private String ParValue;

    public ParValPair(String n, String v) {
        this.ParName = n;
        this.ParValue = v;
    }

    /**
     * @return the ParName
     */
    public String getParName() {
        return ParName;
    }

    /**
     * @param ParName the ParName to set
     */
    public void setParName(String ParName) {
        this.ParName = ParName;
    }

    /**
     * @return the ParValue
     */
    public String getParValue() {
        return ParValue;
    }

    /**
     * @param ParValue the ParValue to set
     */
    public void setParValue(String ParValue) {
        this.ParValue = ParValue;
    }

    /*
    @Override
    public int compareTo(ParValPair other) {
      int name = this.ParName.compareTo(other.ParName);
        return name == 0 ? this.ParValue.compareTo(other.ParValue) : name;
    }
     */
    @Override
    public int hashCode() {
           return new HashCodeBuilder(17, 37)
                .append(ParName)
                .append(ParValue)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ParValPair other = (ParValPair) obj;
        if (!Objects.equals(this.ParName, other.ParName)) {
            return false;
        }
        if (!Objects.equals(this.ParValue, other.ParValue)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(ParValPair o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
