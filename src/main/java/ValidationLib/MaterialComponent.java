/*

 */
package ValidationLib;

/**
 *
 * @author wenzel
 */
public class MaterialComponent {
    private Integer ElementID;
    private float fraction; 

    /**
     *
     */
    public MaterialComponent() {
    }

    /**
     *
     * @param ElementID
     * @param fraction
     */
    public MaterialComponent(Integer ElementID , float fraction) {
        this.ElementID = ElementID;
        this.fraction = fraction;
    }

    /**
     * @return the fraction
     */
    public float getFraction() {
        return fraction;
    }

    /**
     * @param fraction the fraction to set
     */
    public void setFraction(float fraction) {
        this.fraction = fraction;
    }

    /**
     * @return the ElementID
     */
    public Integer getElementID() {
        return ElementID;
    }

    /**
     * @param ElementID the ElementID to set
     */
    public void setElementID(Integer ElementID) {
        this.ElementID = ElementID;
    }
}
