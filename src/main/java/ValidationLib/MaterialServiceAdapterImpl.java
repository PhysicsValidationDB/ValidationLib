/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class MaterialServiceAdapterImpl extends ServiceAdapter implements MaterialServiceAdapter {
    private static MaterialServiceAdapterImpl instance;

    /**
     *
     */
    private MaterialServiceAdapterImpl() {
    }

    public static MaterialServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new MaterialServiceAdapterImpl();
        }
        return instance;
    }
    @Override
    public Material getById(Integer id) {
        String sql = "SELECT Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC FROM PUBLIC.MATERIAL WHERE MID=? ;";
        PreparedStatement ps;
        Material mat = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                Integer Z = result.getInt(1);
                String Materialname = result.getString(2);
                float Density = result.getFloat(3);
                float Ion = result.getFloat(4);
                Integer NComp = result.getInt(5);
                if (NComp == 0 || NComp == null) // we are dealing with an element not a compound!! 
                {
                    mat = Material.create(id, Z, Materialname, Density, Ion);
                } else // we are dealing with a compound material
                {
                    Array cmps = result.getArray(6);
                    Integer[] zips = (Integer[]) cmps.getArray();
                    Array frcs = result.getArray(7);
                    Float[] frs = (Float[]) frcs.getArray();
                    List<MaterialComponent> Components = new ArrayList<>();
                    for (int ii = 1; ii < zips.length; ii++) {
                        Components.add(new MaterialComponent(zips[ii], frs[ii]));
                    }
                    mat = Material.create(id, Z, Materialname, Density, Ion, Components);
                }
            }                                                   // Loop over result set
        } catch (SQLException ex) {
            Logger.getLogger(MaterialServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return mat;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Material> getAll() {

        String sql = "SELECT MID,Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC FROM PUBLIC.MATERIAL ORDER BY MID;";
        Material mat;
        List<Material> materiallist = new ArrayList<>();

        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                Integer mid = result.getInt(1);
                Integer Z = result.getInt(2);
                String Materialname = result.getString(3);
                float Density = result.getFloat(4);
                float Ion = result.getFloat(5);
                Integer NComp = result.getInt(6);
                if (NComp == 0 || NComp == null) // we are dealing with an element not a compound!! 
                {
                    mat = Material.create(mid, Z, Materialname, Density, Ion);
                } else // we are dealing with a compound material
                {
                    Array cmps = result.getArray(7);
                    Integer[] zips = (Integer[]) cmps.getArray();
                    Array frcs = result.getArray(8);
                    Float[] frs = (Float[]) frcs.getArray();
                    List<MaterialComponent> Components = new ArrayList<>();
                    for (int ii = 0; ii < zips.length; ii++) {
                        Components.add(new MaterialComponent(zips[ii], frs[ii]));
                    }
                    mat = Material.create(mid, Z, Materialname, Density, Ion, Components);
                }
                materiallist.add(mat);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MaterialServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return materiallist;
    }
    /**
     *
     * @return
     */
    @Override
    public int delete(Material dt) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.MATERIAL WHERE MID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getMid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MaterialServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int insert(Material dt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int update(Material dt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
