/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class AccessServiceAdapterImpl extends ServiceAdapter implements AccessServiceAdapter {
    private static AccessServiceAdapterImpl instance;

    /**
     *
     */
    private AccessServiceAdapterImpl() {
    }

    public static AccessServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new AccessServiceAdapterImpl();
        }
        return instance;
    }
    @Override
    public Access getById(Integer id) {
        String sql = "SELECT ACCESS FROM PUBLIC.ACCESS WHERE ACCESSID=? ;";
        PreparedStatement ps;
        Access dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Access.create(id, result.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccessServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Access> getAll() {

        String sql = "SELECT ACCESSID,ACCESS FROM PUBLIC.ACCESS;";
        List<Access> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                list.add(Access.create(result.getInt(1), result.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccessServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    @Override
    public int insert(Access dt) {
        int executeUpdate = 0;
        try {
            String sql = "INSERT INTO PUBLIC.ACCESS (ACCESSID,ACCESS) VALUES (?,?);";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getAccessid());
            ps.setString(2, dt.getAccess());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccessServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int update(Access dt) {
        int executeUpdate = 0;
        try {
            String sql = "UPDATE PUBLIC.ACCESS SET ACCESS=? WHERE ACCESSID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getAccess());
            ps.setInt(2, dt.getAccessid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccessServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int delete(Access dt) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.ACCESS WHERE ACCESSID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getAccessid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccessServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

}
