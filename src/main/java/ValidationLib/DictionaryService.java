
/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.io.IOUtils;

/**
 * Class providing a service to build lookup tables from the database
 * dictionaries.
 *
 * @author ncserpico
 */
public class DictionaryService extends ServiceAdapter {

    private static DictionaryService instance;
    private static BidiMap<String, Integer> ParticleMap;
    private static BidiMap<String, Integer> BeamMap;
    private static BidiMap<String, Integer> MaterialMap;
    private static BidiMap<String, Integer> MctoolMap;
    private static BidiMap<String, Integer> ScoresMap;
    private static BidiMap<String, Integer> ObservableMap;
    private static BidiMap<String, Integer> AccessMap;
    private static BidiMap<String, Integer> ModelMap;
    private static BidiMap<String, Integer> VersiontagMap;
    private static BidiMap<String, Integer> ReactionMap;
    private static BidiMap<String, Integer> TestMap;
    private static BidiMap<String, Integer> DatatypesMap;
    private static BidiMap<String, Integer> ReferenceMap;

    private DictionaryService() throws IOException {
        DBConnection DBConnectionInstance = DBConnection.getInstance();
        if (DBConnection.getPooled()) {
            this.Init();   // direct connection to database is available 
        } else // otherwise try to build lookup tables from RESTful web service 
        {
            String url = "https://g4devel.fnal.gov:8181/WebAPI/dictionary?name=";
            this.Init(url);
        }
    }

    public static DictionaryService getInstance() throws IOException {
        if (instance == null) {
            instance = new DictionaryService();
        }
        return instance;
    }

    private void Init(String url) throws MalformedURLException, IOException {
        ObservableMap = new DualHashBidiMap<String, Integer>();
        AccessMap = new DualHashBidiMap<String, Integer>();
        ReactionMap = new DualHashBidiMap<String, Integer>();
        ParticleMap = new DualHashBidiMap<String, Integer>();
        BeamMap = new DualHashBidiMap<String, Integer>();
        MaterialMap = new DualHashBidiMap<String, Integer>();
        MctoolMap = new DualHashBidiMap<String, Integer>();
        ScoresMap = new DualHashBidiMap<String, Integer>();
        ModelMap = new DualHashBidiMap<String, Integer>();
        VersiontagMap = new DualHashBidiMap<String, Integer>();
        TestMap = new DualHashBidiMap<String, Integer>();
        DatatypesMap = new DualHashBidiMap<String, Integer>();
        ReferenceMap = new DualHashBidiMap<String, Integer>();

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

        String urlref = url + "Reference";
        URL obj = new URL(urlref);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        String USER_AGENT = "Mozilla/5.0";
        con.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = con.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(con.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Reference>>() {
            }.getType();
            List<Reference> refs = gson.fromJson(theString, ListType);
            con.disconnect();
            System.out.println("Refs size: " + refs.size());
            for (Reference element : refs) {
                if (element.getInspireid() != 0) {
                    ReferenceMap.put(String.valueOf(element.getInspireid()), element.getRefid());

                } else {
                    ReferenceMap.put(element.getLinkurl(), element.getRefid());
                }
            }
        }
        con.disconnect();
        Iterator it = ReferenceMap.entrySet().iterator();
        System.out.println("inspire reference = referenceid:");
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }

        String urlobs = url + "Observable";
        URL objobs = new URL(urlobs);
        HttpsURLConnection conobs = (HttpsURLConnection) objobs.openConnection();
        conobs.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conobs.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conobs.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conobs.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Observable>>() {
            }.getType();
            List<Observable> obs = gson.fromJson(theString, ListType);
            conobs.disconnect();
            System.out.println("Observables size: " + obs.size());
            for (Observable element : obs) {
                ObservableMap.put(element.getOname(), element.getOid());
            }
        }
        conobs.disconnect();
        Iterator obsit = ObservableMap.entrySet().iterator();
        while (obsit.hasNext()) {
            Map.Entry pair = (Map.Entry) obsit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }

        String urlacc = url + "Access";
        URL objacc = new URL(urlacc);
        HttpsURLConnection conacc = (HttpsURLConnection) objacc.openConnection();
        conacc.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conacc.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conacc.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conacc.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Access>>() {
            }.getType();
            List<Access> obs = gson.fromJson(theString, ListType);
            conacc.disconnect();
            System.out.println("Accesss size: " + obs.size());
            for (Access element : obs) {
                AccessMap.put(element.getAccess(), element.getAccessid());
            }
        }
        conacc.disconnect();
        Iterator accit = AccessMap.entrySet().iterator();
        while (accit.hasNext()) {
            Map.Entry pair = (Map.Entry) accit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
        String urlreac = url + "Reaction";
        URL objreac = new URL(urlreac);
        HttpsURLConnection conreac = (HttpsURLConnection) objreac.openConnection();
        conreac.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conreac.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conreac.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conreac.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Reaction>>() {
            }.getType();
            List<Reaction> obs = gson.fromJson(theString, ListType);
            conreac.disconnect();
            System.out.println("Reactions size: " + obs.size());
            for (Reaction element : obs) {
                ReactionMap.put(element.getRname(), element.getRid());
            }
        }
        conreac.disconnect();
        Iterator reacit = ReactionMap.entrySet().iterator();
        while (reacit.hasNext()) {
            Map.Entry pair = (Map.Entry) reacit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }

        String urlmat = url + "Material";
        URL objmat = new URL(urlmat);
        HttpsURLConnection conmat = (HttpsURLConnection) objmat.openConnection();
        conmat.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conmat.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conmat.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conmat.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Material>>() {
            }.getType();
            List<Material> obs = gson.fromJson(theString, ListType);
            conmat.disconnect();
            System.out.println("Materials size: " + obs.size());
            for (Material element : obs) {
                MaterialMap.put(element.getMname(), element.getMid());
            }
        }
        conmat.disconnect();
        Iterator matit = MaterialMap.entrySet().iterator();
        while (matit.hasNext()) {
            Map.Entry pair = (Map.Entry) matit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }

        String urlBeam = url + "Beam";
        URL objBeam = new URL(urlBeam);
        HttpsURLConnection conBeam = (HttpsURLConnection) objBeam.openConnection();
        conBeam.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conBeam.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conBeam.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conBeam.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Beam>>() {
            }.getType();
            List<Beam> obs = gson.fromJson(theString, ListType);
            conBeam.disconnect();
            System.out.println("Beams size: " + obs.size());
            for (Beam element : obs) {
                BeamMap.put(element.getBname(), element.getBid());
            }
        }
        conBeam.disconnect();
        Iterator Beamit = BeamMap.entrySet().iterator();
        while (Beamit.hasNext()) {
            Map.Entry pair = (Map.Entry) Beamit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
        String urlParticle = url + "Particle";
        URL objParticle = new URL(urlParticle);
        HttpsURLConnection conParticle = (HttpsURLConnection) objParticle.openConnection();
        conParticle.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conParticle.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conParticle.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conParticle.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Particle>>() {
            }.getType();
            List<Particle> obs = gson.fromJson(theString, ListType);
            conParticle.disconnect();
            System.out.println("Particles size: " + obs.size());
            for (Particle element : obs) {
                ParticleMap.put(element.getPname(), element.getPdgid());
            }
        }
        conParticle.disconnect();
        Iterator Particleit = ParticleMap.entrySet().iterator();
        while (Particleit.hasNext()) {
            Map.Entry pair = (Map.Entry) Particleit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
        String urlMctool = url + "Mctool";
        URL objMctool = new URL(urlMctool);
        HttpsURLConnection conMctool = (HttpsURLConnection) objMctool.openConnection();
        conMctool.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conMctool.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conMctool.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conMctool.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Mctool>>() {
            }.getType();
            List<Mctool> obs = gson.fromJson(theString, ListType);
            conMctool.disconnect();
            System.out.println("Mctools size: " + obs.size());
            for (Mctool element : obs) {
                MctoolMap.put(element.getMcname(), element.getMcid());
            }
        }
        conMctool.disconnect();
        Iterator Mctoolit = MctoolMap.entrySet().iterator();
        while (Mctoolit.hasNext()) {
            Map.Entry pair = (Map.Entry) Mctoolit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
        String urlModel = url + "Model";
        URL objModel = new URL(urlModel);
        HttpsURLConnection conModel = (HttpsURLConnection) objModel.openConnection();
        conModel.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conModel.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conModel.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conModel.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Model>>() {
            }.getType();
            List<Model> obs = gson.fromJson(theString, ListType);
            conModel.disconnect();
            System.out.println("Models size: " + obs.size());
            for (Model element : obs) {
                ModelMap.put(element.getModelname(), element.getModelid());
            }
        }
        conModel.disconnect();
        Iterator Modelit = ModelMap.entrySet().iterator();
        while (Modelit.hasNext()) {
            Map.Entry pair = (Map.Entry) Modelit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
        String urlScores = url + "Scores";
        URL objScores = new URL(urlScores);
        HttpsURLConnection conScores = (HttpsURLConnection) objScores.openConnection();
        conScores.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conScores.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conScores.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conScores.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Scores>>() {
            }.getType();
            List<Scores> obs = gson.fromJson(theString, ListType);
            conScores.disconnect();
            System.out.println("Scoress size: " + obs.size());
            for (Scores element : obs) {
                ScoresMap.put(element.getScore(), element.getScoreid());
            }
        }
        conScores.disconnect();
        Iterator Scoresit = ScoresMap.entrySet().iterator();
        while (Scoresit.hasNext()) {
            Map.Entry pair = (Map.Entry) Scoresit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }

        String urlVersiontag = url + "Versiontag";
        System.out.println(urlVersiontag);
        URL objVersiontag = new URL(urlVersiontag);
        HttpsURLConnection conVersiontag = (HttpsURLConnection) objVersiontag.openConnection();
        conVersiontag.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conVersiontag.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conVersiontag.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conVersiontag.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Versiontag>>() {
            }.getType();
            List<Versiontag> obs = gson.fromJson(theString, ListType);
            conVersiontag.disconnect();
            System.out.println("Versiontags size: " + obs.size());
            for (Versiontag element : obs) {
                VersiontagMap.put(element.getTag(), element.getVersionId());
            }
        }
        conVersiontag.disconnect();
        Iterator Versiontagit = VersiontagMap.entrySet().iterator();
        while (Versiontagit.hasNext()) {
            Map.Entry pair = (Map.Entry) Versiontagit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }

        String urlTest = url + "Test";
        URL objTest = new URL(urlTest);
        HttpsURLConnection conTest = (HttpsURLConnection) objTest.openConnection();
        conTest.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conTest.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conTest.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conTest.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Test>>() {
            }.getType();
            List<Test> obs = gson.fromJson(theString, ListType);
            conTest.disconnect();
            System.out.println("Tests size: " + obs.size());
            for (Test element : obs) {
                TestMap.put(element.getTestname(), element.getTestid());
            }
        }
        conTest.disconnect();
        Iterator Testit = TestMap.entrySet().iterator();
        while (Testit.hasNext()) {
            Map.Entry pair = (Map.Entry) Testit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }

        String urlDatatypes = url + "Datatypes";
        URL objDatatypes = new URL(urlDatatypes);
        HttpsURLConnection conDatatypes = (HttpsURLConnection) objDatatypes.openConnection();
        conDatatypes.setRequestMethod("GET");
        //String USER_AGENT = "Mozilla/5.0";
        conDatatypes.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conDatatypes.getResponseCode();
        if (responseCode == 200) {
            String theString = IOUtils.toString(conDatatypes.getInputStream(), "UTF-8");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                    .create();
            java.lang.reflect.Type ListType = new TypeToken<ArrayList<Datatypes>>() {
            }.getType();
            List<Datatypes> obs = gson.fromJson(theString, ListType);
            conDatatypes.disconnect();
            System.out.println("Datatypess size: " + obs.size());
            for (Datatypes element : obs) {
                DatatypesMap.put(element.getDescription(), element.getDtype());
            }
        }
        conDatatypes.disconnect();
        Iterator Datatypesit = DatatypesMap.entrySet().iterator();
        while (Datatypesit.hasNext()) {
            Map.Entry pair = (Map.Entry) Datatypesit.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
    }

    private void Init() {
        ObservableMap = new DualHashBidiMap<String, Integer>();
        AccessMap = new DualHashBidiMap<String, Integer>();
        ReactionMap = new DualHashBidiMap<String, Integer>();
        ParticleMap = new DualHashBidiMap<String, Integer>();
        BeamMap = new DualHashBidiMap<String, Integer>();
        MaterialMap = new DualHashBidiMap<String, Integer>();
        MctoolMap = new DualHashBidiMap<String, Integer>();
        ScoresMap = new DualHashBidiMap<String, Integer>();
        ModelMap = new DualHashBidiMap<String, Integer>();
        VersiontagMap = new DualHashBidiMap<String, Integer>();
        TestMap = new DualHashBidiMap<String, Integer>();
        DatatypesMap = new DualHashBidiMap<String, Integer>();
        ReferenceMap = new DualHashBidiMap<String, Integer>();
        //
        ObservableServiceAdapterImpl osa = ObservableServiceAdapterImpl.getInstance();
        List<Observable> obsList = osa.getAll();
        for (Observable obs : obsList) {
            ObservableMap.put(obs.getOname(), obs.getOid());
        }
        AccessServiceAdapterImpl asa = AccessServiceAdapterImpl.getInstance();
        List<Access> accList = asa.getAll();
        for (Access acc : accList) {
            AccessMap.put(acc.getAccess(), acc.getAccessid());
        }

        ParticleServiceAdapterImpl psa = ParticleServiceAdapterImpl.getInstance();
        List<Particle> parList = psa.getAll();
        for (Particle par : parList) {
            ParticleMap.put(par.getPname(), par.getPdgid());
        }

        BeamServiceAdapterImpl bsa = BeamServiceAdapterImpl.getInstance();
        List<Beam> beamList = bsa.getAll();
        for (Beam b : beamList) {
            BeamMap.put(b.getBname(), b.getBid());
        }
        MaterialServiceAdapterImpl msa = MaterialServiceAdapterImpl.getInstance();
        List<Material> matList = msa.getAll();
        for (Material mat : matList) {
            MaterialMap.put(mat.getMname(), mat.getMid());
        }
        MctoolServiceAdapterImpl mctsa = MctoolServiceAdapterImpl.getInstance();
        List<Mctool> mctList = mctsa.getAll();
        for (Mctool mct : mctList) {
            MctoolMap.put(mct.getMcname(), mct.getMcid());
        }
        ModelServiceAdapterImpl modelsa = ModelServiceAdapterImpl.getInstance();
        List<Model> modelList = modelsa.getAll();
        for (Model model : modelList) {
            ModelMap.put(model.getModelname(), model.getModelid());
        }
        VersiontagServiceAdapterImpl vtgsa = VersiontagServiceAdapterImpl.getInstance();
        List<Versiontag> vtgList = vtgsa.getAll();
        for (Versiontag vt : vtgList) {
            VersiontagMap.put(vt.getTag(), vt.getVersionId());
        }
        ScoresServiceAdapterImpl ssa = ScoresServiceAdapterImpl.getInstance();
        List<Scores> scoresList = ssa.getAll();
        for (Scores scores : scoresList) {
            ScoresMap.put(scores.getScore(), scores.getScoreid());
        }
        ReactionServiceAdapterImpl reacsa = ReactionServiceAdapterImpl.getInstance();
        List<Reaction> reacList = reacsa.getAll();
        for (Reaction reac : reacList) {
            ReactionMap.put(reac.getRname(), reac.getRid());
        }
        TestServiceAdapterImpl testsa = TestServiceAdapterImpl.getInstance();
        List<Test> testList = testsa.getAll();
        for (Test test : testList) {
            TestMap.put(test.getTestname(), test.getTestid());
        }
        DatatypesServiceAdapterImpl dtypessa = DatatypesServiceAdapterImpl.getInstance();
        List<Datatypes> dtypesList = dtypessa.getAll();
        for (Datatypes dtypes : dtypesList) {
            DatatypesMap.put(dtypes.getDescription(), dtypes.getDtype());
        }
        ReferenceServiceAdapterImpl refsa = ReferenceServiceAdapterImpl.getInstance();
        List<Reference> refList = refsa.getAll();
        for (Reference element : refList) {
            if (element.getInspireid() != 0) {
                ReferenceMap.put(String.valueOf(element.getInspireid()), element.getRefid());

            } else {
                ReferenceMap.put(element.getLinkurl(), element.getRefid());
            }
        }
    }

    /**
     * @return the ObservableMap
     */
    public final BidiMap<String, Integer> getObservableMap() {
        return ObservableMap;
    }

    public final BidiMap<String, Integer> getReactionMap() {
        return ReactionMap;
    }

    /**
     * @return the ParticleMap
     */
    public BidiMap<String, Integer> getParticleMap() {
        return ParticleMap;
    }

    /**
     * @return the MaterialMap
     */
    public BidiMap<String, Integer> getMaterialMap() {
        return MaterialMap;
    }

    /**
     * @return the MctoolMap
     */
    public BidiMap<String, Integer> getMctoolMap() {
        return MctoolMap;
    }

    /**
     * @return the ScoresMap
     */
    public BidiMap<String, Integer> getScoresMap() {
        return ScoresMap;
    }

    /**
     * @return the AccessMap
     */
    public BidiMap<String, Integer> getAccessMap() {
        return AccessMap;
    }

    /**
     * @return the ModelMap
     */
    public BidiMap<String, Integer> getModelMap() {
        return ModelMap;
    }

    /**
     * @return the VersiontagMap
     */
    public BidiMap<String, Integer> getVersiontagMap() {
        return VersiontagMap;
    }

    /**
     * @return the BeamMap
     */
    public BidiMap<String, Integer> getBeamMap() {
        return BeamMap;
    }

    /**
     * @return the TestMap
     */
    public BidiMap<String, Integer> getTestMap() {
        return TestMap;
    }

    /**
     * @return the DatatypesMap
     */
    public BidiMap<String, Integer> getDatatypesMap() {
        return DatatypesMap;
    }

    /**
     * @return the ReferenceMap
     */
    public BidiMap<String, Integer> getReferenceMap() {
        return ReferenceMap;
    }

}
