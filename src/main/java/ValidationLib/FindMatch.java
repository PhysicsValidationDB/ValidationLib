
/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public final class FindMatch extends ServiceAdapter {

    public static FindMatch create() {
        return new FindMatch();
    }

    private List<Integer> allselected;
    private ResultServiceAdapterImpl ressa;
    private BeamServiceAdapterImpl beamsa;

    private FindMatch() {
        Init();
    }

    void Init() {
        ressa = ResultServiceAdapterImpl.getInstance();
        beamsa = BeamServiceAdapterImpl.getInstance();
    }

    /**
     *
     * @param selection1
     * @return
     */
    public HashSet<Integer> match(List<Integer> selection1) {
        this.allselected = selection1;
        HashSet<Integer> matches = new HashSet();
        // SELECT * FROM public.result tn1 join public.result tn2 on tn1.beam=tn2.beam and tn1.target=tn2.target and tn1.observable=tn2.observable and tn1.secondary=tn2.secondary and tn1.secondary=tn2.secondary and tn1.reaction=tn2.reaction and tn1.parvalues=tn2.parvalues where tn1.refid=20 and tn2.tid=10000 and tn1.trid=1 
        String sql = "SELECT RESULT.TRID FROM PUBLIC.RESULT JOIN BEAM ON BEAM.BID=RESULT.BEAM WHERE RESULT.REFID IN (SELECT REFERENCE.REFID FROM REFERENCE JOIN TEST ON REFERENCE.REFID=ANY(TEST.REFERENCEID)"
                + " JOIN RESULT ON RESULT.REFID=REFERENCE.REFID WHERE TEST.TESTID=?) AND ?=ALL(BEAM.PARTICLEIDS) AND RESULT.TARGET=? AND RESULT.REACTION=? AND RESULT.OBSERVABLE=?"
                + " AND RESULT.SECONDARY=?;";

        String sql2 = "SELECT R2.TRID FROM PUBLIC.RESULT R1 "
                + "JOIN TEST ON TEST.TESTID=R1.TID "
                + "JOIN PUBLIC.RESULT R2 ON R1.TARGET=R2.TARGET AND R1.OBSERVABLE=R2.OBSERVABLE AND R1.SECONDARY=R2.SECONDARY AND R1.REACTION=R2.REACTION AND R1.PARVALUES=R2.PARVALUES AND R1.PARNAMES=R2.PARNAMES "
                + "JOIN BEAM B1 ON B1.BID=R1.BEAM "
                + "JOIN BEAM B2 ON B2.BID=R2.BEAM "
                + "WHERE R1.TRID=? "
                + "AND R2.REFID=ANY(TEST.REFERENCEID) AND B2.PARTICLEIDS=B1.PARTICLEIDS "
                + "AND B2.MEANENERGY[ARRAY_UPPER(B2.MEANENERGY,1)]<=B1.MEANENERGY[ARRAY_UPPER(B1.MEANENERGY,1)] "
                + "AND B2.MEANENERGY[ARRAY_LOWER(B2.MEANENERGY,1)]>=B1.MEANENERGY[ARRAY_LOWER(B1.MEANENERGY,1)]";

        for (Integer selection : allselected) {
//            Result selected = ressa.getById(selection);
            PreparedStatement ps;
//            Wgroups dt = null;
            try {
                ps = prepareStatement(sql2);
                ps.setInt(1, selection);
                /*
                ps.setInt(1, selected.getTestlnk());
                //ps.setInt(2, 211);
                System.out.println("Beam =====================:  " + beamsa.getById(selected.getBeamlnk()).getParticleIds().get(0));
                ps.setInt(2, beamsa.getById(selected.getBeamlnk()).getParticleIds().get(0));
                ps.setInt(3, selected.getTargetlnk());
                ps.setInt(4, selected.getReactionlnk());
                ps.setInt(5, selected.getObservablelnk());
                ps.setInt(6, selected.getSecondarylnk());
//                selected.getParnames()
                 */
                result = ps.executeQuery();
                while (result.next()) {
                    matches.add(result.getInt(1));
                    System.out.println("=====================:  " + result.getInt(1));
                }
            } catch (SQLException ex) {
                Logger.getLogger(WgroupsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        closeConnection();
        return matches;
    }

}
