/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class TestTypeServiceAdapterImpl extends ServiceAdapter implements TestTypeServiceAdapter {

    @Override
    public List<String> getTypeList() {

        String sql = "SELECT DISTINCT CODE FROM PUBLIC.TESTTYPES ORDER BY CODE;";

        List<String> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                list.add(result.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestTypeServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }
}
