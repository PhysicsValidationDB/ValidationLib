/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;
import java.util.List;

/**
 * Class representing a TEST (Genie,Geant4) only applies to simulated data not
 * experimental.
 *
 * @author wenzel
 */
public class Test implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private transient int testid;
    private Mctool mctool;
    private Wgroups wgroups;
    private String testname;
    private String description;
    private List<String> responsible;
    private List<String> keywords;
    private List<Reference> referenceid;
//    private List<Integer> testdefaults;



    /**
     *
     * @param testid
     * @param mctool
     * @param wgroups
     * @param testname
     * @param description
     * @param responsible
     * @param keywords
     * @param referenceid // * @param testdefaults
     */
    private Test(int testid, String testname, Mctool mctool, String description, List responsible, Wgroups wgroups, List keywords, List referenceid) {
        this.testid = testid;
        this.mctool = mctool;
        this.wgroups = wgroups;
        this.testname = testname;
        this.description = description;
        this.responsible = responsible;
        this.keywords = keywords;
        this.referenceid = referenceid;
    }
    /**
     *
     */
    private Test() {
    }

    public static Test create() {
        return new Test();
    }

    public static Test create(int testid, String testname, Mctool mctool, String description, List responsible, Wgroups wgroups, List keywords, List referenceid) {
        return new Test(testid,testname,mctool,description,responsible,wgroups,keywords,referenceid);
    }

    public static Test create(String jsonstr)  {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Test.class);
    }

    /**
     *
     * @return
     */
    public int getTestid() {
        return this.testid;
    }

    /**
     *
     * @param testid
     */
    public void setTestid(int testid) {
        this.testid = testid;
    }

    /**
     *
     * @return
     */
    public Mctool getMctool() {
        return this.mctool;
    }

    /**
     *
     * @param mctool
     */
    public void setMctool(Mctool mctool) {
        this.mctool = mctool;
    }

    /**
     *
     * @return
     */
    public Wgroups getWgroups() {
        return this.wgroups;
    }

    /**
     *
     * @param wgroups
     */
    public void setWgroups(Wgroups wgroups) {
        this.wgroups = wgroups;
    }

    /**
     *
     * @return
     */
    public String getTestname() {
        return this.testname;
    }

    /**
     *
     * @param testname
     */
    public void setTestname(String testname) {
        this.testname = testname;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public List getResponsible() {
        return this.responsible;
    }

    /**
     *
     * @param responsible
     */
    public void setResponsible(List responsible) {
        this.responsible = responsible;
    }

    /**
     *
     * @return
     */
    public List getKeywords() {
        return this.keywords;
    }

    /**
     *
     * @param keywords
     */
    public void setKeywords(List keywords) {
        this.keywords = keywords;
    }

    /**
     *
     * @return
     */
    public List getReferences() {
        return this.referenceid;
    }

    /**
     *
     * @param referenceid
     */
    public void setReferences(List referenceid) {
        this.referenceid = referenceid;
    }

    /**
     * @return the testdefaults
     */
//    public List<Integer> getTestdefaults() {
//        return testdefaults;
//    }
    /**
     * @param testdefaults the testdefaults to set
     */
//    public void setTestdefaults(List<Integer> testdefaults) {
//        this.testdefaults = testdefaults;
//    }
    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }

}
