/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 *
 *  * Class retrieving or acting on a TEST (Genie, Geant4) in the database.
 *
 *
 * @author wenzel
 */
public class TestServiceAdapterImpl extends ServiceAdapter implements TestServiceAdapter {

    private static TestServiceAdapterImpl instance;

    private TestServiceAdapterImpl() {
    }

    public static TestServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new TestServiceAdapterImpl();
        }
        return instance;
    }

    @Override
    public Test getById(Integer id) {
        DatatypesServiceAdapterImpl DTAdapter = DatatypesServiceAdapterImpl.getInstance();
        String sql = "SELECT TESTNAME,MCID,DESCRIPTION,RESPONSIBLE,WG,KEYWORDS,REFERENCEID FROM PUBLIC.TEST WHERE TESTID=?;";
        MctoolServiceAdapterImpl MCTAdapter = MctoolServiceAdapterImpl.getInstance();
        WgroupsServiceAdapterImpl WGAdapter = WgroupsServiceAdapterImpl.getInstance();
        ReferenceServiceAdapterImpl RefAdapter = ReferenceServiceAdapterImpl.getInstance();
        PreparedStatement ps;
        String testname;
        Mctool mctool;
        String description;
        ArrayList<String> responsible;
        Wgroups wgroups;
        ArrayList<String> keywords;
        ArrayList<Reference> referenceid;
        Test dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                testname = result.getString(1);
                mctool = MCTAdapter.getById(result.getInt(2));
                description = result.getString(3);
                Array Resparray = result.getArray(4);
                String[] Respar = (String[]) Resparray.getArray();
                responsible = new ArrayList();
                for (int i = 0; i < Respar.length; i++) {
                    responsible.add(Respar[i]);
                }
                wgroups = WGAdapter.getById(result.getInt(5));
                Array KWarray = result.getArray(6);
                String[] KWar = (String[]) KWarray.getArray();
                keywords = new ArrayList();
                for (int i = 0; i < KWar.length; i++) {
                    keywords.add(KWar[i]);
                }
                Array Refarray = result.getArray(7);
                Integer[] Refar = (Integer[]) Refarray.getArray();
                referenceid = new ArrayList();
                for (int i = 0; i < Refar.length; i++) {
                    referenceid.add(RefAdapter.getById(Refar[i]));
                }
                dt = Test.create(id, testname, mctool, description, responsible, wgroups, keywords, referenceid);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    @Override
    public int delete(Test dt
    ) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.TEST WHERE DTID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getTestid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int update(Test dt
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insert(Test dt
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Test> getAll() {
        DatatypesServiceAdapterImpl DTAdapter = DatatypesServiceAdapterImpl.getInstance();
        String sql = "SELECT TESTID,TESTNAME,MCID,DESCRIPTION,RESPONSIBLE,WG,KEYWORDS,REFERENCEID FROM PUBLIC.TEST ORDER BY TESTID;";
        MctoolServiceAdapterImpl MCTAdapter = MctoolServiceAdapterImpl.getInstance();
        WgroupsServiceAdapterImpl WGAdapter = WgroupsServiceAdapterImpl.getInstance();
        ReferenceServiceAdapterImpl RefAdapter = ReferenceServiceAdapterImpl.getInstance();
        PreparedStatement ps;
        Integer id;
        String testname;
        Mctool mctool;
        String description;
        ArrayList<String> responsible;
        Wgroups wgroups;
        ArrayList<String> keywords;
        ArrayList<Reference> referenceid;
        ArrayList<Test> dt = new ArrayList();
        try {
            ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                id = result.getInt(1);
                testname = result.getString(2);
                mctool = MCTAdapter.getById(result.getInt(3));
                description = result.getString(4);
                Array Resparray = result.getArray(5);
                String[] Respar = (String[]) Resparray.getArray();
                responsible = new ArrayList();
                for (int i = 0; i < Respar.length; i++) {
                    responsible.add(Respar[i]);
                }
                wgroups = WGAdapter.getById(result.getInt(6));
                Array KWarray = result.getArray(7);
                String[] KWar = (String[]) KWarray.getArray();
                keywords = new ArrayList();
                for (int i = 0; i < KWar.length; i++) {
                    keywords.add(KWar[i]);
                }
                Array Refarray = result.getArray(8);
                Integer[] Refar = (Integer[]) Refarray.getArray();
                referenceid = new ArrayList();
                for (int i = 0; i < Refar.length; i++) {
                    referenceid.add(RefAdapter.getById(Refar[i]));
                }
                dt.add(Test.create(id, testname, mctool, description, responsible, wgroups, keywords, referenceid));
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

}
