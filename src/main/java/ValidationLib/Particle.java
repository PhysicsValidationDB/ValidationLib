/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;

/**
 *
 * @author wenzel
 */
public class Particle implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private int pdgid;
    private String pname;

        /**
     *
     */
    private Particle() {
    }

    public static Particle create() {
        return new Particle();
    }

    public static Particle create(int pdgid, String pname) {
        return new Particle(pdgid,pname);
    }

    public static Particle create(String jsonstr)  {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Particle.class);
    }

    /**
     *
     * @param pid
     * @param pdgid
     * @param pname
     */
    private Particle(int pdgid, String pname) {
        this.pdgid = pdgid;
        this.pname = pname;
    }

    /**
     *
     * @return
     */
    public int getPdgid() {
        return this.pdgid;
    }

    /**
     *
     * @param pdgid
     */
    public void setPdgid(int pdgid) {
        this.pdgid = pdgid;
    }

    /**
     *
     * @return
     */
    public String getPname() {
        return this.pname;
    }

    /**
     *
     * @param pname
     */
    public void setPname(String pname) {
        this.pname = pname;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }
}
