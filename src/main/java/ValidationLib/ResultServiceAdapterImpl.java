/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.io.IOException;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 *
 * Class retrieving or acting on a TEST Result (Experiment,GENIE,Geant4....) in
 * the database. A Result is always linked to a datatable or imageblob therefore
 * this class deals with creating and deleting this classes too. Database
 * transactions are used to ensure things are done in the right order and are
 * rolled back if any step in the transaction chain fails. This class is
 * implemented as a Lazy Initialization Singleton.
 *
 * @author wenzel
 */
public class ResultServiceAdapterImpl extends ServiceAdapter implements ResultServiceAdapter {

    private static ResultServiceAdapterImpl instance;

    private ResultServiceAdapterImpl() {
    }

    public static ResultServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new ResultServiceAdapterImpl();
        }
        return instance;
    }

    @Override
    public Result getById(Integer id, Integer access) {
        String sql = "SELECT TID,REFID,MODEL,MCTOOL,VERSIONTAG,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,IMAGEBLOBID,SCORE,ACCESS,PARNAMES,PARVALUES,MODTIME  FROM PUBLIC.RESULT WHERE ACCESS=" + access + " AND TRID=?;";
        DatatableServiceAdapterImpl DatatableAdapter = DatatableServiceAdapterImpl.getInstance();
        PreparedStatement ps;
        Result dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Result.create();
                dt.setTrid(id);
                int col = 1;
                dt.setTestlnk(result.getInt(col));
                dt.setReferencelnk(result.getInt(++col));
                dt.setModellnk(result.getInt(++col));
                dt.setMctoollnk(result.getInt(++col));
                dt.setVersiontaglnk(result.getInt(++col));
                dt.setBeamlnk(result.getInt(++col));
                dt.setTargetlnk(result.getInt(++col));
                dt.setObservablelnk(result.getInt(++col));
                dt.setSecondarylnk(result.getInt(++col));
                dt.setReactionlnk(result.getInt(++col));
                dt.setDatatable(DatatableAdapter.getById(result.getInt(++col)));
                dt.setImageblobslnk(result.getInt(++col));
                dt.setScoreslnk(result.getInt(++col));
                dt.setAccesslnk(result.getInt(++col));
                Array Pnamesarray = result.getArray(++col);
                List<String> parnames = new ArrayList();
                if (Pnamesarray != null) {
                    String[] Pnamesar = (String[]) Pnamesarray.getArray();
                    for (int i = 0; i < Pnamesar.length; i++) {
                        parnames.add(Pnamesar[i]);
                    }
                }
                dt.setParnames(parnames);
                Array Pvaluesarray = result.getArray(++col);
                List<String> parvalues = new ArrayList();
                if (Pvaluesarray != null) {
                    String[] Pvaluesar = (String[]) Pvaluesarray.getArray();
                    for (int i = 0; i < Pvaluesar.length; i++) {
                        parvalues.add(Pvaluesar[i]);
                    }
                }
                dt.setParvalues(parvalues);
                dt.setModtime(result.getTimestamp(++col));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        DictionaryService ds = null;
        try {
            ds = DictionaryService.getInstance();
        } catch (IOException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        dt.setAccesskw(ds.getAccessMap().getKey(dt.getAccesslnk()));
        dt.setScoreskw(ds.getScoresMap().getKey(dt.getScoreslnk()));
        dt.setBeamkw(ds.getBeamMap().getKey(dt.getBeamlnk()));
        dt.setTargetkw(ds.getMaterialMap().getKey(dt.getTargetlnk()));
        dt.setTestkw(ds.getTestMap().getKey(dt.getTestlnk()));
        dt.setModelkw(ds.getModelMap().getKey(dt.getModellnk()));
        dt.setMctoolkw(ds.getMctoolMap().getKey(dt.getMctoollnk()));
        dt.setVersiontagkw(ds.getVersiontagMap().getKey(dt.getVersiontaglnk()));
        dt.setObservablekw(ds.getObservableMap().getKey(dt.getObservablelnk()));
        dt.setSecondarykw(ds.getParticleMap().getKey(dt.getSecondarylnk()));
        dt.setReactionkw(ds.getReactionMap().getKey(dt.getReactionlnk()));
        dt.setReferencekw(ds.getReferenceMap().getKey(dt.getReferencelnk()));

        return dt;
    }

    @Override
    public boolean delete(Integer trid) {
        int executeUpdate = 0;
        boolean returnvalue;
        Integer datatableid = null;
        Integer imageblobid = null;
        String sql;
        try {
            // First find out what objects (not dictionaries this result is linked to 
            sql = "SELECT DATATABLEID,IMAGEBLOBID FROM  PUBLIC.RESULT WHERE TRID=?;";
            PreparedStatement ps = prepareStatement(sql);
            ps.setInt(1, trid);
            result = ps.executeQuery();
            if (!result.next()) {
                return false;
            } else {
                do {
                    datatableid = result.getInt(1);
                    imageblobid = result.getInt(2);
                } while (result.next());
            }

        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        try {
            openTransaction();
            sql = "DELETE FROM PUBLIC.RESULT WHERE TRID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, trid);
            executeUpdate = ps.executeUpdate();
            if (datatableid != null && datatableid != 0) {
                String sqldt = "DELETE FROM PUBLIC.DATATABLE WHERE DTID=?;";
                PreparedStatement psdt = prepareWriteStatement(sqldt);
                psdt.setInt(1, datatableid);
                executeUpdate = psdt.executeUpdate();
            }
            if (imageblobid != null && imageblobid != 0) {
                String sqlib = "DELETE FROM PUBLIC.IMAGEBLOB WHERE IBID=?;";
                PreparedStatement psib = prepareWriteStatement(sqlib);
                psib.setInt(1, datatableid);
                executeUpdate = psib.executeUpdate();
            }
            closeTransaction();
            returnvalue = true;
        } catch (SQLException ex) {
            try {
                rollbackTransaction();
            } catch (SQLException ex1) {
                Logger.getLogger(ResultServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(ResultServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
            returnvalue = false;
        } finally {
            closeConnection();
        }
        return returnvalue;
    }

    @Override
    public int update(Result dt
    ) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insert(Result res) {

        Integer maxidxrs = null;         // highest index of index of Result table
        Integer maxidxdt = null;         // highest index of index of Datatable table
        Integer maxidxib = null;         // highest index of index of Imageblob table
        Integer executeUpdate = null;
        PreparedStatement ps;
        //
        // make sure sequences start at the end.
        try {
            getWriterConnection();
            String sql = "SELECT SETVAL('RESULT_TRID_SEQ',(SELECT MAX(TRID) FROM PUBLIC.RESULT));";
            ps = prepareWriteStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                maxidxrs = result.getInt(1);
            }
            sql = "SELECT SETVAL('DATATABLE_DTID_SEQ',(SELECT MAX(DTID) FROM PUBLIC.DATATABLE));";
            ps = prepareWriteStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                maxidxdt = result.getInt(1);
            }
            sql = "SELECT SETVAL('IMAGEBLOB_IBID_SEQ',(SELECT MAX(IBID) FROM PUBLIC.IMAGEBLOB));";
            ps = prepareWriteStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                maxidxib = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }

        try {
            openTransaction();
            if (res.getDatatable() != null) {
                Datatable dt = res.getDatatable();
                String sqldt = "INSERT INTO PUBLIC.DATATABLE "
                        + "(DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) "
                        + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?);";
                ps = prepareWriteStatement(sqldt);
                ps.setInt(1, dt.getDatatypeslnk());
                ps.setString(2, dt.getTitle());
                ps.setInt(3, dt.getNpoints());

                //
                if (dt.getNbins() != null) {
                    List<Integer> nbins = dt.getNbins();
                    Integer[] binar = (Integer[]) nbins.toArray(new Integer[nbins.size()]);
                    Array sqlBINArray = connection.createArrayOf("integer", binar);
                    ps.setArray(4, sqlBINArray);
                } else {
                    ps.setArray(4, null);
                }
                //
                List<String> AxisTitles = dt.getAxisTitle();
                String[] atar = (String[]) AxisTitles.toArray(new String[AxisTitles.size()]);
                Array sqlATArray = connection.createArrayOf("varchar", atar);
                ps.setArray(5, sqlATArray);
                //
                List<Float> Values = dt.getVal();
                Float[] valar = (Float[]) Values.toArray(new Float[Values.size()]);
                Array sqlValArray = connection.createArrayOf("float", valar);
                ps.setArray(6, sqlValArray);
                //
                List<Float> ERR_STAT_PLUS = dt.getErrStatPlus();
                Float[] ERR_STAT_PLUSar = (Float[]) ERR_STAT_PLUS.toArray(new Float[ERR_STAT_PLUS.size()]);
                Array sqlERR_STAT_PLUSArray = connection.createArrayOf("float", ERR_STAT_PLUSar);
                ps.setArray(7, sqlERR_STAT_PLUSArray);
                //
                List<Float> ERR_STAT_MINUS = dt.getErrStatMinus();
                Float[] ERR_STAT_MINUSar = (Float[]) ERR_STAT_MINUS.toArray(new Float[ERR_STAT_MINUS.size()]);
                Array sqlERR_STAT_MINUSArray = connection.createArrayOf("float", ERR_STAT_MINUSar);
                ps.setArray(8, sqlERR_STAT_MINUSArray);
                //
                if (dt.getErrSysPlus() != null) {
                    List<Float> ERR_SYS_PLUS = dt.getErrSysPlus();
                    Float[] ERR_SYS_PLUSar = (Float[]) ERR_SYS_PLUS.toArray(new Float[ERR_SYS_PLUS.size()]);
                    Array sqlERR_SYS_PLUSArray = connection.createArrayOf("float", ERR_SYS_PLUSar);
                    ps.setArray(9, sqlERR_SYS_PLUSArray);
                } else {
                    System.out.println("ooooooops9");
                    ps.setArray(9, null);
                }
                //
                if (dt.getErrSysMinus() != null) {
                    List<Float> ERR_SYS_MINUS = dt.getErrSysMinus();
                    Float[] ERR_SYS_MINUSar = (Float[]) ERR_SYS_MINUS.toArray(new Float[ERR_SYS_MINUS.size()]);
                    Array sqlERR_SYS_MINUSArray = connection.createArrayOf("float", ERR_SYS_MINUSar);
                    ps.setArray(10, sqlERR_SYS_MINUSArray);
                } else {
                    System.out.println("ooooooops10");
                    ps.setArray(10, null);
                }
                //
                if (dt.getBinMin() != null) {
                    List<Float> BIN_MIN = dt.getBinMin();
                    Float[] BIN_MINar = (Float[]) BIN_MIN.toArray(new Float[BIN_MIN.size()]);
                    Array sqlBIN_MINArray = connection.createArrayOf("float", BIN_MINar);
                    ps.setArray(11, sqlBIN_MINArray);
                } else {
                    System.out.println("ooooooops11");
                    ps.setArray(11, null);
                }
                //
                if (dt.getBinMax() != null) {
                    List<Float> BIN_MAX = dt.getBinMax();
                    Float[] BIN_MAXar = (Float[]) BIN_MAX.toArray(new Float[BIN_MAX.size()]);
                    Array sqlBIN_MAXArray = connection.createArrayOf("float", BIN_MAXar);
                    ps.setArray(12, sqlBIN_MAXArray);
                } else {
                    System.out.println("ooooooops12");
                    ps.setArray(12, null);
                }
                //  
                executeUpdate = ps.executeUpdate();
                maxidxdt++;
            }
            if (res.getImageblobslnk() != null && res.getImageblobslnk() != 0) {
                //
                // not yet dealing with blobs
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
            // 
            //  Adding experimental data (linked to a reference instead of test)
            //
            if (res.getReferencelnk() != null && res.getReferencelnk() != 0) {   // not linked to reference 
                String sqlres = "INSERT INTO PUBLIC.RESULT "
                        + "(REFID,MODEL,MCTOOL,VERSIONTAG,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) "
                        + "VALUES "
                        + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                ps = prepareWriteStatement(sqlres);
                int col = 1;
                ps.setInt(col, res.getReferencelnk());
                ps.setInt(++col, res.getModellnk());
                ps.setInt(++col, res.getMctoollnk());
                ps.setInt(++col, res.getVersiontaglnk());
                ps.setInt(++col, res.getBeamlnk());
                ps.setInt(++col, res.getTargetlnk());
                ps.setInt(++col, res.getObservablelnk());
                if (res.getSecondarylnk() == null) {
                    ps.setNull(++col, java.sql.Types.INTEGER);
                } else {
                    ps.setInt(++col, res.getSecondarylnk());
                }
                ps.setInt(++col, res.getReactionlnk());
                ps.setInt(++col, maxidxdt);
                if (res.getScoreslnk() == null) {
                    ps.setNull(++col, java.sql.Types.INTEGER);
                } else {
                    ps.setInt(++col, res.getScoreslnk());
                }
                ps.setInt(++col, res.getAccesslnk());
            } else if (res.getTestlnk() != null && res.getTestlnk() != 0) {  //  linked to test 
                String sqlres = "INSERT INTO PUBLIC.RESULT "
                        + "(TID,MODEL,MCTOOL,VERSIONTAG,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) "
                        + "VALUES "
                        + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                ps = prepareWriteStatement(sqlres);
                int col = 1;
                ps.setInt(col, res.getTestlnk());
                ps.setInt(++col, res.getModellnk());
                ps.setInt(++col, res.getMctoollnk());
                ps.setInt(++col, res.getVersiontaglnk());
                ps.setInt(++col, res.getBeamlnk());
                ps.setInt(++col, res.getTargetlnk());
                ps.setInt(++col, res.getObservablelnk());
                if (res.getSecondarylnk() == null) {
                    ps.setNull(++col, java.sql.Types.INTEGER);
                } else {
                    ps.setInt(++col, res.getSecondarylnk());
                }
                ps.setInt(++col, res.getReactionlnk());
                ps.setInt(++col, maxidxdt);
                if (res.getScoreslnk() == null) {
                    ps.setNull(++col, java.sql.Types.INTEGER);
                } else {
                    ps.setInt(++col, res.getScoreslnk());
                }
                ps.setInt(++col, res.getAccesslnk());
            } else {
                throw new UnsupportedOperationException("Not supported."); //To change body of generated methods, choose Tools | Templates.
            }
            String[] names;
            String[] values;
            List<String> parnames = res.getParnames();
            if (parnames != null) {
                Array sqlValArray = connection.createArrayOf("varchar", parnames.toArray());
                ps.setArray(13, sqlValArray);
            } else {
                ps.setArray(13, null);
            }

            List<String> parvalues = res.getParvalues();
            if (parvalues != null) {
                Array sqlValArray2 = connection.createArrayOf("varchar", parvalues.toArray());
                ps.setArray(14, sqlValArray2);
            } else {
                ps.setArray(14, null);
            }
            executeUpdate = ps.executeUpdate();
            closeTransaction();
            maxidxrs++;
        } catch (SQLException ex) {
            try {
                rollbackTransaction();
            } catch (SQLException ex1) {
                Logger.getLogger(ResultServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(ResultServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return maxidxrs;
    }

    @Override
    public Set<Integer> getAll() {
        String sql = "SELECT TRID FROM PUBLIC.RESULT;";
        Set<Integer> ResultList = new HashSet();
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                ResultList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return ResultList;
    }

    public void openTransaction() throws SQLException {
        try {
            connection = getWriterConnection();
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
            connection = null;
            Logger
                    .getLogger(ResultServiceAdapterImpl.class
                            .getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public void closeTransaction() {
        if (connection != null) {
            try {
                connection.commit();
                connection.setAutoCommit(true);
                closeConnection();
                connection = null;

            } catch (SQLException ex) {
                Logger.getLogger(ResultServiceAdapterImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void rollbackTransaction() throws SQLException {
        if (connection != null) {
            connection.rollback();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        closeTransaction();
        super.finalize();
    }

    @Override
    public List<Integer> getRefs() {
        String sql = "SELECT DISTINCT REFID FROM PUBLIC.RESULT WHERE MCTOOL=1 ORDER BY REFID";
        List<Integer> RefList = new ArrayList();
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                RefList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        Collections.sort(RefList);
        return RefList;
    }

    @Override
    public Integer getTotalExp() {
        String sql = "SELECT COUNT(MCTOOL) FROM PUBLIC.RESULT WHERE MCTOOL=1;";
        Integer TotalNrofExperiments = null;
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                TotalNrofExperiments = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return TotalNrofExperiments;
    }

    @Override
    public Integer getNrofExpResultsbyRef(Integer iref) {
        String sql = "SELECT COUNT(MCTOOL) FROM PUBLIC.RESULT WHERE MCTOOL=1 AND REFID=?;";
        Integer NrofResults = null;
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, iref);
            result = ps.executeQuery();
            while (result.next()) {
                NrofResults = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return NrofResults;
    }

    @Override
    public Integer getNrofResultsbyTest(Integer itest, Integer access) {
        String sql = "SELECT COUNT(MCTOOL) FROM PUBLIC.RESULT WHERE MCTOOL>1 AND ACCESS=" + access + " AND TID=?;";
        Integer NrofResults = null;
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, itest);
            result = ps.executeQuery();
            while (result.next()) {
                NrofResults = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return NrofResults;
    }

    //
    // get Geant 4 tests 
    //
    @Override
    public Set<Integer> getTests() {
        String sql = "SELECT DISTINCT TID  FROM PUBLIC.RESULT WHERE TID!=0";
        Set<Integer> RefList = new HashSet();
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                RefList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return RefList;
    }

    @Override
    public Integer getTotalTests(Integer access) {
        String sql = "SELECT COUNT(MCTOOL) FROM PUBLIC.RESULT WHERE MCTOOL>1 AND ACCESS=" + access + ";";
        Integer TotalNrofTests = null;
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                TotalNrofTests = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return TotalNrofTests;
    }

    /*
    @Override
    public Integer getNrofExpResultsbyRef(Integer iref) {
        String sql = "SELECT COUNT(MCDTID) FROM PUBLIC.RESULT WHERE MCDTID=1 AND REFID=?;";
        Integer NrofResults = null;
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, iref);
            result = ps.executeQuery();
            while (result.next()) {
                NrofResults = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return NrofResults;
    }
     */
    @Override
    public Set<Integer> getBeamsbyExperiment(Integer testid) {
        Set<Integer> BeamList = new HashSet();
        String sql = "SELECT DISTINCT BEAM  FROM PUBLIC.RESULT WHERE REFID=? ORDER BY BEAM;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                BeamList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return BeamList;
    }

    @Override
    public Set<Integer> getObservablesbyExperiment(Integer testid) {
        Set<Integer> ObservableList = new HashSet();
        String sql = "SELECT DISTINCT OBSERVABLE FROM PUBLIC.RESULT WHERE REFID=? ORDER BY OBSERVABLE;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                ObservableList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return ObservableList;
    }

    @Override
    public Set<Integer> getReactionsbyExperiment(Integer testid) {
        Set<Integer> ReactionList = new HashSet();
        String sql = "SELECT DISTINCT REACTION FROM PUBLIC.RESULT WHERE REFID=? ORDER BY REACTION;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                ReactionList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return ReactionList;
    }

    @Override
    public Set<Integer> getSecondariesbyExperiment(Integer testid) {
        Set<Integer> SecondaryList = new HashSet();
        String sql = "SELECT DISTINCT SECONDARY FROM PUBLIC.RESULT WHERE REFID=? ORDER BY SECONDARY;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                SecondaryList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return SecondaryList;
    }

    @Override
    public Set<Integer> getTargetsbyExperiment(Integer testid) {
        Set<Integer> TargetList = new HashSet();
        String sql = "SELECT DISTINCT TARGET FROM PUBLIC.RESULT WHERE REFID=? ORDER BY TARGET;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                TargetList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return TargetList;
    }

    @Override
    public HashMap< String, HashSet<String>> getParValPairsbyExperiment(Integer testid) {
        Set<ParValPair> ParValList = new HashSet();
        String[] PNamesar;
        String[] PValuesar;
        String sql = "SELECT PARNAMES,PARVALUES FROM PUBLIC.RESULT WHERE REFID=?;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                Array PNamesarray = result.getArray(1);
                Array PValuesarray = result.getArray(2);
                if (PNamesarray != null && PValuesarray != null) {
                    PNamesar = (String[]) PNamesarray.getArray();
                    PValuesar = (String[]) PValuesarray.getArray();
                    for (int i = 0; i < PNamesar.length; i++) {
                        ParValList.add(new ParValPair(PNamesar[i], PValuesar[i]));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        Set<ParValPair> uniquepairs = new HashSet<ParValPair>(ParValList);
        Set<String> mySet1 = new HashSet();
        for (ParValPair pair : uniquepairs) {
            //System.out.println("unique:  " + pair.getParName() + "   " + pair.getParValue());
            mySet1.add(pair.getParName());
        }
        HashMap hm = new HashMap();
        for (String name : mySet1) {
            hm.put(name, new HashSet());
        }
        Set set = hm.entrySet();
        for (ParValPair pair : uniquepairs) {
            HashSet hs = (HashSet) hm.get(pair.getParName());
            hs.add(pair.getParValue());
        }

        //Iterator it = hm.entrySet().iterator();
        //while (it.hasNext()) {
        //  Map.Entry pair = (Map.Entry) it.next();
        // System.out.println(pair.getKey());
        // HashSet<String> hs = (HashSet) pair.getValue();
        // Collections.sort(hs);
        //for (String ele : hs) {
        //    System.out.println(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;" + ele);
        //}
        //}
        return hm;
    }

    @Override
    public HashMap< String, HashSet<String>> getParValPairs(List<Integer> Selection) {
        //System.out.println("***************getParValPairs");
        Set<ParValPair> ParValList = new HashSet();
        String[] PNamesar;
        String[] PValuesar;
        String sql = "SELECT PARNAMES,PARVALUES FROM PUBLIC.RESULT WHERE TRID=?;";
        PreparedStatement ps;
        for (Integer test : Selection) {
            try {
                ps = prepareStatement(sql);
                ps.setInt(1, test);
                result = ps.executeQuery();
                while (result.next()) {
                    Array PNamesarray = result.getArray(1);
                    Array PValuesarray = result.getArray(2);
                    if (PNamesarray != null && PValuesarray != null) {
                        PNamesar = (String[]) PNamesarray.getArray();
                        PValuesar = (String[]) PValuesarray.getArray();
                        for (int i = 0; i < PNamesar.length; i++) {
                            ParValList.add(new ParValPair(PNamesar[i], PValuesar[i]));
                        }
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(ResultServiceAdapterImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        closeConnection();
        if (ParValList.size() == 0) {
            //System.out.println("*****************no unique");
            return null;
        }
        Set<ParValPair> uniquepairs = new HashSet<ParValPair>(ParValList);
        Set<String> mySet1 = new HashSet();
        for (ParValPair pair : uniquepairs) {
            System.out.println("*****************unique:  " + pair.getParName() + "   " + pair.getParValue());
            mySet1.add(pair.getParName());
        }
        HashMap hm = new HashMap();
        for (String name : mySet1) {
            hm.put(name, new HashSet());
        }
        Set set = hm.entrySet();
        for (ParValPair pair : uniquepairs) {
            HashSet hs = (HashSet) hm.get(pair.getParName());
            hs.add(pair.getParValue());
        }

        Iterator it = hm.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            //System.out.println("RSSA: :::::::::::::::::::::  " + pair.getKey());
            HashSet<String> hs = (HashSet) pair.getValue();
            // Collections.sort(hs);
            for (String ele : hs) {
                System.out.println(":::::::::::::::::::::::::::::" + ele);
            }
        }
        return hm;
    }

    @Override
    public Set<Integer> getAccessbyTest(Integer testid) {
        Set<Integer> AccessList = new HashSet();
        String sql = "SELECT DISTINCT ACCESS  FROM PUBLIC.RESULT WHERE TID=? ORDER BY ACCESS;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                AccessList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return AccessList;
    }

    @Override
    public Set<Integer> getBeamsbyTest(Integer testid, Integer access) {
        Set<Integer> BeamList = new HashSet();
        String sql = "SELECT DISTINCT BEAM  FROM PUBLIC.RESULT WHERE ACCESS=" + access + " AND TID=? ORDER BY BEAM;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                BeamList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return BeamList;
    }

    @Override
    public Set<Integer> getObservablesbyTest(Integer testid, Integer access) {
        Set<Integer> ObservableList = new HashSet();
        String sql = "SELECT DISTINCT OBSERVABLE FROM PUBLIC.RESULT WHERE ACCESS=" + access + " AND TID=? ORDER BY OBSERVABLE;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                ObservableList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return ObservableList;
    }

    @Override
    public Set<Integer> getReactionsbyTest(Integer testid, Integer access) {
        Set<Integer> ReactionList = new HashSet();
        String sql = "SELECT DISTINCT REACTION FROM PUBLIC.RESULT WHERE ACCESS=" + access + " AND TID=? ORDER BY REACTION;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                ReactionList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return ReactionList;
    }

    @Override
    public Set<Integer> getSecondariesbyTest(Integer testid, Integer access) {
        Set<Integer> SecondaryList = new HashSet();
        String sql = "SELECT DISTINCT SECONDARY FROM PUBLIC.RESULT WHERE ACCESS=" + access + " AND TID=? ORDER BY SECONDARY;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                SecondaryList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return SecondaryList;
    }

    @Override
    public Set<Integer> getTargetsbyTest(Integer testid, Integer access) {
        Set<Integer> TargetList = new HashSet();
        String sql = "SELECT DISTINCT TARGET FROM PUBLIC.RESULT WHERE ACCESS=" + access + " AND TID=? ORDER BY TARGET;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, testid);
            result = ps.executeQuery();
            while (result.next()) {
                TargetList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return TargetList;
    }

    @Override
    public HashMap< String, HashSet<String>> getParValPairsbyTest(Integer testid, Integer access) {
        Set<ParValPair> ParValList = new HashSet();
        String[] PNamesar;
        String[] PValuesar;
        String sql = "SELECT PARNAMES,PARVALUES FROM PUBLIC.RESULT WHERE ACCESS=? AND TID=?;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, access);
            ps.setInt(2, testid);
            result = ps.executeQuery();
            while (result.next()) {
                Array PNamesarray = result.getArray(1);
                Array PValuesarray = result.getArray(2);
                if (PNamesarray != null && PValuesarray != null) {
                    PNamesar = (String[]) PNamesarray.getArray();
                    PValuesar = (String[]) PValuesarray.getArray();
                    for (int i = 0; i < PNamesar.length; i++) {
                        ParValList.add(new ParValPair(PNamesar[i], PValuesar[i]));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        Set<ParValPair> uniquepairs = new HashSet<ParValPair>(ParValList);
        Set<String> mySet1 = new HashSet();
        for (ParValPair pair : uniquepairs) {
            System.out.println("unique:  " + pair.getParName() + "   " + pair.getParValue());
            mySet1.add(pair.getParName());
        }
        HashMap hm = new HashMap();
        for (String name : mySet1) {
            hm.put(name, new HashSet());
        }
        Set set = hm.entrySet();
        for (ParValPair pair : uniquepairs) {
            HashSet hs = (HashSet) hm.get(pair.getParName());
            hs.add(pair.getParValue());
        }

        Iterator it = hm.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair.getKey());
            HashSet<String> hs = (HashSet) pair.getValue();
            // Collections.sort(hs);
            for (String ele : hs) {
                System.out.println(ele);
            }
        }
        return hm;
    }

    /**
     *
     * @param testlnk
     * @param referencelnk
     * @param modellnk
     * @param beamlnk
     * @param versiontaglnk
     * @param targetlnk
     * @param observablelnk
     * @param secondarylnk
     * @param reactionlnk
     * @param particlelnk
     * @param mctoollnk
     * @return
     * @throws java.io.IOException
     */
    @Override
    public List<Result> getbyMetadata(
            Integer testlnk,
            Integer referencelnk,
            Integer modellnk,
            Integer mctoollnk,
            Integer versiontaglnk,
            Integer beamlnk,
            Integer targetlnk,
            Integer observablelnk,
            Integer secondarylnk,
            Integer reactionlnk,
            Integer particlelnk)
            throws IOException {
        DatatableServiceAdapterImpl DatatableAdapter = DatatableServiceAdapterImpl.getInstance();
        boolean AND = false;
        List<Result> list = new ArrayList<>();
        String sql = "SELECT TRID,TID,REFID,MODEL,MCTOOL,VERSIONTAG,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,IMAGEBLOBID,SCORE,ACCESS,PARNAMES,PARVALUES,MODTIME  FROM PUBLIC.RESULT ";
        if (testlnk != null && testlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE TID=" + testlnk;
            } else {
                sql = sql + " AND TID=" + testlnk;
            }
        }
        if (referencelnk != null && referencelnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE REFID=" + referencelnk;
            } else {
                sql = sql + " AND REFID=" + referencelnk;
            }
        }
        if (modellnk != null && modellnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE MODEL=" + modellnk;
            } else {
                sql = sql + " AND MODEL=" + modellnk;
            }
        }
        if (mctoollnk != null && mctoollnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE MCTOOL=" + mctoollnk;
            } else {
                sql = sql + " AND MCTOOL=" + mctoollnk;
            }
        }
        if (versiontaglnk != null && versiontaglnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE VERSIONTAG=" + versiontaglnk;
            } else {
                sql = sql + " AND VERSIONTAG=" + versiontaglnk;
            }
        }
        if (beamlnk != null && beamlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE BEAM=" + beamlnk;
            } else {
                sql = sql + " AND BEAM=" + beamlnk;
            }
        }
        if (targetlnk != null && targetlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE TARGET=" + targetlnk;
            } else {
                sql = sql + " AND TARGET=" + targetlnk;
            }
        }
        if (observablelnk != null && observablelnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE OBSERVABLE=" + observablelnk;
            } else {
                sql = sql + " AND OBSERVABLE=" + observablelnk;
            }
        }
        if (secondarylnk != null && secondarylnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE SECONDARY=" + secondarylnk;
            } else {
                sql = sql + " AND SECONDARY=" + secondarylnk;
            }
        }
        if (reactionlnk != null && reactionlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE REACTION=" + reactionlnk;
            } else {
                sql = sql + " AND REACTION=" + reactionlnk;
            }
        }
        if (particlelnk != null && particlelnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE PARTICLE=" + particlelnk;
            } else {
                sql = sql + " AND PARTICLE=" + particlelnk;
            }
        }

        sql = sql + " LIMIT 100;";

        // some ideas to work on parname/value pairs
        //SELECT * FROM public.result WHERE '164' = ANY (parvalues);
        //SELECT * FROM public.result WHERE '164' = ANY (parvalues) and 'theta outgoing particle' = ANY (parnames);
        System.out.println(sql);

        try {
            getConnection();

        } catch (SQLException exc) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, exc);
        }
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();

        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while (result.next()) {
                Result dt = Result.create();
                int col = 1;
                dt.setTrid(result.getInt(col));
                dt.setTestlnk(result.getInt(++col));
                dt.setReferencelnk(result.getInt(++col));
                dt.setModellnk(result.getInt(++col));
                dt.setMctoollnk(result.getInt(++col));
                dt.setVersiontaglnk(result.getInt(++col));
                dt.setBeamlnk(result.getInt(++col));
                dt.setTargetlnk(result.getInt(++col));
                dt.setObservablelnk(result.getInt(++col));
                dt.setSecondarylnk(result.getInt(++col));
                dt.setReactionlnk(result.getInt(++col));
                dt.setDatatable(DatatableAdapter.getById(result.getInt(++col)));
                dt.setImageblobslnk(result.getInt(++col));
                dt.setScoreslnk(result.getInt(++col));
                dt.setAccesslnk(result.getInt(++col));
                Array Pnamesarray = result.getArray(++col);
                List<String> parnames = new ArrayList();
                if (Pnamesarray != null) {
                    String[] Pnamesar = (String[]) Pnamesarray.getArray();
                    for (int i = 0; i < Pnamesar.length; i++) {
                        parnames.add(Pnamesar[i]);
                    }
                }
                dt.setParnames(parnames);
                Array Pvaluesarray = result.getArray(++col);
                List<String> parvalues = new ArrayList();
                if (Pvaluesarray != null) {
                    String[] Pvaluesar = (String[]) Pvaluesarray.getArray();
                    for (int i = 0; i < Pvaluesar.length; i++) {
                        parvalues.add(Pvaluesar[i]);
                    }
                }
                dt.setParvalues(parvalues);
                dt.setModtime(result.getTimestamp(++col));

                list.add(dt);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    /**
     *
     * @param testlnk
     * @param referencelnk
     * @param modellnk
     * @param mctoollnk
     * @param versiontaglnk
     * @param beamlnk
     * @param targetlnk
     * @param observablelnk
     * @param secondarylnk
     * @param reactionlnk
     * @param ParName
     * @return
     * @throws java.io.IOException
     */
    @Override
    public List<Result> getbyMetadataTest(
            Integer testlnk,
            Integer referencelnk,
            Integer modellnk,
            Integer mctoollnk,
            Integer versiontaglnk,
            Integer beamlnk,
            Integer targetlnk,
            Integer observablelnk,
            Integer secondarylnk,
            Integer reactionlnk,
            String ParName,
            String ParValue)
            throws IOException {
        DatatableServiceAdapterImpl DatatableAdapter = DatatableServiceAdapterImpl.getInstance();
        boolean AND = false;
        List<Result> list = new ArrayList<Result>();
        String sql = "SELECT TRID,TID,REFID,MODEL,MCTOOL,VERSIONTAG,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,IMAGEBLOBID,SCORE,ACCESS,PARNAMES,PARVALUES,MODTIME  FROM PUBLIC.RESULT ";
        if (testlnk != null && testlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE TID=" + testlnk;
            } else {
                sql = sql + " AND TID=" + testlnk;
            }
        }
        if (referencelnk != null && referencelnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE REFID=" + referencelnk;
            } else {
                sql = sql + " AND REFID=" + referencelnk;
            }
        }
        if (modellnk != null && modellnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE MODEL=" + modellnk;
            } else {
                sql = sql + " AND MODEL=" + modellnk;
            }
        }
        if (mctoollnk != null && mctoollnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE MCTOOL=" + mctoollnk;
            } else {
                sql = sql + " AND MCTOOL=" + mctoollnk;
            }
        }
        if (versiontaglnk != null && versiontaglnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE VERSIONTAG=" + versiontaglnk;
            } else {
                sql = sql + " AND VERSIONTAG=" + versiontaglnk;
            }
        }

        if (beamlnk != null && beamlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE BEAM=" + beamlnk;
            } else {
                sql = sql + " AND BEAM=" + beamlnk;
            }
        }
        if (targetlnk != null && targetlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE TARGET=" + targetlnk;
            } else {
                sql = sql + " AND TARGET=" + targetlnk;
            }
        }
        if (observablelnk != null && observablelnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE OBSERVABLE=" + observablelnk;
            } else {
                sql = sql + " AND OBSERVABLE=" + observablelnk;
            }
        }
        if (secondarylnk != null && secondarylnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE SECONDARY=" + secondarylnk;
            } else {
                sql = sql + " AND SECONDARY=" + secondarylnk;
            }
        }
        if (reactionlnk != null && reactionlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE REACTION=" + reactionlnk;
            } else {
                sql = sql + " AND REACTION=" + reactionlnk;
            }
        }
        if (ParName != null && ParValue != null) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE " + "'" + ParName + "' = ANY(PARNAMES)  AND '" + ParValue + "'=ANY(PARVALUES)";
            } else {
                sql = sql + " AND '" + ParName + "' = ANY(PARNAMES)  AND '" + ParValue + "'=ANY(PARVALUES)";
            }
        }
        sql = sql + ";";

        // some ideas to work on parname/value pairs
        //SELECT * FROM public.result WHERE '164' = ANY (parvalues);
        //SELECT * FROM public.result WHERE '164' = ANY (parvalues) and 'theta outgoing particle' = ANY (parnames);
        System.out.println(sql);

        try {
            getConnection();

        } catch (SQLException exc) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, exc);
        }
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();

        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while (result.next()) {
                Result dt = Result.create();
                int col = 1;
                dt.setTrid(result.getInt(col));
                dt.setTestlnk(result.getInt(++col));
                dt.setReferencelnk(result.getInt(++col));
                dt.setModellnk(result.getInt(++col));
                dt.setMctoollnk(result.getInt(++col));
                dt.setVersiontaglnk(result.getInt(++col));
                dt.setBeamlnk(result.getInt(++col));
                dt.setTargetlnk(result.getInt(++col));
                dt.setObservablelnk(result.getInt(++col));
                dt.setSecondarylnk(result.getInt(++col));
                dt.setReactionlnk(result.getInt(++col));
                dt.setDatatable(DatatableAdapter.getById(result.getInt(++col)));
                dt.setImageblobslnk(result.getInt(++col));
                dt.setScoreslnk(result.getInt(++col));
                dt.setAccesslnk(result.getInt(++col));
                Array Pnamesarray = result.getArray(++col);
                List<String> parnames = new ArrayList();
                if (Pnamesarray != null) {
                    String[] Pnamesar = (String[]) Pnamesarray.getArray();
                    for (int i = 0; i < Pnamesar.length; i++) {
                        parnames.add(Pnamesar[i]);
                    }
                }
                dt.setParnames(parnames);
                Array Pvaluesarray = result.getArray(++col);
                List<String> parvalues = new ArrayList();
                if (Pvaluesarray != null) {
                    String[] Pvaluesar = (String[]) Pvaluesarray.getArray();
                    for (int i = 0; i < Pvaluesar.length; i++) {
                        parvalues.add(Pvaluesar[i]);
                    }
                }
                dt.setParvalues(parvalues);
                dt.setModtime(result.getTimestamp(++col));
                list.add(dt);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    /**
     *
     * @param testlnk
     * @param referencelnk
     * @param selectedmodellnks
     * @param selectedmctoollnks
     * @param selectedversiontaglnks
     * @param selectedbeamlnks
     * @param selectedtargetlnks
     * @param selectedobservablelnks
     * @param selectedsecondarylnks
     * @param selectedreactionlnks
     * @return
     * @throws IOException
     */
    @Override
    public Set<Result> getSelected(
            Integer testlnk,
            Integer referencelnk,
            Set<Integer> selectedmodellnks,
            Set<Integer> selectedmctoollnks,
            Set<Integer> selectedversiontaglnks,
            Set<Integer> selectedbeamlnks,
            Set<Integer> selectedtargetlnks,
            Set<Integer> selectedobservablelnks,
            Set<Integer> selectedsecondarylnks,
            Set<Integer> selectedreactionlnks)
            throws IOException {
        DatatableServiceAdapterImpl DatatableAdapter = DatatableServiceAdapterImpl.getInstance();
        boolean AND = false;
        Set<Result> list = new HashSet<>();
        String sql = "SELECT TRID,TID,REFID,MODEL,MCTOOL,VERSIONTAG,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,IMAGEBLOBID,SCORE,ACCESS,PARNAMES,PARVALUES,MODTIME  FROM PUBLIC.RESULT ";
        if (testlnk != null && testlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE TID=" + testlnk;
            } else {
                sql = sql + " AND TID=" + testlnk;
            }
        }
        if (referencelnk != null && referencelnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE REFID=" + referencelnk;
            } else {
                sql = sql + " AND REFID=" + referencelnk;
            }
        }
        if (selectedmodellnks != null) {
            if (selectedmodellnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (model=";
                } else {
                    sql = sql + " and (model=";
                }
                for (Integer model : selectedmodellnks) {
                    if (first) {
                        sql = sql + model;
                        first = false;
                    } else {
                        sql = sql + " or model=" + model;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedmctoollnks != null) {
            if (selectedmctoollnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (mctool=";
                } else {
                    sql = sql + " and (mctool=";
                }
                for (Integer mctool : selectedmctoollnks) {
                    if (first) {
                        sql = sql + mctool;
                        first = false;
                    } else {
                        sql = sql + " or mctool=" + mctool;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedversiontaglnks != null) {
            if (selectedversiontaglnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (versiontag=";
                } else {
                    sql = sql + " and (versiontag=";
                }
                for (Integer versiontag : selectedversiontaglnks) {
                    if (first) {
                        sql = sql + versiontag;
                        first = false;
                    } else {
                        sql = sql + " or versiontag=" + versiontag;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedbeamlnks != null) {
            if (selectedbeamlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (beam=";
                } else {
                    sql = sql + " and (beam=";
                }
                for (Integer Beam : selectedbeamlnks) {
                    if (first) {
                        sql = sql + Beam;
                        first = false;
                    } else {
                        sql = sql + " or beam=" + Beam;
                    }
                }
                sql = sql + ")";
            }
        }

        if (selectedtargetlnks != null) {
            if (selectedtargetlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (target=";
                } else {
                    sql = sql + " and (target=";
                }
                for (Integer Target : selectedtargetlnks) {
                    if (first) {
                        sql = sql + Target;
                        first = false;
                    } else {
                        sql = sql + " or target=" + Target;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedobservablelnks != null) {
            if (selectedobservablelnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (observable=";
                } else {
                    sql = sql + " and (observable=";
                }
                for (Integer Observable : selectedobservablelnks) {
                    if (first) {
                        sql = sql + Observable;
                        first = false;
                    } else {
                        sql = sql + " or observable=" + Observable;
                    }
                }
                sql = sql + ")";
            }
        }

        if (selectedsecondarylnks != null) {
            if (selectedsecondarylnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (secondary=";
                } else {
                    sql = sql + " and (secondary=";
                }
                for (Integer Secondary : selectedsecondarylnks) {
                    if (first) {
                        sql = sql + Secondary;
                        first = false;
                    } else {
                        sql = sql + " or secondary=" + Secondary;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedreactionlnks != null) {
            if (selectedreactionlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (reaction=";
                } else {
                    sql = sql + " and (reaction=";
                }
                for (Integer Reaction : selectedreactionlnks) {
                    if (first) {
                        sql = sql + Reaction;
                        first = false;
                    } else {
                        sql = sql + " or reaction=" + Reaction;
                    }
                }
                sql = sql + ")";
            }
        }
        sql = sql + ";";
        //System.out.println(sql);

        try {
            getConnection();

        } catch (SQLException exc) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, exc);
        }
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();

        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while (result.next()) {
                Result dt = Result.create();
                int col = 1;
                dt.setTrid(result.getInt(col));
                dt.setTestlnk(result.getInt(++col));
                dt.setReferencelnk(result.getInt(++col));
                dt.setModellnk(result.getInt(++col));
                dt.setMctoollnk(result.getInt(++col));
                dt.setVersiontaglnk(result.getInt(++col));
                dt.setBeamlnk(result.getInt(++col));
                dt.setTargetlnk(result.getInt(++col));
                dt.setObservablelnk(result.getInt(++col));
                dt.setSecondarylnk(result.getInt(++col));
                dt.setReactionlnk(result.getInt(++col));
                dt.setDatatable(DatatableAdapter.getById(result.getInt(++col)));
                dt.setImageblobslnk(result.getInt(++col));
                dt.setScoreslnk(result.getInt(++col));
                dt.setAccesslnk(result.getInt(++col));
                Array Pnamesarray = result.getArray(++col);
                List<String> parnames = new ArrayList();
                if (Pnamesarray != null) {
                    String[] Pnamesar = (String[]) Pnamesarray.getArray();
                    for (int i = 0; i < Pnamesar.length; i++) {
                        parnames.add(Pnamesar[i]);
                    }
                }
                dt.setParnames(parnames);
                Array Pvaluesarray = result.getArray(++col);
                List<String> parvalues = new ArrayList();
                if (Pvaluesarray != null) {
                    String[] Pvaluesar = (String[]) Pvaluesarray.getArray();
                    for (int i = 0; i < Pvaluesar.length; i++) {
                        parvalues.add(Pvaluesar[i]);
                    }
                }
                dt.setParvalues(parvalues);
                dt.setModtime(result.getTimestamp(++col));
                list.add(dt);

            }

        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    /*
    @Override
    public List<Integer> getSelectedlnks(
            Integer testlnk,
            Integer referencelnk,
            Set<Integer> selectedbeamlnks,
            Set<Integer> selectedmcdetaillnks,
            Set<Integer> selectedtargetlnks,
            Set<Integer> selectedobservablelnks,
            Set<Integer> selectedsecondarylnks,
            Set<Integer> selectedreactionlnks,
            Set<Integer> selectedparticlelnks,
            Set<Integer> selectedmctoollnks,
            Set<Integer> selectedscoreslnks)
            throws IOException {
        DatatableServiceAdapterImpl DatatableAdapter = DatatableServiceAdapterImpl.getInstance();
        boolean AND = false;
        List<Integer> list = new ArrayList<>();
        String sql = "SELECT TRID FROM PUBLIC.RESULT ";
        if (testlnk != null && testlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE TID=" + testlnk;
            } else {
                sql = sql + " AND TID=" + testlnk;
            }
        }
        if (referencelnk != null && referencelnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE REFID=" + referencelnk;
            } else {
                sql = sql + " AND REFID=" + referencelnk;
            }
        }
        if (selectedbeamlnks != null) {
            if (selectedbeamlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (beam=";
                } else {
                    sql = sql + " and (beam=";
                }
                for (Integer Beam : selectedbeamlnks) {
                    if (first) {
                        sql = sql + Beam;
                        first = false;
                    } else {
                        sql = sql + " or beam=" + Beam;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedmcdetaillnks != null) {
            if (selectedmcdetaillnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (mcdtid=";
                } else {
                    sql = sql + " and (mcdtid=";
                }
                for (Integer mcdtid : selectedmcdetaillnks) {
                    if (first) {
                        sql = sql + mcdtid;
                        first = false;
                    } else {
                        sql = sql + " or mcdtid=" + mcdtid;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedtargetlnks != null) {
            if (selectedtargetlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (target=";
                } else {
                    sql = sql + " and (target=";
                }
                for (Integer Target : selectedtargetlnks) {
                    if (first) {
                        sql = sql + Target;
                        first = false;
                    } else {
                        sql = sql + " or target=" + Target;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedobservablelnks != null) {
            if (selectedobservablelnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (observable=";
                } else {
                    sql = sql + " and (observable=";
                }
                for (Integer Observable : selectedobservablelnks) {
                    if (first) {
                        sql = sql + Observable;
                        first = false;
                    } else {
                        sql = sql + " or observable=" + Observable;
                    }
                }
                sql = sql + ")";
            }
        }

        if (selectedsecondarylnks != null) {
            if (selectedsecondarylnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (secondary=";
                } else {
                    sql = sql + " and (secondary=";
                }
                for (Integer Secondary : selectedsecondarylnks) {
                    if (first) {
                        sql = sql + Secondary;
                        first = false;
                    } else {
                        sql = sql + " or secondary=" + Secondary;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedreactionlnks != null) {
            if (selectedreactionlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (reaction=";
                } else {
                    sql = sql + " and (reaction=";
                }
                for (Integer Reaction : selectedreactionlnks) {
                    if (first) {
                        sql = sql + Reaction;
                        first = false;
                    } else {
                        sql = sql + " or reaction=" + Reaction;
                    }
                }
                sql = sql + ")";
            }
        }

        if (selectedparticlelnks != null) {
            if (selectedparticlelnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (particle=";
                } else {
                    sql = sql + " and (particle=";
                }
                for (Integer Particle : selectedparticlelnks) {
                    if (first) {
                        sql = sql + Particle;
                        first = false;
                    } else {
                        sql = sql + " or particle=" + Particle;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedmctoollnks != null) {
            if (selectedmctoollnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (mctool=";
                } else {
                    sql = sql + " and (mctool=";
                }
                for (Integer Mctool : selectedmctoollnks) {
                    if (first) {
                        sql = sql + Mctool;
                        first = false;
                    } else {
                        sql = sql + " or mctool=" + Mctool;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedscoreslnks != null) {
            if (selectedscoreslnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (scores=";
                } else {
                    sql = sql + " and (scores=";
                }
                for (Integer Scores : selectedscoreslnks) {
                    if (first) {
                        sql = sql + Scores;
                        first = false;
                    } else {
                        sql = sql + " or scores=" + Scores;
                    }
                }
                sql = sql + ")";
            }
        }

        sql = sql + ";";
        //System.out.println(sql);
        try {
            getConnection();

        } catch (SQLException exc) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, exc);
        }
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();

        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while (result.next()) {
                list.add(result.getInt(1));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
     */
    @Override
    public List<Integer> getSelectedlnks(
            Integer testlnk,
            Integer referencelnk,
            Set<Integer> selectedbeamlnks,
            Set<Integer> selectedmodellnks,
            Set<Integer> selectedmctoollnks,
            Set<Integer> selectedversiontaglnks,
            Set<Integer> selectedtargetlnks,
            Set<Integer> selectedobservablelnks,
            Set<Integer> selectedsecondarylnks,
            Set<Integer> selectedreactionlnks,
            Set<Integer> selectedparticlelnks,
            Set<Integer> selectedscoreslnks,
            HashMap<String, HashSet<String>> selectedParValuePairs)
            throws IOException {
        DatatableServiceAdapterImpl DatatableAdapter = DatatableServiceAdapterImpl.getInstance();
        boolean AND = false;
        List<Integer> list = new ArrayList<>();
        String sql = "SELECT TRID FROM PUBLIC.RESULT ";
        if (testlnk != null && testlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE TID=" + testlnk;
            } else {
                sql = sql + " AND TID=" + testlnk;
            }
        }
        if (referencelnk != null && referencelnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE REFID=" + referencelnk;
            } else {
                sql = sql + " AND REFID=" + referencelnk;
            }
        }

        if (selectedbeamlnks != null) {
            if (selectedbeamlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (beam=";
                } else {
                    sql = sql + " and (beam=";
                }
                for (Integer Beam : selectedbeamlnks) {
                    if (first) {
                        sql = sql + Beam;
                        first = false;
                    } else {
                        sql = sql + " or beam=" + Beam;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedmodellnks != null) {
            if (selectedmodellnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (model=";
                } else {
                    sql = sql + " and (model=";
                }
                for (Integer model : selectedmodellnks) {
                    if (first) {
                        sql = sql + model;
                        first = false;
                    } else {
                        sql = sql + " or model=" + model;
                    }
                }
                sql = sql + ")";
            }
        }

        if (selectedmctoollnks != null) {
            if (selectedmctoollnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (mctool=";
                } else {
                    sql = sql + " and (mctool=";
                }
                for (Integer mctool : selectedmctoollnks) {
                    if (first) {
                        sql = sql + mctool;
                        first = false;
                    } else {
                        sql = sql + " or mctool=" + mctool;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedversiontaglnks != null) {
            if (selectedversiontaglnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (versiontag=";
                } else {
                    sql = sql + " and (versiontag=";
                }
                for (Integer versiontag : selectedversiontaglnks) {
                    if (first) {
                        sql = sql + versiontag;
                        first = false;
                    } else {
                        sql = sql + " or versiontag=" + versiontag;
                    }
                }
                sql = sql + ")";
            }
        }

        if (selectedtargetlnks != null) {
            if (selectedtargetlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (target=";
                } else {
                    sql = sql + " and (target=";
                }
                for (Integer Target : selectedtargetlnks) {
                    if (first) {
                        sql = sql + Target;
                        first = false;
                    } else {
                        sql = sql + " or target=" + Target;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedobservablelnks != null) {
            if (selectedobservablelnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (observable=";
                } else {
                    sql = sql + " and (observable=";
                }
                for (Integer Observable : selectedobservablelnks) {
                    if (first) {
                        sql = sql + Observable;
                        first = false;
                    } else {
                        sql = sql + " or observable=" + Observable;
                    }
                }
                sql = sql + ")";
            }
        }

        if (selectedsecondarylnks != null) {
            if (selectedsecondarylnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (secondary=";
                } else {
                    sql = sql + " and (secondary=";
                }
                for (Integer Secondary : selectedsecondarylnks) {
                    if (first) {
                        sql = sql + Secondary;
                        first = false;
                    } else {
                        sql = sql + " or secondary=" + Secondary;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedreactionlnks != null) {
            if (selectedreactionlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (reaction=";
                } else {
                    sql = sql + " and (reaction=";
                }
                for (Integer Reaction : selectedreactionlnks) {
                    if (first) {
                        sql = sql + Reaction;
                        first = false;
                    } else {
                        sql = sql + " or reaction=" + Reaction;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedparticlelnks != null) {
            if (selectedparticlelnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (particle=";
                } else {
                    sql = sql + " and (particle=";
                }
                for (Integer Particle : selectedparticlelnks) {
                    if (first) {
                        sql = sql + Particle;
                        first = false;
                    } else {
                        sql = sql + " or particle=" + Particle;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedmctoollnks != null) {
            if (selectedmctoollnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (mctool=";
                } else {
                    sql = sql + " and (mctool=";
                }
                for (Integer Mctool : selectedmctoollnks) {
                    if (first) {
                        sql = sql + Mctool;
                        first = false;
                    } else {
                        sql = sql + " or mctool=" + Mctool;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedscoreslnks != null) {
            if (selectedscoreslnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (scores=";
                } else {
                    sql = sql + " and (scores=";
                }
                for (Integer Scores : selectedscoreslnks) {
                    if (first) {
                        sql = sql + Scores;
                        first = false;
                    } else {
                        sql = sql + " or scores=" + Scores;
                    }
                }
                sql = sql + ")";
            }
        }
        // Now deal with the par name/value pairs
        if (selectedParValuePairs != null) {
            Iterator it = selectedParValuePairs.entrySet().iterator();
            boolean first = true;
            while (it.hasNext()) {
                HashMap.Entry pair = (HashMap.Entry) it.next();
                if (!AND) {
                    AND = true;
                    sql = sql + " WHERE (" + "'" + pair.getKey() + "' = ANY(PARNAMES)";
                } else {
                    sql = sql + " AND ('" + pair.getKey() + "' = ANY(PARNAMES)";
                }
                sql = sql + ")";
                HashSet assetSet = (HashSet) pair.getValue();
                Iterator setIterator = assetSet.iterator();
                boolean firt = true;
                while (setIterator.hasNext()) {
                    String item = (String) setIterator.next();
                    if (firt) {
                        sql = sql + " AND (('" + item + "' = ANY(PARVALUES))";
                        firt = false;
                    } else {
                        sql = sql + " OR ('" + item + "' = ANY(PARVALUES))";
                    }
                }
                it.remove(); // avoids a ConcurrentModificationException
                sql = sql + ")";
            }
        }
        sql = sql + ";";
        System.out.println(sql);
        try {
            getConnection();
        } catch (SQLException exc) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, exc);
        }
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while (result.next()) {
                list.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public List<Integer> getSelectedlnks1(
            Integer testlnk,
            Integer referencelnk,
            Set<Integer> selectedaccesslnks,
            Set<Integer> selectedbeamlnks,
            Set<Integer> selectedmodellnks,
            Set<Integer> selectedmctoollnks,
            Set<Integer> selectedversiontaglnks,
            Set<Integer> selectedtargetlnks,
            Set<Integer> selectedobservablelnks,
            Set<Integer> selectedsecondarylnks,
            Set<Integer> selectedreactionlnks,
            Set<Integer> selectedparticlelnks,
            Set<Integer> selectedscoreslnks,
            HashMap<String, HashSet<String>> selectedParValuePairs)
            throws IOException {
        
        DatatableServiceAdapterImpl DatatableAdapter = DatatableServiceAdapterImpl.getInstance();
        boolean AND = false;
        List<Integer> list = new ArrayList<>();
        String sql = "SELECT TRID FROM PUBLIC.RESULT ";
        System.out.println(sql);
        if (testlnk != null && testlnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE TID=" + testlnk;
            } else {
                sql = sql + " AND TID=" + testlnk;
            }
        }
        if (referencelnk != null && referencelnk != 0) {
            if (!AND) {
                AND = true;
                sql = sql + " WHERE REFID=" + referencelnk;
            } else {
                sql = sql + " AND REFID=" + referencelnk;
            }
        }
        if (selectedaccesslnks != null) {
            if (selectedaccesslnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (access=";
                } else {
                    sql = sql + " and (access=";
                }
                for (Integer Access : selectedaccesslnks) {
                    if (first) {
                        sql = sql + Access;
                        first = false;
                    } else {
                        sql = sql + " or access=" + Access;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedbeamlnks != null) {
            if (selectedbeamlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (beam=";
                } else {
                    sql = sql + " and (beam=";
                }
                for (Integer Beam : selectedbeamlnks) {
                    if (first) {
                        sql = sql + Beam;
                        first = false;
                    } else {
                        sql = sql + " or beam=" + Beam;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedmodellnks != null) {
            if (selectedmodellnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (model=";
                } else {
                    sql = sql + " and (model=";
                }
                for (Integer model : selectedmodellnks) {
                    if (first) {
                        sql = sql + model;
                        first = false;
                    } else {
                        sql = sql + " or model=" + model;
                    }
                }
                sql = sql + ")";
            }
        }

        if (selectedmctoollnks != null) {
            if (selectedmctoollnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (mctool=";
                } else {
                    sql = sql + " and (mctool=";
                }
                for (Integer mctool : selectedmctoollnks) {
                    if (first) {
                        sql = sql + mctool;
                        first = false;
                    } else {
                        sql = sql + " or mctool=" + mctool;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedversiontaglnks != null) {
            if (selectedversiontaglnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (versiontag=";
                } else {
                    sql = sql + " and (versiontag=";
                }
                for (Integer versiontag : selectedversiontaglnks) {
                    if (first) {
                        sql = sql + versiontag;
                        first = false;
                    } else {
                        sql = sql + " or versiontag=" + versiontag;
                    }
                }
                sql = sql + ")";
            }
        }

        if (selectedtargetlnks != null) {
            if (selectedtargetlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (target=";
                } else {
                    sql = sql + " and (target=";
                }
                for (Integer Target : selectedtargetlnks) {
                    if (first) {
                        sql = sql + Target;
                        first = false;
                    } else {
                        sql = sql + " or target=" + Target;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedobservablelnks != null) {
            if (selectedobservablelnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (observable=";
                } else {
                    sql = sql + " and (observable=";
                }
                for (Integer Observable : selectedobservablelnks) {
                    if (first) {
                        sql = sql + Observable;
                        first = false;
                    } else {
                        sql = sql + " or observable=" + Observable;
                    }
                }
                sql = sql + ")";
            }
        }

        if (selectedsecondarylnks != null) {
            if (selectedsecondarylnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (secondary=";
                } else {
                    sql = sql + " and (secondary=";
                }
                for (Integer Secondary : selectedsecondarylnks) {
                    if (first) {
                        sql = sql + Secondary;
                        first = false;
                    } else {
                        sql = sql + " or secondary=" + Secondary;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedreactionlnks != null) {
            if (selectedreactionlnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (reaction=";
                } else {
                    sql = sql + " and (reaction=";
                }
                for (Integer Reaction : selectedreactionlnks) {
                    if (first) {
                        sql = sql + Reaction;
                        first = false;
                    } else {
                        sql = sql + " or reaction=" + Reaction;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedparticlelnks != null) {
            if (selectedparticlelnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (particle=";
                } else {
                    sql = sql + " and (particle=";
                }
                for (Integer Particle : selectedparticlelnks) {
                    if (first) {
                        sql = sql + Particle;
                        first = false;
                    } else {
                        sql = sql + " or particle=" + Particle;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedmctoollnks != null) {
            if (selectedmctoollnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (mctool=";
                } else {
                    sql = sql + " and (mctool=";
                }
                for (Integer Mctool : selectedmctoollnks) {
                    if (first) {
                        sql = sql + Mctool;
                        first = false;
                    } else {
                        sql = sql + " or mctool=" + Mctool;
                    }
                }
                sql = sql + ")";
            }
        }
        if (selectedscoreslnks != null) {
            if (selectedscoreslnks.size() > 0) {
                boolean first = true;
                if (!AND) {
                    AND = true;
                    sql = sql + " where (scores=";
                } else {
                    sql = sql + " and (scores=";
                }
                for (Integer Scores : selectedscoreslnks) {
                    if (first) {
                        sql = sql + Scores;
                        first = false;
                    } else {
                        sql = sql + " or scores=" + Scores;
                    }
                }
                sql = sql + ")";
            }
        }
        // Now deal with the par name/value pairs
        if (selectedParValuePairs != null) {
            Iterator it = selectedParValuePairs.entrySet().iterator();
            boolean first = true;
            while (it.hasNext()) {
                HashMap.Entry pair = (HashMap.Entry) it.next();
                if (!AND) {
                    AND = true;
                    sql = sql + " WHERE (" + "'" + pair.getKey() + "' = ANY(PARNAMES)";
                } else {
                    sql = sql + " AND ('" + pair.getKey() + "' = ANY(PARNAMES)";
                }
                sql = sql + ")";
                HashSet assetSet = (HashSet) pair.getValue();
                Iterator setIterator = assetSet.iterator();
                boolean firt = true;
                while (setIterator.hasNext()) {
                    String item = (String) setIterator.next();
                    if (firt) {
                        sql = sql + " AND (('" + item + "' = ANY(PARVALUES))";
                        firt = false;
                    } else {
                        sql = sql + " OR ('" + item + "' = ANY(PARVALUES))";
                    }
                }
                it.remove(); // avoids a ConcurrentModificationException
                sql = sql + ")";
            }
        }
        sql = sql + ";";
        System.out.println("==========================================================================");
        System.out.println(sql);
        System.out.println("==========================================================================");
        try {
            getConnection();
        } catch (SQLException exc) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, exc);
        }
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while (result.next()) {
                list.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public Set<Integer> getModelsbyTest(Integer testid, Integer access) {

        Set<Integer> ModelList = new HashSet();
        String sql = "SELECT DISTINCT MODEL FROM PUBLIC.RESULT WHERE ACCESS=? AND TID=? ORDER BY MODEL;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, access);
            ps.setInt(2, testid);
            result = ps.executeQuery();
            while (result.next()) {
                ModelList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return ModelList;
    }

    @Override
    public Set<Integer> getMctoolsbyTest(Integer testid, Integer access) {
        Set<Integer> MctoolList = new HashSet();
        String sql = "SELECT DISTINCT MCTOOL FROM PUBLIC.RESULT WHERE ACCESS=? AND TID=? ORDER BY MCTOOL;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, access);
            ps.setInt(2, testid);
            result = ps.executeQuery();
            while (result.next()) {
                MctoolList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return MctoolList;
    }

    @Override
    public Set<Integer> getVersiontagsbyTest(Integer testid, Integer access) {

        Set<Integer> VersiontagList = new HashSet();
        String sql = "SELECT DISTINCT VERSIONTAG FROM PUBLIC.RESULT WHERE ACCESS=? AND TID=? ORDER BY VERSIONTAG;";
        PreparedStatement ps;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, access);
            ps.setInt(2, testid);
            result = ps.executeQuery();
            while (result.next()) {
                VersiontagList.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return VersiontagList;
    }

}
