/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;

/**
 *
 * @author wenzel
 */
public class Scores implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private int scoreid;
    private String score;
    private String stype;
    /**
     *
     */
    private Scores() {
    }

    public static Scores create() {
        return new Scores();
    }

    public static Scores create(int scoreid) {
        return new Scores(scoreid);
    }
   public static Scores create(int scoreid, String score, String stype) {
        return new Scores(scoreid, score, stype);
    }
    public static Scores create(String jsonstr)  {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Scores.class);
    }



    /**
     *
     * @param scoreid
     */
    private Scores(int scoreid) {
        this.scoreid = scoreid;
    }

    /**
     *
     * @param scoreid
     * @param score
     * @param stype
     */
    private Scores(int scoreid, String score, String stype) {
        this.scoreid = scoreid;
        this.score = score;
        this.stype = stype;
    }

    /**
     *
     * @return
     */
    public int getScoreid() {
        return this.scoreid;
    }

    /**
     *
     * @param scoreid
     */
    public void setScoreid(int scoreid) {
        this.scoreid = scoreid;
    }

    /**
     *
     * @return
     */
    public String getScore() {
        return this.score;
    }

    /**
     *
     * @param score
     */
    public void setScore(String score) {
        this.score = score;
    }

    /**
     *
     * @return
     */
    public String getStype() {
        return this.stype;
    }

    /**
     *
     * @param stype
     */
    public void setStype(String stype) {
        this.stype = stype;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }
}
