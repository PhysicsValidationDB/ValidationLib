/*
---------------------------------------------------------------
     ___      ___ ___ _ ___ ___ 
    |   \ ___/ __/ __(_) __| _ \
    | |) / _ \__ \__ \ | _||   /
    |___/\___/___/___/_|___|_|_\
    
    Database of Scientific Simulated and Experimental Results
---------------------------------------------------------------
 */
//
// This class represents a 1D histogram as extracted from the Datatable
//
package ValidationLib;

import java.util.List;

/**
 *
 * @author wenzel
 */
public class Histogram1D implements java.io.Serializable {
    private String Title;
    private List<Bin1D> bins;
    
    public Histogram1D(String tit,List<Bin1D> lbins) {
        this.Title = tit;
        this.bins=lbins;
    }

    /**
     * @return the Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param Title the Title to set
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * @return the bins
     */
    public List<Bin1D> getBins() {
        return bins;
    }

    /**
     * @param bins the bins to set
     */
    public void setBins(List<Bin1D> bins) {
        this.bins = bins;
    }
}
