/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class MctoolServiceAdapterImpl extends ServiceAdapter implements MctoolServiceAdapter {
    private static MctoolServiceAdapterImpl instance;

    /**
     *
     */
    private MctoolServiceAdapterImpl() {
    }

    public static MctoolServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new MctoolServiceAdapterImpl();
        }
        return instance;
    }
    /**
     *
     * @param id
     * @return
     */
    @Override
    public Mctool getById(Integer id) {
        String sql = "SELECT MCNAME FROM PUBLIC.MCTOOL WHERE MCID=? ;";
        PreparedStatement ps;
        Mctool dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Mctool.create(id, result.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MctoolServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Mctool> getAll() {

        String sql = "SELECT MCID,MCNAME FROM PUBLIC.MCTOOL ORDER BY MCID;";
        List<Mctool> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                list.add(Mctool.create(result.getInt(1), result.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MctoolServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    /**
     *
     * @param dt
     * @return
     */
    @Override
    public int insert(Mctool dt) {
        int executeUpdate = 0;
        try {
            String sql = "INSERT INTO PUBLIC.MCTOOL (MCNAME) VALUES (?);";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getMcname());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MctoolServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    /**
     *
     * @param dt
     * @return
     */
    @Override
    public int update(Mctool dt) {
        int executeUpdate = 0;
        try {
            String sql = "UPDATE PUBLIC.MCTOOL SET MCNAME=? WHERE MCID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getMcname());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MctoolServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    /**
     *
     * @param dt
     * @return
     */
    @Override
    public int delete(Mctool dt) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.MCTOOL WHERE MCID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getMcid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MctoolServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

}
