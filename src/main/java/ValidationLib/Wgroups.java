/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;

/**
 *
 * @author wenzel
 */
public class Wgroups implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private Integer wgid;
    private String wgname;

    public static Wgroups create() {
        return new Wgroups();
    }
    public static Wgroups create(Integer wgid, String wgname) {
        return new Wgroups(wgid,wgname);
    }
    public static Wgroups create(String jsonstr) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Wgroups.class);
    }

    /**
     *
     */
    private Wgroups() {
    }

    /**
     *
     * @param wgid
     * @param wgname
     */
    private Wgroups(Integer wgid, String wgname) {
        this.wgid = wgid;
        this.wgname = wgname;
    }

    /**
     *
     * @return
     */
    public int getWgid() {
        return this.wgid;
    }

    /**
     *
     * @param wgid
     */
    public void setWgid(Integer wgid) {
        this.wgid = wgid;
    }

    /**
     *
     * @return
     */
    public String getWgname() {
        return this.wgname;
    }

    /**
     *
     * @param wgname
     */
    public void setWgname(String wgname) {
        this.wgname = wgname;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }
}
