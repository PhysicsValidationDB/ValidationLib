/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class ReactionServiceAdapterImpl extends ServiceAdapter implements ReactionServiceAdapter {
    private static ReactionServiceAdapterImpl instance;

    /**
     *
     */
    private ReactionServiceAdapterImpl() {
    }

    public static ReactionServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new ReactionServiceAdapterImpl();
        }
        return instance;
    }
    /**
     *
     * @param id
     * @return
     */
    @Override
    public Reaction getById(Integer id) {
        String sql = "SELECT RNAME FROM PUBLIC.REACTION WHERE RID=? ;";
        PreparedStatement ps;
        Reaction dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Reaction.create(id, result.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReactionServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Reaction> getAll() {

        String sql = "SELECT RID,RNAME FROM PUBLIC.REACTION ORDER BY RID;";
        List<Reaction> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                list.add(Reaction.create(result.getInt(1), result.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReactionServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    /**
     *
     * @param dt
     * @return
     */
    @Override
    public int insert(Reaction dt) {
        int executeUpdate = 0;
        try {
            String sql = "INSERT INTO PUBLIC.REACTION (RID,RNAME) VALUES (?,?);";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getRid());
            ps.setString(2, dt.getRname());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReactionServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    /**
     *
     * @param dt
     * @return
     */
    @Override
    public int update(Reaction dt) {
        int executeUpdate = 0;
        try {
            String sql = "UPDATE PUBLIC.REACTION SET RNAME=? WHERE RID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getRname());
            ps.setInt(2, dt.getRid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReactionServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    /**
     *
     * @param dt
     * @return
     */
    @Override
    public int delete(Reaction dt) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.REACTION WHERE RID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getRid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReactionServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

}
