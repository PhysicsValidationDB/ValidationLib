/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;

/**
 *
 * @author wenzel
 */
public class Observable implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private int oid;
    private String oname;
    /**
     *
     */
    private Observable() {
    }

    public static Observable create() {
        return new Observable();
    }

    public static Observable create(int oid, String oname) {
        return new Observable(oid, oname);
    }

    public static Observable create(String jsonstr)  {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Observable.class);
    }

    /**
     *
     * @param oid
     */
    private Observable(int oid) {
        this.oid = oid;
    }

    /**
     *
     * @param oid
     * @param oname
     */
    private Observable(int oid, String oname) {
        this.oid = oid;
        this.oname = oname;
    }

    /**
     *
     * @return
     */
    public int getOid() {
        return this.oid;
    }

    /**
     *
     * @param oid
     */
    public void setOid(int oid) {
        this.oid = oid;
    }

    /**
     *
     * @return
     */
    public String getOname() {
        return this.oname;
    }

    /**
     *
     * @param oname
     */
    public void setOname(String oname) {
        this.oname = oname;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }
}
