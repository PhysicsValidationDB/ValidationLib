/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------
 */
//
//
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;

/**
 *
 * @author wenzel
 */
public class Access implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private transient int accessid;
    private String access;

    /**
     *
     */
    private Access() {
    }

    public static Access create() {
        return new Access();
    }

    public static Access create(int accessid, String access) {
        return new Access(accessid, access);
    }

    public static Access create(String jsonstr)  {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Access.class);
    }

    /**
     *
     * @param accessid
     * @param access
     */
    private Access(int accessid, String access) {
        this.accessid = accessid;
        this.access = access;
    }

    /**
     *
     * @return
     */
    public int getAccessid() {
        return this.accessid;
    }

    /**
     *
     * @param accessid
     */
    public void setAccessid(int accessid) {
        this.accessid = accessid;
    }

    /**
     *
     * @return
     */
    public String getAccess() {
        return this.access;
    }

    /**
     *
     * @param access
     */
    public void setAccess(String access) {
        this.access = access;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }
}
