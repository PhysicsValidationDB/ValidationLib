/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
/**
 *
 * @author wenzel
 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class Datatable implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private transient int dtid;
    private transient Integer datatypeslnk;
    private String title;
    private Integer npoints;
    private List<Integer> nbins;
    private List<String> axisTitle;
    private List<Float> val;
    private List<Float> errStatPlus;
    private List<Float> errStatMinus;
    private List<Float> errSysPlus;
    private List<Float> errSysMinus;
    private List<Float> binMin;
    private List<Float> binMax;
    //
    private String datatypeskw;

    /**
     *
     */
    private Datatable() {
    }

    public static Datatable create() {
        return new Datatable();
    }

    public static Datatable create(int dtid, Integer datatypeslnk, String datatypeskw, String title, Integer npoints, List<Integer> nbins, List<String> axisTitle, List<Float> val, List<Float> errStatPlus, List<Float> errStatMinus, List<Float> errSysPlus, List<Float> errSysMinus, List<Float> binMin, List<Float> binMax) {
        return new Datatable(dtid, datatypeslnk, datatypeskw, title, npoints, nbins, axisTitle, val, errStatPlus, errStatMinus, errSysPlus, errSysMinus, binMin, binMax);
    }

    public Datatable create(String jsonstr) throws IOException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                //                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        System.out.println("111111111111111111111111111111111111111111111111111111111111111111");
        System.out.println(jsonstr);
        System.out.println("111111111111111111111111111111111111111111111111111111111111111111");
        DictionaryService ds = DictionaryService.getInstance();
        Datatable dt = gson.fromJson(jsonstr, Datatable.class);
        if (ds.getDatatypesMap().containsKey(datatypeskw)) {
            dt.setDatatypeslnk(ds.getDatatypesMap().get(datatypeskw));
        } else {
            throw new IllegalArgumentException("There is no Datatypes dictionary entry for key: " + datatypeskw);
        }

        return dt;

    }

    /**
     *
     * @param dtid
     */
    //private Datatable(int dtid) {
    //    this.dtid = dtid;
    //}
    /**
     *
     * @param dtid
     * @param datatypeslnk
     * @param title
     * @param npoints
     * @param nbins
     * @param axisTitle
     * @param val
     * @param errStatPlus
     * @param errStatMinus
     * @param errSysPlus
     * @param errSysMinus
     * @param binMin
     * @param binMax
     */
    private Datatable(int dtid, Integer datatypeslnk, String datatypeskw, String title, Integer npoints, List<Integer> nbins, List<String> axisTitle, List<Float> val, List<Float> errStatPlus, List<Float> errStatMinus, List<Float> errSysPlus, List<Float> errSysMinus, List<Float> binMin, List<Float> binMax) {
        this.dtid = dtid;
        this.datatypeslnk = datatypeslnk;
        this.datatypeskw = datatypeskw;
        this.title = title;
        this.npoints = npoints;
        this.nbins = nbins;
        this.axisTitle = axisTitle;
        this.val = val;
        this.errStatPlus = errStatPlus;
        this.errStatMinus = errStatMinus;
        this.errSysPlus = errSysPlus;
        this.errSysMinus = errSysMinus;
        this.binMin = binMin;
        this.binMax = binMax;

    }

    public Histogram1D getHistogram1D() {
        if (datatypeslnk == 1 || datatypeslnk == 2) {
            ArrayList<Bin1D> lobins = new ArrayList<Bin1D>();
            if (errStatPlus.size() == nbins.get(0) && errSysPlus.size() == nbins.get(0)) {
                for (int i = 0; i < nbins.get(0); i++) {
                    lobins.add(new Bin1D(binMin.get(i), binMax.get(i),
                            val.get(i),
                            errStatPlus.get(i), errStatMinus.get(i),
                            errSysPlus.get(i), errSysMinus.get(i)
                    ));
                }
            } else if (errStatPlus.size() == nbins.get(0) && errSysPlus.size() != nbins.get(0)) {
                for (int i = 0; i < nbins.get(0); i++) {
                    float syserr = 0;
                    lobins.add(new Bin1D(binMin.get(i), binMax.get(i),
                            val.get(i),
                            errStatPlus.get(i), errStatMinus.get(i), syserr, syserr));
                }
            } else if (errStatPlus.size() != nbins.get(0) && errSysPlus.size() == nbins.get(0)) {
                for (int i = 0; i < nbins.get(0); i++) {
                    float staterr = 0;
                    lobins.add(new Bin1D(binMin.get(i), binMax.get(i),
                            val.get(i),
                            staterr, staterr, errSysPlus.get(i), errSysMinus.get(i)));
                }
            } else {
                for (int i = 0; i < nbins.get(0); i++) {
                    float syserr = 0;
                    float staterr = 0;
                    lobins.add(new Bin1D(binMin.get(i), binMax.get(i),
                            val.get(i),
                            staterr, staterr, syserr, syserr));
                }
            }
            return new Histogram1D(title, lobins);
        } else {
            return null;
        }
    }

    public List<DataPoint2D> getListofDataPoint2D() {
        ArrayList lst = new ArrayList<DataPoint2D>();
        if (datatypeslnk == 1000 || datatypeslnk == 1001) {
            for (int i = 0; i < npoints; i++) {
                lst.add(new DataPoint2D(val.get(i), val.get(npoints + i),
                        errStatPlus.get(i), errStatPlus.get(npoints + i)));
            }
            return lst;
        } else {
            return null;
        }

    }

    /**
     *
     * @return
     */
    public int getDtid() {
        return this.dtid;
    }

    /**
     *
     * @param dtid
     */
    public void setDtid(int dtid) {
        this.dtid = dtid;
    }

    /**
     *
     * @return
     */
    public String getTitle() {
        return this.title;
    }

    /**
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     */
    public Integer getNpoints() {
        return this.npoints;
    }

    /**
     *
     * @param npoints
     */
    public void setNpoints(Integer npoints) {
        this.npoints = npoints;
    }

    /**
     *
     * @return
     */
    public List<Integer> getNbins() {
        return this.nbins;
    }

    /**
     *
     * @param nbins
     */
    public void setNbins(List<Integer> nbins) {
        this.nbins = nbins;
    }

    /**
     *
     * @return
     */
    public List<String> getAxisTitle() {
        return this.axisTitle;
    }

    /**
     *
     * @param axisTitle
     */
    public void setAxisTitle(List<String> axisTitle) {
        this.axisTitle = axisTitle;
    }

    /**
     *
     * @return
     */
    public List<Float> getVal() {
        return this.val;
    }

    /**
     *
     * @param val
     */
    public void setVal(List<Float> val) {
        this.val = val;
    }

    /**
     *
     * @return
     */
    public List<Float> getErrStatPlus() {
        return this.errStatPlus;
    }

    /**
     *
     * @param errStatPlus
     */
    public void setErrStatPlus(List<Float> errStatPlus) {
        this.errStatPlus = errStatPlus;
    }

    /**
     *
     * @return
     */
    public List<Float> getErrStatMinus() {
        return this.errStatMinus;
    }

    /**
     *
     * @param errStatMinus
     */
    public void setErrStatMinus(List<Float> errStatMinus) {
        this.errStatMinus = errStatMinus;
    }

    /**
     *
     * @return
     */
    public List<Float> getErrSysPlus() {
        return this.errSysPlus;
    }

    /**
     *
     * @param errSysPlus
     */
    public void setErrSysPlus(List<Float> errSysPlus) {
        this.errSysPlus = errSysPlus;
    }

    /**
     *
     * @return
     */
    public List<Float> getErrSysMinus() {
        return this.errSysMinus;
    }

    /**
     *
     * @param errSysMinus
     */
    public void setErrSysMinus(List<Float> errSysMinus) {
        this.errSysMinus = errSysMinus;
    }

    /**
     *
     * @return
     */
    public List<Float> getBinMin() {
        return this.binMin;
    }

    /**
     *
     * @param binMin
     */
    public void setBinMin(List<Float> binMin) {
        this.binMin = binMin;
    }

    /**
     *
     * @return
     */
    public List<Float> getBinMax() {
        return this.binMax;
    }

    /**
     *
     * @param binMax
     */
    public void setBinMax(List<Float> binMax) {
        this.binMax = binMax;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                //.excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }

    /**
     * @return the datatypeslnk
     */
    public Integer getDatatypeslnk() {
        return datatypeslnk;
    }

    /**
     * @param datatypeslnk the datatypeslnk to set
     */
    public void setDatatypeslnk(Integer datatypeslnk) {
        this.datatypeslnk = datatypeslnk;
    }

    /**
     * @return the datatypeskw
     */
    public String getDatatypeskw() {
        return datatypeskw;
    }

    /**
     * @param datatypeskw the datatypeskw to set
     */
    public void setDatatypeskw(String datatypeskw) {
        this.datatypeskw = datatypeskw;
    }
}
