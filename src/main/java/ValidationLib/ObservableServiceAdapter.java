/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;
import java.util.List;

/**
 *
 * @author wenzel
 */
public interface ObservableServiceAdapter {

    /**
     *
     * @param id
     * @return
     */
    public Observable getById(Integer id);

    /**
     *
     * @return
     */
    public List<Observable> getAll();

    /**
     *
     * @param mcdt
     * @return 
     */
    public int insert(Observable mcdt);

    /**
     *
     * @param mcdt
     * @return 
     */
    public int update(Observable mcdt);

    /**
     *
     * @param mcdt
     * @return 
     */
    public int delete(Observable mcdt);

}
