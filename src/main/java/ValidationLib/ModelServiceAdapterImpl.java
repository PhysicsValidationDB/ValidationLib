/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class ModelServiceAdapterImpl extends ServiceAdapter implements ModelServiceAdapter {
    private static ModelServiceAdapterImpl instance;

    /**
     *
     */
    private ModelServiceAdapterImpl() {
    }

    public static ModelServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new ModelServiceAdapterImpl();
        }
        return instance;
    }
    /**
     *
     * @param id
     * @return
     */
    @Override
    public Model getById(Integer id) {
        String sql = "SELECT NAME FROM PUBLIC.MODEL WHERE MODELID=? ;";
        PreparedStatement ps;
        Model dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Model.create(id, result.getString(1), false);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ModelServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Model> getAll() {

        String sql = "SELECT MODELID,NAME FROM PUBLIC.MODEL ORDER BY MODELID;";
        List<Model> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                list.add(Model.create(result.getInt(1), result.getString(2), false));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ModelServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    /**
     *
     * @param dt
     * @return
     */
    @Override
    public int insert(Model dt) {
        int executeUpdate = 0;
        try {
            String sql = "INSERT INTO PUBLIC.MODEL (NAME) VALUES (?);";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getModelname());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ModelServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    /**
     *
     * @param dt
     * @return
     */
    @Override
    public int update(Model dt) {
        int executeUpdate = 0;
        try {
            String sql = "UPDATE PUBLIC.MODEL SET NAME=? WHERE MODELID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getModelname());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ModelServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    /**
     *
     * @param dt
     * @return
     */
    @Override
    public int delete(Model dt) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.MODEL WHERE MODELID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getModelid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ModelServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

}
