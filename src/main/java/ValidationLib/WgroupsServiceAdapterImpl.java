/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class WgroupsServiceAdapterImpl extends ServiceAdapter implements WgroupsServiceAdapter {
    private static WgroupsServiceAdapterImpl instance;

    /**
     *
     */
    private WgroupsServiceAdapterImpl() {
    }

    public static WgroupsServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new WgroupsServiceAdapterImpl();
        }
        return instance;
    }
    @Override
    public Wgroups getById(Integer id) {
        String sql = "SELECT WGNAME FROM PUBLIC.WGROUPS WHERE WGID=? ;";
        PreparedStatement ps;
        Wgroups dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Wgroups.create(id, result.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WgroupsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Wgroups> getAll() {

        String sql = "SELECT WGID,WGNAME FROM PUBLIC.WGROUPS ORDER BY WGID;";
        List<Wgroups> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                list.add(Wgroups.create(result.getInt(1), result.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WgroupsServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    @Override
    public int insert(Wgroups dt) {
        int executeUpdate = 0;
        try {
            String sql = "INSERT INTO PUBLIC.WGROUPS (WGNAME) VALUES (?);";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getWgname());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(WgroupsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int update(Wgroups dt) {
        int executeUpdate = 0;
        try {
            String sql = "UPDATE PUBLIC.WGROUPS SET WGNAME=? WHERE WGID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getWgname());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(WgroupsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int delete(Wgroups dt) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.WGROUPS WHERE WGID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getWgid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(WgroupsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

}
