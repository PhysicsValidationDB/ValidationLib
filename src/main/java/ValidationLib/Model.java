/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;

/**
 *
 * @author wenzel
 */
public class Model implements java.io.Serializable {

    private static long serialVersionUID = 1;
    private Integer modelid;
    private String  modelname ;              // modelname  of the model
    private boolean isTransient;
    /**
     *
     */
    private Model() {
    }

    public static Model create() {
        return new Model();
    }

    public static Model create(Integer modelid, String modelname , boolean isTransient) {
        return new Model(modelid, modelname , isTransient);
    }

    public static Model create(String jsonstr){
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Model.class);
    }

    /**
     *
     * @param modelid
     * @param modelname 
     */
    private Model(Integer modelid, String modelname , boolean isTransient) {
        this.modelid = modelid;
        this.modelname  = modelname ;
        this.isTransient = isTransient;
    }

    

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }

    /**
     * @return the modelid
     */
    public Integer getModelid() {
        return modelid;
    }

    /**
     * @param modelid the modelid to set
     */
    public void setModelid(Integer modelid) {
        this.modelid = modelid;
    }

    /**
     * @return the modelname
     */
    public String getModelname() {
        return modelname;
    }

    /**
     * @param modelname the modelname to set
     */
    public void setModelname(String modelname) {
        this.modelname = modelname;
    }



}
