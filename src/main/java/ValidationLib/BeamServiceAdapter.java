/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.util.List;

/**
 *
 * @author wenzel
 */
public interface BeamServiceAdapter {

    /**
     *
     * @param id
     * @return
     */
    public Beam getById(Integer id);

    /**
     *
     * @return
     */
    public List<Beam> getAll();

    /**
     *
     * @param dt
     * @return
     */
    public int insert(Beam dt);

    /**
     *
     * @param dt
     * @return
     */
    public int update(Beam dt);

    /**
     *
     * @param dt
     * @return
     */
    public int delete(Beam dt);

    public List<Integer> getBeamsInRange(
            Integer lowerBound,
            Integer upperBound);
}
