/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 *
 * Class retrieving or acting on a TEST Result (Experiment,Genie,Geant4....) in
 * the database. A Result is always linked to a datatable or imageblob therefore
 * this class deals with creating and deleting this classes too. Database
 * transactions are used to ensure things are done in the right order and are
 * rolled back if any step in the transaction chain fails. This class is
 * implemented as a Lazy Initialization Singleton.
 *
 * @author wenzel
 */
public class Result_expandedServiceAdapterImpl extends ServiceAdapter implements Result_expandedServiceAdapter {

    private static Result_expandedServiceAdapterImpl instance;

    private Result_expandedServiceAdapterImpl() {
    }

    public static Result_expandedServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new Result_expandedServiceAdapterImpl();
        }
        return instance;
    }

    @Override
    public Result_expanded getById(Integer id) {
        String sql = "SELECT TID,REFID,MODEL,MCTOOL,VERSIONTAG,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,IMAGEBLOBID,SCORE,ACCESS,PARNAMES,PARVALUES,MODTIME  FROM PUBLIC.RESULT WHERE TRID=?;";
        DatatableServiceAdapterImpl DatatableAdapter = DatatableServiceAdapterImpl.getInstance();
//

        //
        PreparedStatement ps;
        Result_expanded dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Result_expanded.create();
                dt.setTrid(id);
                int col = 1;
                dt.setTestlnk(result.getInt(col));
                if (result.getInt(col) != 0) {
                    dt.setTest(TestServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                }
                col++;
                dt.setReferencelnk(result.getInt(col));
                if (result.getInt(col) != 0) {
                    dt.setReference(ReferenceServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                }
                col++;
                dt.setModellnk(result.getInt(col));
                if (result.getInt(col) != 0) {
                    dt.setModel(ModelServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                }
                col++;
                dt.setMctoollnk(result.getInt(col));
                if (result.getInt(col) != 0) {
                    dt.setMctool(MctoolServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                }
                col++;
                dt.setVersiontaglnk(result.getInt(col));
                if (result.getInt(col) != 0) {
                    dt.setVersiontag(VersiontagServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                }
                col++;
                dt.setBeamlnk(result.getInt(col));
                if (result.getInt(col) != 0) {
                    dt.setBeam(BeamServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                }
                col++;
                dt.setTargetlnk(result.getInt(col));
                if (result.getInt(col) != 0) {
                    dt.setTarget(MaterialServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                }
                col++;
                dt.setObservablelnk(result.getInt(col));
                if (result.getInt(col) != 0) {
                    dt.setObservable(ObservableServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                }
                col++;
                dt.setSecondarylnk(result.getInt(col));
                if (result.getInt(col) != 0) {
                    dt.setSecondary(ParticleServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                }
                col++;
                dt.setReactionlnk(result.getInt(col));
                if (result.getInt(col) != 0) {
                    dt.setReaction(ReactionServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                }
                col++;
                dt.setDatatablelnk(result.getInt(col));
                dt.setDatatable(DatatableAdapter.getById(result.getInt(col)));
                col++;
                dt.setImageblobslnk(result.getInt(col));
                col++;
                dt.setScoreslnk(result.getInt(col));
                dt.setScores(ScoresServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                col++;
                dt.setAccesslnk(result.getInt(col));
                dt.setAccess(AccessServiceAdapterImpl.getInstance().getById(result.getInt(col)));
                col++;
                Array Pnamesarray = result.getArray(col);
                List<String> parnames = new ArrayList();
                if (Pnamesarray != null) {
                    String[] Pnamesar = (String[]) Pnamesarray.getArray();
                    parnames.addAll(Arrays.asList(Pnamesar));
                }
                dt.setParnames(parnames);
                col++;
                Array Pvaluesarray = result.getArray(col);
                List<String> parvalues = new ArrayList();
                if (Pvaluesarray != null) {
                    String[] Pvaluesar = (String[]) Pvaluesarray.getArray();
                    parvalues.addAll(Arrays.asList(Pvaluesar));
                }
                dt.setParvalues(parvalues);
                col++;
                dt.setModtime(result.getTimestamp(col));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Result_expandedServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

}
