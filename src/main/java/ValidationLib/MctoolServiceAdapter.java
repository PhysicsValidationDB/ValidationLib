/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.util.List;

/**
 *
 * @author wenzel
 */
public interface MctoolServiceAdapter {

    /**
     *
     * @param id
     * @return
     */
    public Mctool getById(Integer id);

    /**
     *
     * @return
     */
    public List<Mctool> getAll();

    /**
     *
     * @param mct
     * @return
     */
    public int insert(Mctool mct);

    /**
     *
     * @param mct
     * @return
     */
    public int update(Mctool mct);

    /**
     *
     * @param mct
     * @return
     */
    public int delete(Mctool mct);

}
