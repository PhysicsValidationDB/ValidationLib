/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;

/**
 *
 * @author wenzel
 */
public class Reaction implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private int rid;
    private String rname;

    /**
     *
     */
    private Reaction() {
    }

    public static Reaction create() {
        return new Reaction();
    }

    public static Reaction create(int rid, String rname) {
        return new Reaction(rid, rname);
    }

    public static Reaction create(String jsonstr)  {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Reaction.class);
    }


 
    /**
     *
     * @param rid
     */
    private Reaction(int rid) {
        this.rid = rid;
    }

    /**
     *
     * @param rid
     * @param rname
     */
    private Reaction(int rid, String rname) {
        this.rid = rid;
        this.rname = rname;
    }

    /**
     *
     * @return
     */
    public int getRid() {
        return this.rid;
    }

    /**
     *
     * @param rid
     */
    public void setRid(int rid) {
        this.rid = rid;
    }

    /**
     *
     * @return
     */
    public String getRname() {
        return this.rname;
    }

    /**
     *
     * @param rname
     */
    public void setRname(String rname) {
        this.rname = rname;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }
}
