/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Basic handler for database connection.
 *
 * <p>can be used standalone, or as base class for concrete implementations of
 * a service adapter.
 * </p>
 * <p>
 * For single transaction commits, the connection can be
 * </p>
 * @author schaelic
 */
public class ServiceAdapter {

    Connection connection;
    static Integer connectionCount = 0;
    boolean islocalconnection = true;
    ResultSet result = null;


    /**
     *
     */
    public ServiceAdapter() {
        // this.InitalizeContext();
    }


    /**
     *
     * @param con
     * @throws SQLException
     */
    public void setConnection(Connection con) throws SQLException {
        this.closeConnection();
        this.connection = con;
        this.islocalconnection = false;
    }

    /**
     *
     */
    public void resetConnection() {
        if (!islocalconnection) {
            connection = null;
            islocalconnection = true;
        }
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException {
        connectionCount++;
        if (connection == null) {
            DBConnection DBConnectionInstance = DBConnection.getInstance();
            connection = DBConnectionInstance.getReaderConnection();
            islocalconnection = true;
        }
        return connection;
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    protected Connection getWriterConnection() throws SQLException {
        if (connection == null) {
            DBConnection DBConnectionInstance = DBConnection.getInstance();
            connection = DBConnectionInstance.getWriterConnection();
            islocalconnection = true;
        }
        return connection;
    }

    /**
     *
     */
    public void closeConnection() {
        if (connection != null && islocalconnection) {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ServiceAdapter.class.getName()).log(Level.SEVERE, null, ex);
            }
            connection = null;
        }
    }

    /**
     *
     * @param sql
     * @return
     * @throws SQLException
     */
    protected ResultSet executeQuery(String sql) throws SQLException {
        getConnection();
        Statement select = connection.createStatement();
        result = select.executeQuery(sql);
        return result;
    }

    /**
     *
     * @param sql
     * @return
     * @throws SQLException
     */
    protected PreparedStatement prepareStatement(String sql) throws SQLException {
        getConnection();
        return connection.prepareStatement(sql);
    }

    /**
     *
     * @param sql
     * @return
     * @throws SQLException
     */
    protected PreparedStatement prepareWriteStatement(String sql) throws SQLException {
        getWriterConnection();
        return connection.prepareStatement(sql);
    }

    /**
     * destructor, make sure to close all dangling connections
     * @see #closeConnection()
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        closeConnection(); // close any dangling connections
        super.finalize();
    }
}
