/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class ReferenceServiceAdapterImpl extends ServiceAdapter implements ReferenceServiceAdapter {

    private static ReferenceServiceAdapterImpl instance;

    /**
     *
     */
    private ReferenceServiceAdapterImpl() {
    }

    public static ReferenceServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new ReferenceServiceAdapterImpl();
        }
        return instance;
    }

    /**
     *
     * @param refid
     * @return
     */
    @Override
    public Reference getById(Integer refid) {

        String sql = "SELECT  INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL FROM PUBLIC.REFERENCE WHERE REFID=?;";
        Reference ref = Reference.create();
        try {
            PreparedStatement ps = prepareStatement(sql);
            ps.setInt(1, refid);
            result = ps.executeQuery();
            while (result.next()) {
                ref.setRefid(refid);
                ref.setInspireid(result.getInt(1));
                Array auths = result.getArray(2);
                String aus[] = (String[]) auths.getArray();
                ArrayList authors = new ArrayList<>();
                authors.addAll(Arrays.asList(aus));
                java.util.Collections.sort(authors);
                ref.setAuthors(authors);
                ref.setTitle(result.getString(3));
                ref.setJournal(result.getString(4));
                ref.setErn(result.getString(5));
                ref.setPages(result.getString(6));
                ref.setVolume(result.getString(7));
                ref.setYear(result.getInt(8));
                ref.setAbstract(result.getString(9));
                Array KWarray = result.getArray(10);
                String[] KWar = (String[]) KWarray.getArray();
                ArrayList<String> keywords = new ArrayList();
                for (int i = 1; i < KWar.length; i++) {
                    keywords.add(KWar[i]);
                }
                ref.setKeywords(keywords);
                ref.setLinkurl(result.getString(11));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReferenceServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }

        return ref;
    }

    /**
     *
     * @param inspireid
     * @return
     */
    @Override
    public Reference getByInspireId(Integer inspireid) {

        String sql = "SELECT  REFID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL FROM PUBLIC.REFERENCE WHERE INSPIREID=?;";
        Reference ref = Reference.create();
        try {
            PreparedStatement ps = prepareStatement(sql);
            ps.setInt(1, inspireid);
            result = ps.executeQuery();
            while (result.next()) {
                ref.setInspireid(inspireid);
                ref.setRefid(result.getInt(1));
                Array auths = result.getArray(2);
                String aus[] = (String[]) auths.getArray();
                ArrayList authors = new ArrayList<>();
                authors.addAll(Arrays.asList(aus));
                ref.setAuthors(authors);
                ref.setTitle(result.getString(3));
                ref.setJournal(result.getString(4));
                ref.setErn(result.getString(5));
                ref.setPages(result.getString(6));
                ref.setVolume(result.getString(7));
                ref.setYear(result.getInt(8));
                ref.setAbstract(result.getString(9));
                Array kwds = result.getArray(10);
                if (kwds != null) {
                    String kw[] = (String[]) kwds.getArray();
                    ArrayList keywords = new ArrayList<>();
                    keywords.addAll(Arrays.asList(kw));
                    ref.setKeywords(keywords);
                } else {
                    ref.setKeywords(null);
                }
                ref.setLinkurl(result.getString(11));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReferenceServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }

        return ref;
    }

    /**
     *
     * @param ref
     */
    @Override
    public void insertReference(Reference ref) {
        String sql = "INSERT INTO PUBLIC.REFERENCE ("
                + "INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL)"
                + " VALUES(?,?,?,?,?,?,?,?,?,?,?);";

        try {
            getWriterConnection();
        } catch (SQLException exc) {
            Logger.getLogger(ReferenceServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, exc);
        }
        try {
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, ref.getInspireid());
            List<String> authors = ref.getAuthors();
            String[] auths = (String[]) authors.toArray(new String[authors.size()]);
            Array sqlArray = connection.createArrayOf("varchar", auths);
            ps.setArray(2, sqlArray);
            ps.setString(3, ref.getTitle());
            ps.setString(4, ref.getJournal());
            ps.setString(5, ref.getErn());
            ps.setString(6, ref.getPages());
            ps.setString(7, ref.getVolume());
            ps.setInt(8, ref.getYear());
            ps.setString(9, ref.getAbstract());
            List<String> keywords = ref.getKeywords();
            String[] kwds = (String[]) keywords.toArray(new String[keywords.size()]);
            Array sqlArray2 = connection.createArrayOf("varchar", kwds);
            ps.setArray(10, sqlArray2);
            ps.setString(11, ref.getLinkurl());
            System.out.println(ps.toString());
           // result = ps.executeQuery();
            int update = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReferenceServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
    }

    /**
     *
     * @param ref
     */
    @Override
    public void updateReference(Reference ref) {
        //
    }

    /**
     *
     * @param ref
     */
    @Override
    public void deleteReference(Reference ref) {
        //
    }

    /**
     *
     * @return
     */
    @Override
    public List<Reference> getAll() {
        String sql = "SELECT REFID,INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL FROM PUBLIC.REFERENCE ORDER BY REFID;";
        List<Reference> list = new ArrayList<>();

        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();

            while (result.next()) {
                Reference ref = Reference.create();
                ref.setRefid(result.getInt(1));
                ref.setInspireid(result.getInt(2));
                Array auths = result.getArray(3);
                String aus[] = (String[]) auths.getArray();
                ArrayList authors = new ArrayList<>();
                authors.addAll(Arrays.asList(aus));
                ref.setAuthors(authors);
                ref.setTitle(result.getString(4));
                ref.setJournal(result.getString(5));
                ref.setErn(result.getString(6));
                ref.setPages(result.getString(7));
                ref.setVolume(result.getString(8));
                ref.setYear(result.getInt(9));
                ref.setAbstract(result.getString(10));
                Array kwds = result.getArray(11);
                String kw[] = (String[]) kwds.getArray();
                ArrayList keywords = new ArrayList<>();
                keywords.addAll(Arrays.asList(kw));
                ref.setKeywords(keywords);
                ref.setLinkurl(result.getString(12));
                list.add(ref);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReferenceServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }

        return list;
    }
//
// the following prints out the reference tables where the abstract is missing
// used this tho provide feedback to the inspire team
//

    /**
     *
     */
    public void PrintMissing() {
        String sql = "SELECT LINKURL FROM PUBLIC.REFERENCE WHERE ABSTR=''ORDER BY LINKURL;";
        List<Reference> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                System.out.println(result.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReferenceServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
    }

    @Override
    public Integer NrOfResultSets(Reference refs) {
        ResultServiceAdapterImpl rssa = ResultServiceAdapterImpl.getInstance();
        return rssa.getNrofExpResultsbyRef(refs.getRefid());
    }

}
