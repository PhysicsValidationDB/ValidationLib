/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class VersiontagServiceAdapterImpl extends ServiceAdapter implements VersiontagServiceAdapter {
    private static VersiontagServiceAdapterImpl instance;

    /**
     *
     */
    private VersiontagServiceAdapterImpl() {
    }

    public static VersiontagServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new VersiontagServiceAdapterImpl();
        }
        return instance;
    }
    @Override
    public Versiontag getById(Integer id) {
        String sql = "SELECT TAG FROM PUBLIC.VERSIONTAG WHERE VERSIONID=? ;";
        PreparedStatement ps;
        Versiontag dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Versiontag.create(id, result.getString(1), false);
            }
        } catch (SQLException ex) {
            Logger.getLogger(VersiontagServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Versiontag> getAll() {

        String sql = "SELECT VERSIONID,TAG FROM PUBLIC.VERSIONTAG ORDER BY VERSIONID;";
        List<Versiontag> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                list.add(Versiontag.create(result.getInt(1), result.getString(2), false));
            }
        } catch (SQLException ex) {
            Logger.getLogger(VersiontagServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    @Override
    public int insert(Versiontag dt) {
        int executeUpdate = 0;
        try {
            String sql = "INSERT INTO PUBLIC.VERSIONTAG (TAG) VALUES (?);";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getTag());
            executeUpdate = ps.executeUpdate();            
        } catch (SQLException ex) {
            Logger.getLogger(VersiontagServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int update(Versiontag dt) {
        int executeUpdate = 0;
        try {
            String sql = "UPDATE PUBLIC.VERSIONTAG SET TAG=? WHERE VERSIONID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getTag());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(VersiontagServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int delete(Versiontag dt) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.VERSIONTAG WHERE VERSIONID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getVersionId());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(VersiontagServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

}
