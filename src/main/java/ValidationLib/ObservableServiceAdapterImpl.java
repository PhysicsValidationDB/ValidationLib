/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class ObservableServiceAdapterImpl extends ServiceAdapter implements ObservableServiceAdapter {
    private static ObservableServiceAdapterImpl instance;

    /**
     *
     */
    private ObservableServiceAdapterImpl() {
    }

    public static ObservableServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new ObservableServiceAdapterImpl();
        }
        return instance;
    }
    @Override
    public Observable getById(Integer id) {
        String sql = "SELECT ONAME FROM PUBLIC.OBSERVABLE WHERE OID=? ;";
        PreparedStatement ps;
        Observable dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Observable.create(id, result.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ObservableServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Observable> getAll() {

        String sql = "SELECT OID,ONAME FROM PUBLIC.OBSERVABLE ORDER BY OID;";
        List<Observable> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                list.add(Observable.create(result.getInt(1), result.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ObservableServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    @Override
    public int insert(Observable dt) {
        int executeUpdate = 0;
        try {
            String sql = "INSERT INTO PUBLIC.OBSERVABLE (OID,ONAME) VALUES (?,?);";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getOid());
            ps.setString(2, dt.getOname());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ObservableServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int update(Observable dt) {
        int executeUpdate = 0;
        try {
            String sql = "UPDATE PUBLIC.OBSERVABLE SET ONAME=? WHERE OID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getOname());
            ps.setInt(2, dt.getOid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ObservableServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int delete(Observable dt) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.OBSERVABLE WHERE OID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getOid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ObservableServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

}
