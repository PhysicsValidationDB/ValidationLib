/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

/**
 *
 * @author ncserpico
 */
public class DisplaySearchResult {
    private Integer id;
    private Integer resultCount;
    private String name;
    
    public DisplaySearchResult(Integer id, Integer count, Integer type){
        this.id = id;
        this.resultCount = count;
        if(type == 1){
            TestServiceAdapterImpl tsa = TestServiceAdapterImpl.getInstance();
            this.name = tsa.getById(this.id).getTestname();
        }
        else if(type == 2){
            ReferenceServiceAdapterImpl refsa = ReferenceServiceAdapterImpl.getInstance();
            this.name = refsa.getById(this.id).getTitle();
        }
    }
    
    public void increaseResultCount(){
        this.resultCount++;
    }

    /**
     * @return the resultCount
     */
    public Integer getResultCount() {
        return resultCount;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    
}
