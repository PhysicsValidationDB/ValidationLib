/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;

/**
 * Handler class for all interactions with the imageblogs table of the
 * validation database. Responsible for interactions with corresponding large
 * objects.
 *
 * @author wenzel
 */
public class ImageblobsServiceAdapterImpl extends ServiceAdapter implements ImageblobsServiceAdapter {

    /**
     *
     * @return @deprecated
     */
    @Override
    public List<Integer> getImages() {
        String sql = "select id from public.imageblobs order by id;";
        List<Integer> list = new ArrayList<>();
        try {
            executeQuery(sql);
            while (result.next()) {
                list.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ImageblobsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    /**
     *
     * @return
     */
    @Override
    public Integer getNumberOfImages() {
        String sql = "select count(id) from public.imageblobs;";
        Integer NrofImages = 0;
        try {
            executeQuery(sql);
            while (result.next()) {
                NrofImages = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ImageblobsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return NrofImages;
    }

    /**
     *
     * @param im
     * @return
     */
    @Deprecated
    @Override
    public Integer insertImage(Imageblobs im) {
        Integer id = im.getId();
        try {
            getWriterConnection();
            insertImageBlob(im);
        } catch (SQLException ex) {
            Logger.getLogger(ImageblobsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }

        return id;
    }

    private Integer insertImageBlob(Imageblobs im) throws SQLException {
        Integer id = im.getId();
        long oid = im.getOid();
//        String sql = "insert into imageblobs (image) values (?);";
        String sql = "insert into imageblobs (image) values (?) returning id;";  // note returning statement requires postres 8.2 or above

        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setLong(1, oid);
        result = ps.executeQuery();
//            result= executeQuery("select currval('imageblobs_id_seq')"); // alternative for postgres 8.1
        while (result.next()) {
            id = result.getInt(1);
        }

        return id;
    }

    /**
     *
     * @return @deprecated
     */
    @Deprecated
    @Override
    public Integer getMaxInd() {
        String sql = "select id from public.imageblobs where id=(select max(id)  from imageblobs);";
        Integer MaxInd = 0;
        try {
            executeQuery(sql);
            while (result.next()) {
                MaxInd = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ImageblobsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return MaxInd;
    }
    static final int BUFF_SIZE = 1000000;
    static final byte[] buffer = new byte[BUFF_SIZE];

    /**
     *
     * @param in
     * @param out
     * @throws IOException
     */
    public static void copy(InputStream in, OutputStream out) throws IOException {
        //try {
        int amountRead;
        while ((amountRead = in.read(buffer)) > -1) {
            out.write(buffer, 0, amountRead);
        }
    }

    /**
     *
     * @param in
     * @param out
     * @throws IOException
     */
    public static void copy(InputStream in, InputStream out) throws IOException {
        //try {
        int amountRead;
        while ((amountRead = in.read(buffer)) > -1) {
            out.read(buffer, 0, amountRead);
        }
    }

    /**
     *
     * @param bid
     * @param out
     * @throws SQLException
     * @throws IOException
     */
    @Override
    public void getBlob(Integer bid, OutputStream out) throws SQLException, IOException {
        getConnection();
        String sql = "select image from imageblobs where id=?;";

        try {
            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setInt(1, bid);
            result = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(ImageblobsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        result.first();
        connection.setAutoCommit(false);
        Blob datablob = result.getBlob("image");
        InputStream in = datablob.getBinaryStream();
        copy(in, out);
        connection.setAutoCommit(true);
        closeConnection();
    }

    /**
     *
     * @param bid
     * @return
     * @throws SQLException
     * @throws IOException
     */
    @Override
    public InputStream getIStream(Integer bid) throws SQLException, IOException {
        getConnection();
        String sql = "select image from imageblobs where id=?;";

        try {
            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setInt(1, bid);
            result = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(ImageblobsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        result.first();
        connection.setAutoCommit(false);
        Blob datablob = result.getBlob("image");
        int blobLength = (int) datablob.length();
        byte[] blobAsBytes = datablob.getBytes(1, blobLength);
        //release the blob and free up memory. (since JDBC 4.0)
        datablob.free();
        ByteArrayInputStream instr = new ByteArrayInputStream(blobAsBytes);
        connection.setAutoCommit(true);
        closeConnection();
        return instr;
    }

    /**
     *
     * @param in
     * @return
     * @throws SQLException
     * @throws IOException
     */
    @Override
    public Integer setBlob(InputStream in) throws SQLException, IOException {
        getWriterConnection();
        // All LargeObject API calls must be within a transaction block
        connection.setAutoCommit(false);

        LargeObjectManager lobj = ((org.postgresql.PGConnection) connection).getLargeObjectAPI();
        // Create a new large object
        long oid = lobj.createLO(LargeObjectManager.READ | LargeObjectManager.WRITE);
        // Open the large object for writing
        LargeObject obj = lobj.open(oid, LargeObjectManager.WRITE);

        // Copy the data from the file to the large object
        byte buf[] = new byte[32 * 1024];
        int s;
        while ((s = in.read(buf, 0, 32 * 1024)) > 0) {
            obj.write(buf, 0, s);
        }

        // Close the large object
        obj.close();

        // Now insert the row into imageslo
        Imageblobs im = new Imageblobs();
        im.setOid(oid);
        Integer id = insertImageBlob(im);
        in.close();
        // Finally, commit the transaction.
        connection.commit();
        connection.setAutoCommit(true);

        closeConnection();
        return id;
    }

    /**
     *
     * @param bid
     * @return
     */
    @Override
    public Imageblobs getImage(Integer bid) {
        String sql = "select id,image,mtype,fname from public.imageblobs where id =?;";
        Imageblobs image = null;
        try {
            PreparedStatement ps = prepareStatement(sql);
            ps.setInt(1, bid);
            result = ps.executeQuery();

            while (result.next()) {
                image = new Imageblobs();
                image.setId(result.getInt(1));
                image.setOid(result.getInt(2));
                image.setMimeType(result.getString(3));
                image.setFileName(result.getString(4));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ImageblobsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return image;

    }

    /**
     *
     * @param fname
     * @param tdid
     * @return
     */
    @Override
    public List<Imageblobs> getByFileNameAndTdid(String fname, Integer tdid) {
        String sql = "select b.id,b.image,b.mtype,b.fname from imageblobs b, imagelinks lnk, tests t "
                + "where t.tdid =? and lnk.tid=t.id and lnk.bid=b.id and b.fname=?";
        List<Imageblobs> list = new ArrayList<Imageblobs>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            ps.setInt(1, tdid);
            ps.setString(2, fname);
            result = ps.executeQuery();

            while (result.next()) {
                Imageblobs image = new Imageblobs();
                image.setId(result.getInt(1));
                image.setOid(result.getInt(2));
                image.setMimeType(result.getString(3));
                image.setFileName(result.getString(4));
                list.add(image);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ImageblobsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        if (list.size() > 0) {
            return list;
        }
        return null;

    }

    /**
     *
     * @param fname
     * @param gvers
     * @param tdid
     * @return
     */
    @Override
    public List<Imageblobs> getByFileNameAndVersionAndTdid(String fname, String gvers, Integer tdid) {
        String sql = "select b.id,b.image,b.mtype,b.fname from imageblobs b, imagelinks lnk, tests t "
                + "where t.tdid =? and t.g4version=? and lnk.tid=t.id and lnk.bid=b.id and b.fname=?";
        List<Imageblobs> list = new ArrayList<Imageblobs>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            ps.setInt(1, tdid);
            ps.setString(2, gvers);
            ps.setString(3, fname);
            result = ps.executeQuery();

            while (result.next()) {
                Imageblobs image = new Imageblobs();
                image.setId(result.getInt(1));
                image.setOid(result.getInt(2));
                image.setMimeType(result.getString(3));
                image.setFileName(result.getString(4));
                list.add(image);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ImageblobsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        if (list.size() > 0) {
            return list;
        }
        return null;

    }

    /**
     *
     * @param imageblob
     */
    @Override
    public void delete(Imageblobs imageblob) {
        try {
            String sql = "delete from imageblobs where id=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, imageblob.getId());

            Integer count = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ImageblobsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
    }

     /**
     *
     * @param imageblob
     */
    @Override
    public void delete(Integer imageblobid) {
        try {
            String sql = "delete from imageblobs where id=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, imageblobid);
            Integer count = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ImageblobsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
    }   
    
    /**
     *
     * @param imageblob
     */
    @Override
    public void dropBlob(Imageblobs imageblob) {

        /*
         * CREATE TRIGGER t_image BEFORE UPDATE OR DELETE ON imageblobs
         * FOR EACH ROW EXECUTE PROCEDURE lo_manage(image);
         *
         */
        throw new UnsupportedOperationException("Not supported.");
    }

}
