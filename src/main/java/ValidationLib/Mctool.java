/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;

/**
 *
 * @author wenzel
 */
public class Mctool implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private Integer mcid;
    private String mcname;
    /**
     *
     */
    private Mctool() {
    }

    public static Mctool create() {
        return new Mctool();
    }

    public static Mctool create(Integer mcid, String mcname) {
        return new Mctool(mcid, mcname);
    }

    public static Mctool create(String jsonstr){
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Mctool.class);
    }

    /**
     *
     * @param mcid
     * @param mcname
     */
    private Mctool(Integer mcid, String mcname) {
        this.mcid = mcid;
        this.mcname = mcname;
    }

    /**
     *
     * @return
     */
    public int getMcid() {
        return this.mcid;
    }

    /**
     *
     * @param mcid
     */
    public void setMcid(int mcid) {
        this.mcid = mcid;
    }

    /**
     *
     * @return
     */
    public String getMcname() {
        return this.mcname;
    }

    /**
     *
     * @param mcname
     */
    public void setMcname(String mcname) {
        this.mcname = mcname;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }
}
