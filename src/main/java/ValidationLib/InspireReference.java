/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.io.Serializable;
import java.util.ArrayList;
import java.net.HttpURLConnection;
import java.net.URL;
import nu.xom.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonStructure;

/**
 *
 * @author wenzel
 */
public class InspireReference implements Serializable {

    private static final long serialVersionUID = 1;
    private  Integer recid;
    private List<String> authors;
    private String title;
    private String journal;
    private String ern;//electronic-resource-num
    private String pages;
    private String volume;
    private Integer year;
    private String abstr; //abstract
    private List<String> kwords;
    private String linkurl; //link to corresponding inspire record

    /**
     *
     * @param recordnr
     * @throws Exception
     */
    public InspireReference(Integer recordnr) throws Exception {

        recid = recordnr;
        String url = "https://inspirehep.net/record/" + recordnr + "/export/xe"; // connection to spires API.
        linkurl = "https://inspirehep.net/record/" + recordnr;
        URL obj = new URL(url);
        authors = new ArrayList();
        kwords = new ArrayList();
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        String USER_AGENT = "Mozilla/5.0";
        con.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = con.getResponseCode();
        Document doc = new Builder().build(con.getInputStream());
        Element root = doc.getRootElement();
        Elements elements = doc.getRootElement().getChildElements();
        for (int i = 0; i < elements.size(); i++) {
            Elements records = elements.get(i).getChildElements();
            for (int j = 0; j < records.size(); j++) {
                Elements contributors = records.get(j).getChildElements("contributors");
                for (int ii = 0; ii < contributors.size(); ii++) {
                    Elements auths = contributors.get(ii).getChildElements("authors");
                    for (int n = 0; n < auths.size(); n++) {
                        Elements author = auths.get(n).getChildElements("author");
                        for (int m = 0; m < author.size(); m++) {
                            authors.add(author.get(m).getValue());
                        }
                    }
                }   // end loop over contributors
                Elements titles = records.get(j).getChildElements("titles");
                for (int kk = 0; kk < titles.size(); kk++) {
                    Elements tit = titles.get(kk).getChildElements("title");
                    for (int mm = 0; mm < tit.size(); mm++) {
                        title = tit.get(mm).getValue();
                    }
                    Elements sectit = titles.get(kk).getChildElements("secondary-title");
                    for (int mm = 0; mm < sectit.size(); mm++) {
                        journal = sectit.get(mm).getValue();
                    }
                } // end loop over titles
                Elements ernr = records.get(j).getChildElements("electronic-resource-num");
                for (int mm = 0; mm < ernr.size(); mm++) {
                    ern = ernr.get(mm).getValue();
                }

                Elements pags = records.get(j).getChildElements("pages");
                for (int mm = 0; mm < pags.size(); mm++) {
                    pages = pags.get(mm).getValue();
                }
                Elements vol = records.get(j).getChildElements("volume");
                for (int mm = 0; mm < vol.size(); mm++) {
                    volume = vol.get(mm).getValue();
                }
                Elements dates = records.get(j).getChildElements("dates");
                for (int mm = 0; mm < dates.size(); mm++) {
                    Elements yea = dates.get(mm).getChildElements("year");
                    for (int nn = 0; nn < yea.size(); nn++) {
                        year = Integer.parseInt(yea.get(nn).getValue());
                    }
                } // end loop over dates 
                Elements abst = records.get(j).getChildElements("abstract");
                for (int mm = 0; mm < abst.size(); mm++) {
                    abstr = abst.get(mm).getValue();
                }
            }  // end loop over records 
        }
        if (title == null) {
            System.out.println("title=null");
        }
        if (authors == null) {
            System.out.println("author=null");
        }
        con.disconnect();
//
// the inspire xml web api doesn't provide the keywords so we use the json api for that. 
// The Json API seems a bit buggy so we only use it for the key words (thesaurus_terms)
//        
        String urljson = "https://inspirehep.net/record/" + recordnr + "?of=recjson&ot=thesaurus_terms";
        URL objjson = new URL(urljson);
        HttpURLConnection conjson = (HttpURLConnection) objjson.openConnection();
        conjson.setRequestMethod("GET");
        conjson.setRequestProperty("User-Agent", USER_AGENT);
        responseCode = conjson.getResponseCode();
        if (responseCode == 200) {
            InputStream content = conjson.getInputStream();
            JsonReader jsonReader = Json.createReader(content);
            JsonStructure struct = jsonReader.read();
            if ((struct instanceof JsonObject)) {
                JsonObject obj1 = (JsonObject) struct;
            }
            if ((struct instanceof JsonArray)) {
                JsonArray arr = (JsonArray) struct;
                for (int i = 0; i < arr.size(); i++) {
                    JsonObject obj2 = arr.getJsonObject(i);
                    if (!obj2.isNull("thesaurus_terms")) {
                        JsonArray keywords = obj2.getJsonArray("thesaurus_terms");
                        for (int ii = 0; ii < keywords.size(); ii++) {
                            JsonObject obj3 = keywords.getJsonObject(ii);
                            if (!obj3.toString().contains("{}")) {
                                if (!obj3.isNull("term")) {
                                    String junk = obj3.getString("term");
                                    if (junk != null) {
                                        kwords.add(junk);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public String toString() {

        String returnString = "Inspire record id= " + getRecid() + "\n"
                + "Title=             " + getTitle() + "\n"
                + "Authors=           ";
        for (String author : getAuthors()) {
            returnString = returnString + author + ":";
        }
        returnString = returnString + "\n";
        returnString = returnString
                + "Journal=           " + getSectitle() + "\n"
                + "Volume=            " + getVolume() + "\n"
                + "pages=             " + getPages() + "\n"
                + "year=              " + getYear() + "\n"
                + "DOI (ern)          " + getErn() + "\n"
                + "abstract=          " + getAbstr() + "\n"
                + "Inspire link=      " + getLinkurl() + "\n";
        return returnString;
    }

    /**
     * @return the recid
     */
    public Integer getRecid() {
        return recid;
    }

    /**
     * @return the authors
     */
    public List<String> getAuthors() {
        return authors;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the journal
     */
    public String getSectitle() {
        return getJournal();
    }

    /**
     * @return the ern
     */
    public String getErn() {
        return ern;
    }

    /**
     * @return the pages
     */
    public String getPages() {
        return pages;
    }

    /**
     * @return the volume
     */
    public String getVolume() {
        return volume;
    }

    /**
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /**
     * @return the abstr
     */
    public String getAbstr() {
        return abstr;
    }

    /**
     * @return the linkurl
     */
    public String getLinkurl() {
        return linkurl;
    }

    /**
     * @param recid the recid to set
     */
    public void setRecid(Integer recid) {
        this.recid = recid;
    }

    /**
     * @param authors the authors to set
     */
    public void setAuthors(ArrayList<String> authors) {
        this.authors = authors;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the journal
     */
    public String getJournal() {
        return journal;
    }

    /**
     * @param journal the journal to set
     */
    public void setJournal(String journal) {
        this.journal = journal;
    }

    /**
     * @param ern the ern to set
     */
    public void setErn(String ern) {
        this.ern = ern;
    }

    /**
     * @param pages the pages to set
     */
    public void setPages(String pages) {
        this.pages = pages;
    }

    /**
     * @param volume the volume to set
     */
    public void setVolume(String volume) {
        this.volume = volume;
    }

    /**
     * @param year the year to set
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /**
     * @param abstr the abstr to set
     */
    public void setAbstr(String abstr) {
        this.abstr = abstr;
    }

    /**
     * @param linkurl the linkurl to set
     */
    public void setLinkurl(String linkurl) {
        this.linkurl = linkurl;
    }

    /**
     * @return the keywords
     */
    public List<String> getKeywords() {
        return kwords;
    }

    /**
     * @param keywords the keywords to set
     */
    public void setKeywords(List<String> keywords) {
        this.kwords = keywords;
    }

    // convert InputStream to String
    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

}
