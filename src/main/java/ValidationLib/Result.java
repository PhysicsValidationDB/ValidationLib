/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author wenzel
 */
public class Result implements java.io.Serializable {

    private static long serialVersionUID = 1;

    private transient int trid;
    // links to dictionaries:
    private transient Integer testlnk;
    private transient Integer referencelnk;
    private transient Integer modellnk;
    private transient Integer mctoollnk;
    private transient Integer versiontaglnk;
    private transient Integer beamlnk;
    private transient Integer targetlnk;
    private transient Integer observablelnk;
    private transient Integer secondarylnk;
    private transient Integer reactionlnk;
    private transient Integer scoreslnk;
    private transient Integer accesslnk;
    // links to keywords / names:
    private String testkw;
    private String referencekw;                 
    private String modelkw;
    private String mctoolkw;
    private String versiontagkw;
    private String beamkw;
    private String targetkw;
    private String observablekw;
    private String secondarykw;
    private String reactionkw;
    private String scoreskw;
    private String accesskw;
    //  data: 
    //  we either link to raw data or an image represented by a binary large object (blob) 
    private Datatable datatable;
    private Integer imageblobslnk;
//    
    private List<String> parnames;
    private List<String> parvalues;
    private transient Date modtime;
    //

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }

    public static Result create() {
        return new Result();
    }

    /*
    public static Result create(String jsonstr) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Result.class);
    }
     */
    public static Result create(JsonReader reader) throws IOException {

        Gson gson = new GsonBuilder().create();
        Result result = null;
        try {
            result = gson.fromJson(reader, Result.class);
        } catch (JsonSyntaxException e) {
            System.out.println(e.toString());
        }
        DictionaryService ds = DictionaryService.getInstance();
        if (result.getTestkw() == null && result.getReferencekw() == null) {
            throw new IllegalArgumentException("There reference and test keyword can not both be null");
        }
        if (result.getTestkw() != null) {
            if (ds.getTestMap().containsKey(result.getTestkw())) {
                result.setTestlnk(ds.getTestMap().get(result.getTestkw()));
            } else {
                throw new IllegalArgumentException("There is no Test dictionary entry for key: " + result.getTestkw());
            }
        }
        if (result.getReferencekw() != null) {
            if (ds.getReferenceMap().containsKey(result.getReferencekw())) {
                result.setReferencelnk(ds.getReferenceMap().get(result.getReferencekw()));
            } else {
                throw new IllegalArgumentException("There is no Reference dictionary entry for key: " + result.getReferencekw());
            }
        }
        if (ds.getModelMap().containsKey(result.getModelkw())) {
            result.setModellnk(ds.getModelMap().get(result.getModelkw()));
        } else {
            throw new IllegalArgumentException("There is no Model dictionary entry for key: " + result.getModelkw());
        }
        if (ds.getMctoolMap().containsKey(result.getMctoolkw())) {
            result.setMctoollnk(ds.getMctoolMap().get(result.getMctoolkw()));
        } else {
            throw new IllegalArgumentException("There is no Mctool dictionary entry for key: " + result.getMctoolkw());
        }
        if (ds.getVersiontagMap().containsKey(result.getVersiontagkw())) {
            result.setVersiontaglnk(ds.getVersiontagMap().get(result.getVersiontagkw()));
        } else {
            throw new IllegalArgumentException("There is no Versiontag dictionary entry for key: " + result.getVersiontagkw());
        }
        if (ds.getBeamMap().containsKey(result.getBeamkw())) {
            result.setBeamlnk(ds.getBeamMap().get(result.getBeamkw()));
        } else {
            throw new IllegalArgumentException("There is no Beam dictionary entry for key: " + result.getBeamkw());
        }
        if (ds.getMaterialMap().containsKey(result.getTargetkw())) {
            result.setTargetlnk(ds.getMaterialMap().get(result.getTargetkw()));
        } else {
            throw new IllegalArgumentException("There is no Material dictionary entry for key: " + result.getTargetkw());
        }
        if (ds.getObservableMap().containsKey(result.getObservablekw())) {
            result.setObservablelnk(ds.getObservableMap().get(result.getObservablekw()));
        } else {
            throw new IllegalArgumentException("There is no Observable dictionary entry for key: " + result.getObservablekw());
        }
        if (ds.getParticleMap().containsKey(result.getSecondarykw())) {
            result.setSecondarylnk(ds.getParticleMap().get(result.getSecondarykw()));
        } else {
            throw new IllegalArgumentException("There is no Particle dictionary entry for key: " + result.getSecondarykw());
        }
        if (ds.getReactionMap().containsKey(result.getReactionkw())) {
            result.setReactionlnk(ds.getReactionMap().get(result.getReactionkw()));
        } else {
            throw new IllegalArgumentException("There is no Reaction dictionary entry for key: " + result.getReactionkw());
        }
        if (ds.getScoresMap().containsKey(result.getScoreskw())) {
            result.setScoreslnk(ds.getScoresMap().get(result.getScoreskw()));
        } else {
            throw new IllegalArgumentException("There is no Scores dictionary entry for key: " + result.getScoreskw());
        }
        if (ds.getAccessMap().containsKey(result.getAccesskw())) {
            result.setAccesslnk(ds.getAccessMap().get(result.getAccesskw()));
        } else {
            throw new IllegalArgumentException("There is no Access dictionary entry for key: " + result.getAccesskw());
        }
        //Datatable dt = result.getDatatable();
 //      System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        if (ds.getDatatypesMap().containsKey(result.getDatatable().getDatatypeskw())) {
            result.getDatatable().setDatatypeslnk(ds.getDatatypesMap().get(result.getDatatable().getDatatypeskw()));
        } else {
            throw new IllegalArgumentException("There is no Datatypes dictionary entry for key: " + result.getDatatable().getDatatypeskw());
        }
   //     System.out.println(result.getDatatable().toJSON());
   //      System.out.println(result.getDatatable().getDatatypeslnk());
   //     System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

        //return result;
        /*
        // The following are some sanity checks e.g. that histo limits make sense etc. 
        //
        Datatable dt = result.getDatatable();
        List<Float> binMax = dt.getBinMax();
        List<Float> binMin = dt.getBinMin();
        Iterator<Float> maxIter = binMax.iterator();
        Iterator<Float> minIter = binMin.iterator();
        Float max1 = null;
        if (maxIter.hasNext()) {
            max1 = maxIter.next();
        }
        while (maxIter.hasNext()) {
            Float max2 = maxIter.next();
            if (max2 < max1) {
                check = false;
                break;
            }
            max1 = max2;
        }
        if (check == true) {
            Float min1 = null;
            if (minIter.hasNext()) {
                min1 = minIter.next();
            }
            while (minIter.hasNext()) {
                Float min2 = minIter.next();
                if (min2 < min1) {
                    check = false;
                    break;
                }
                min1 = min2;
            }
        }
         */
        return result;
    }

    /**
     *
     */
    private Result() {
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                //                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }

    /**
     * @return the datatable
     */
    public Datatable getDatatable() {
        return datatable;
    }

    /**
     * @param datatable the datatable to set
     */
    public void setDatatable(Datatable datatable) {
        this.datatable = datatable;
    }

    /**
     * @return the parnames
     */
    public List<String> getParnames() {
        return parnames;
    }

    /**
     * @param parnames the parnames to set
     */
    public void setParnames(List<String> parnames) {
        this.parnames = parnames;
    }

    /**
     * @return the parvalue
     */
    public List<String> getParvalues() {
        return parvalues;
    }

    /**
     * @param parvalue the parvalue to set
     */
    public void setParvalues(List<String> parvalue) {
        this.parvalues = parvalue;
    }

    /**
     * @return the modtime
     */
    public Date getModtime() {
        return modtime;
    }

    /**
     * @param modtime the modtime to set
     */
    public void setModtime(Date modtime) {
        this.modtime = modtime;
    }

    /**
     * @return the testlnk
     */
    public Integer getTestlnk() {
        return testlnk;
    }

    /**
     * @param testlnk the testlnk to set
     */
    public void setTestlnk(Integer testlnk) {
        this.testlnk = testlnk;
    }

    /**
     * @return the referencelnk
     */
    public Integer getReferencelnk() {
        return referencelnk;
    }

    /**
     * @param referencelnk the referencelnk to set
     */
    public void setReferencelnk(Integer referencelnk) {
        this.referencelnk = referencelnk;
    }

    /**
     * @return the beamlnk
     */
    public Integer getBeamlnk() {
        return beamlnk;
    }

    /**
     * @param beamlnk the beamlnk to set
     */
    public void setBeamlnk(Integer beamlnk) {
        this.beamlnk = beamlnk;
    }

    /**
     * @return the targetlnk
     */
    public Integer getTargetlnk() {
        return targetlnk;
    }

    /**
     * @param targetlnk the targetlnk to set
     */
    public void setTargetlnk(Integer targetlnk) {
        this.targetlnk = targetlnk;
    }

    /**
     * @return the observablelnk
     */
    public Integer getObservablelnk() {
        return observablelnk;
    }

    /**
     * @param observablelnk the observablelnk to set
     */
    public void setObservablelnk(Integer observablelnk) {
        this.observablelnk = observablelnk;
    }

    /**
     * @return the secondarylnk
     */
    public Integer getSecondarylnk() {
        return secondarylnk;
    }

    /**
     * @param secondarylnk the secondarylnk to set
     */
    public void setSecondarylnk(Integer secondarylnk) {
        this.secondarylnk = secondarylnk;
    }

    /**
     * @return the reactionlnk
     */
    public Integer getReactionlnk() {
        return reactionlnk;
    }

    /**
     * @param reactionlnk the reactionlnk to set
     */
    public void setReactionlnk(Integer reactionlnk) {
        this.reactionlnk = reactionlnk;
    }

    /**
     * @return the imageblobslnk
     */
    public Integer getImageblobslnk() {
        return imageblobslnk;
    }

    /**
     * @param imageblobslnk the imageblobslnk to set
     */
    public void setImageblobslnk(Integer imageblobslnk) {
        this.imageblobslnk = imageblobslnk;
    }

    /**
     * @return the scoreslnk
     */
    public Integer getScoreslnk() {
        return scoreslnk;
    }

    /**
     * @param scoreslnk the scoreslnk to set
     */
    public void setScoreslnk(Integer scoreslnk) {
        this.scoreslnk = scoreslnk;
    }

    /**
     * @return the accesslnk
     */
    public Integer getAccesslnk() {
        return accesslnk;
    }

    /**
     * @param accesslnk the accesslnk to set
     */
    public void setAccesslnk(Integer accesslnk) {
        this.accesslnk = accesslnk;
    }

    /**
     * @return the trid
     */
    public int getTrid() {
        return trid;
    }

    /**
     * @return the modellnk
     */
    public Integer getModellnk() {
        return modellnk;
    }

    /**
     * @param modellnk the modellnk to set
     */
    public void setModellnk(Integer modellnk) {
        this.modellnk = modellnk;
    }

    /**
     * @return the versiontaglnk
     */
    public Integer getVersiontaglnk() {
        return versiontaglnk;
    }

    /**
     * @param versiontaglnk the versiontaglnk to set
     */
    public void setVersiontaglnk(Integer versiontaglnk) {
        this.versiontaglnk = versiontaglnk;
    }

    /**
     * @param trid the trid to set
     */
    public void setTrid(int trid) {
        this.trid = trid;
    }

    /**
     * @return the beamkw
     */
    public String getBeamkw() {
        return beamkw;
    }

    /**
     * @param beamkw the beamkw to set
     */
    public void setBeamkw(String beamkw) {
        this.beamkw = beamkw;
    }

    /**
     * @return the targetkw
     */
    public String getTargetkw() {
        return targetkw;
    }

    /**
     * @param targetkw the targetkw to set
     */
    public void setTargetkw(String targetkw) {
        this.targetkw = targetkw;
    }

    /**
     * @return the observablekw
     */
    public String getObservablekw() {
        return observablekw;
    }

    /**
     * @param observablekw the observablekw to set
     */
    public void setObservablekw(String observablekw) {
        this.observablekw = observablekw;
    }

    /**
     * @return the secondarykw
     */
    public String getSecondarykw() {
        return secondarykw;
    }

    /**
     * @param secondarykw the secondarykw to set
     */
    public void setSecondarykw(String secondarykw) {
        this.secondarykw = secondarykw;
    }

    /**
     * @return the reactionkw
     */
    public String getReactionkw() {
        return reactionkw;
    }

    /**
     * @param reactionkw the reactionkw to set
     */
    public void setReactionkw(String reactionkw) {
        this.reactionkw = reactionkw;
    }

    /**
     * @return the scoreskw
     */
    public String getScoreskw() {
        return scoreskw;
    }

    /**
     * @param scoreskw the scoreskw to set
     */
    public void setScoreskw(String scoreskw) {
        this.scoreskw = scoreskw;
    }

    /**
     * @return the accesskw
     */
    public String getAccesskw() {
        return accesskw;
    }

    /**
     * @param accesskw the accesskw to set
     */
    public void setAccesskw(String accesskw) {
        this.accesskw = accesskw;
    }

    /**
     * @return the testkw
     */
    public String getTestkw() {
        return testkw;
    }

    /**
     * @param testkw the testkw to set
     */
    public void setTestkw(String testkw) {
        this.testkw = testkw;
    }

    /**
     * @return the modelkw
     */
    public String getModelkw() {
        return modelkw;
    }

    /**
     * @param modelkw the modelkw to set
     */
    public void setModelkw(String modelkw) {
        this.modelkw = modelkw;
    }

    /**
     * @return the versiontagkw
     */
    public String getVersiontagkw() {
        return versiontagkw;
    }

    /**
     * @param versiontagkw the versiontagkw to set
     */
    public void setVersiontagkw(String versiontagkw) {
        this.versiontagkw = versiontagkw;
    }

    /**
     * @return the mctoollnk
     */
    public Integer getMctoollnk() {
        return mctoollnk;
    }

    /**
     * @param mctoollnk the mctoollnk to set
     */
    public void setMctoollnk(Integer mctoollnk) {
        this.mctoollnk = mctoollnk;
    }

    /**
     * @return the mctoolkw
     */
    public String getMctoolkw() {
        return mctoolkw;
    }

    /**
     * @param mctoolkw the mctoolkw to set
     */
    public void setMctoolkw(String mctoolkw) {
        this.mctoolkw = mctoolkw;
    }

    /**
     * @return the referencekw
     */
    public String getReferencekw() {
        return referencekw;
    }

    /**
     * @param referencekw the referencekw to set
     */
    public void setReferencekw(String referencekw) {
        this.referencekw = referencekw;
    }

}
