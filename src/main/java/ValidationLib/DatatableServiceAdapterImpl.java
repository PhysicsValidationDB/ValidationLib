/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.io.IOException;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Table representing the data (e.g. 1D/2d Histograms, 1D/2D Graphs)
 *
 *
 * @author wenzel
 */
public class DatatableServiceAdapterImpl extends ServiceAdapter implements DatatableServiceAdapter {

    private static DatatableServiceAdapterImpl instance;

    private DatatableServiceAdapterImpl() {
    }

    public static DatatableServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new DatatableServiceAdapterImpl();
        }
        return instance;
    }

    @Override
    public Datatable getById(Integer id) {
        DatatypesServiceAdapterImpl DTAdapter = DatatypesServiceAdapterImpl.getInstance();
        String sql = "SELECT "
                + "DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX"
                + " FROM PUBLIC.DATATABLE WHERE DTID=? ;";
        PreparedStatement ps;
        Datatable dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                Integer dtypelnk = result.getInt(1);
                String title = result.getString(2);
                Integer npoints = result.getInt(3);
                Array nbinsarray = result.getArray(4);
                ArrayList<Integer> nbins = new ArrayList();
                if (nbinsarray != null) {
                    Integer[] nbinsar = (Integer[]) nbinsarray.getArray();
                    //ArrayList<Integer> nbins = new ArrayList();
                    for (int i = 0; i < nbinsar.length; i++) {
                        nbins.add(nbinsar[i]);
                    }
                }
                Array ATarray = result.getArray(5);
                String[] ATar = (String[]) ATarray.getArray();
                ArrayList<String> axisTitle = new ArrayList();
                for (int i = 0; i < ATar.length; i++) {
                    axisTitle.add(ATar[i]);
                }
                Array valarray = result.getArray(6);
                Float[] valar = (Float[]) valarray.getArray();
                ArrayList<Float> val = new ArrayList();
                for (int i = 0; i < valar.length; i++) {
                    val.add(valar[i]);
                }
                Array errStatPlusarray = result.getArray(7);
                ArrayList<Float> errStatPlus = new ArrayList();
                if (errStatPlusarray != null) {
                    Float[] errStatPlusar = (Float[]) errStatPlusarray.getArray();
                    if (errStatPlusarray != null) {
                        for (int i = 0; i < errStatPlusar.length; i++) {
                            errStatPlus.add(errStatPlusar[i]);
                        }
                    }
                }
                Array errStatMinusarray = result.getArray(8);
                ArrayList<Float> errStatMinus = new ArrayList();
                if (errStatMinusarray != null) {
                    Float[] errStatMinusar = (Float[]) errStatMinusarray.getArray();
                    for (int i = 0; i < errStatMinusar.length; i++) {
                        errStatMinus.add(errStatMinusar[i]);
                    }
                }
                Array errSysPlusvalarray = result.getArray(9);
                ArrayList<Float> errSysPlus = new ArrayList();
                if (errSysPlusvalarray != null) {
                    Float[] errSysPlusar = (Float[]) errSysPlusvalarray.getArray();
                    for (int i = 0; i < errSysPlusar.length; i++) {
                        errSysPlus.add(errSysPlusar[i]);
                    }
                }
                Array errSysMinusvalarray = result.getArray(10);
                ArrayList<Float> errSysMinus = new ArrayList();
                if (errSysMinusvalarray != null) {
                    Float[] errSysMinusar = (Float[]) errSysMinusvalarray.getArray();

                    for (int i = 0; i < errSysMinusar.length; i++) {
                        errSysMinus.add(errSysMinusar[i]);
                    }
                }
                Array binMinarray = result.getArray(11);
                ArrayList<Float> binMin = new ArrayList();
                if (binMinarray != null) {
                    Float[] binMinar = (Float[]) binMinarray.getArray();
                    for (int i = 0; i < binMinar.length; i++) {
                        binMin.add(binMinar[i]);
                    }
                }
                Array binMaxarray = result.getArray(12);
                ArrayList<Float> binMax = new ArrayList();
                if (binMaxarray != null) {
                    Float[] binMaxar = (Float[]) binMaxarray.getArray();
                    for (int i = 0; i < binMaxar.length; i++) {
                        binMax.add(binMaxar[i]);
                    }
                }
                DictionaryService ds = DictionaryService.getInstance();
                String dtypekw = ds.getDatatypesMap().getKey(dtypelnk);
                dt = Datatable.create(id, dtypelnk, dtypekw, title, npoints, nbins, axisTitle, val, errStatPlus, errStatMinus, errSysPlus, errSysMinus, binMin, binMax);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatatableServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DatatableServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    @Override
    public int update(Datatable dt
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Datatable> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
