/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;
import java.util.List;

/**
 *
 * @author wenzel
 */
public class Material implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private transient int mid;
    private Integer z;
    private String mname;
    private float density;
    private float ion;
    private List<MaterialComponent> Components;

    /**
     *
     */
    private Material() {
    }

    public static Material create() {
        return new Material();
    }

    public static Material create(int mid, Integer z, String mname, float density, float ion) {
        return new Material(mid, z, mname, density, ion);
    }

    public static Material create(int mid, Integer z, String mname, float density, float ion, List<MaterialComponent> Components) {
        return new Material(mid, z, mname, density, ion, Components);
    }

    public static Material create(String jsonstr) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Material.class);
    }

    /**
     *
     * @param mid
     * @param z
     * @param mname
     * @param density
     * @param ion
     */
    private Material(int mid, Integer z, String mname, float density, float ion) {

        this.mid = mid;
        this.z = z;
        this.mname = mname;
        this.density = density;
        this.ion = ion;
    }

    /**
     *
     * @param mid
     * @param z
     * @param mname
     * @param density
     * @param ion
     * @param Components
     */
    private Material(int mid, Integer z, String mname, float density, float ion, List<MaterialComponent> Components) {
        this.mid = mid;
        this.z = z;
        this.mname = mname;
        this.density = density;
        this.ion = ion;
        this.Components = Components;
    }

    /**
     *
     * @return
     */
    public int getMid() {
        return this.mid;
    }

    /**
     *
     * @param mid
     */
    public void setMid(int mid) {
        this.mid = mid;
    }

    /**
     *
     * @return
     */
    public Integer getZ() {
        return this.z;
    }

    /**
     *
     * @param z
     */
    public void setZ(Integer z) {
        this.z = z;
    }

    /**
     *
     * @return
     */
    public String getMname() {
        return this.mname;
    }

    /**
     *
     * @param mname
     */
    public void setMname(String mname) {
        this.mname = mname;
    }

    /**
     *
     * @return
     */
    public float getDensity() {
        return this.density;
    }

    /**
     *
     * @param density
     */
    public void setDensity(float density) {
        this.density = density;
    }

    /**
     *
     * @return
     */
    public float getIon() {
        return this.ion;
    }

    /**
     *
     * @param ion
     */
    public void setIon(float ion) {
        this.ion = ion;
    }

    /**
     * @return the Components
     */
    public List<MaterialComponent> getComponents() {
        return Components;
    }

    /**
     * @param Components the Components to set
     */
    public void setComponents(List<MaterialComponent> Components) {
        this.Components = Components;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }
}
