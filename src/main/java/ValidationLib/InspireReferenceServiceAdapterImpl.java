/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class InspireReferenceServiceAdapterImpl extends ServiceAdapter implements InspireReferenceServiceAdapter {

    private static InspireReferenceServiceAdapterImpl instance;

    /**
     *
     */
    private InspireReferenceServiceAdapterImpl() {
    }

    public static InspireReferenceServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new InspireReferenceServiceAdapterImpl();
        }
        return instance;
    }
    private ArrayList<Integer> list;

    @Override
    public InspireReference getById(Integer id) {
        InspireReference returnref = null;
        try {
            returnref = new InspireReference(id);
        } catch (Exception ex) {
            Logger.getLogger(InspireReferenceServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return returnref;
    }

    @Override
    public ArrayList<Integer> getListofIDs() {
        list = new ArrayList<>();
        try {
            String sql = "select inspireid  from public.inspirereference;";
            result = executeQuery(sql);
            while (result.next()) {
                list.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(InspireReferenceServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    @Override
    public List<InspireReference> getAll() {
        getListofIDs();
        List<InspireReference> listofrefs = new ArrayList<>();
        for (Integer refs : list) {
            try {
                listofrefs.add(new InspireReference(refs));
            } catch (Exception ex) {
                Logger.getLogger(InspireReferenceServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        /*
        // 
        // lambda expressions work for jsk 1.8 and above
        //
        list.stream().forEach((refs) -> {
            try {
                listofrefs.add(new InspireReference(refs));
            } catch (Exception ex) {
                Logger.getLogger(InspireReferenceServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
         */
        return listofrefs;
    }

}
