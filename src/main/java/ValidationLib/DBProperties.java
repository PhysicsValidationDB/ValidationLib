/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */

package ValidationLib;
/* ------------------------
 * DBPropertiesServlet.java
 * ------------------------
 * class that encapsulates the properties needed to connect to the database.
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author wenzel
 */
public class DBProperties {

    private static DBProperties instance = null;
    private static String propertiespath;
    private static String writer_uname;
    private static String writer_passw;
    private static String reader_uname;
    private static String reader_passw;
    private static String dbname;
    private static String portnr;
    private static String server;
    private static String url;

    /**
     *
     * @return
     */
    public static DBProperties getInstance() {
        if (instance == null) {
            throw new NullPointerException();
        }
        return instance;
    }

    /**
     * @return the propertiespath
     */
    public static String getPropertiespath() {
        return propertiespath;
    }

    /**
     * @return the writer_uname
     */
    public static String getWriter_uname() {
        return writer_uname;
    }

    /**
     * @return the writer_passw
     */
    public static String getWriter_passw() {
        return writer_passw;
    }

    /**
     * @return the reader_uname
     */
    public static String getReader_uname() {
        return reader_uname;
    }

    /**
     * @return the reader_passw
     */
    public static String getReader_passw() {
        return reader_passw;
    }

    /**
     * @return the dbname
     */
    public static String getDbname() {
        return dbname;
    }

    /**
     * @return the portnr
     */
    public static String getPortnr() {
        return portnr;
    }

    /**
     * @return the server
     */
    public static String getServer() {
        return server;
    }

    /**
     * @return the url
     */
    public static String getUrl() {
        return url;
    }

    /**
     * Default constructor.
     */
    public DBProperties() {
        // nothing needed
    }

    /*
     * This method get properties from properties file.
     */

    /**
     *
     * @param path
     * @return
     */

    public static DBProperties getProperties(String path) {
        propertiespath = path;
        instance = new DBProperties();
        // Read properties file.
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(getPropertiespath()));
        } catch (IOException e) {
            System.out.println("DBProperties: Could not open properties file");
            System.exit(1);
        }
        writer_uname = properties.getProperty("g4val_writeruname");
        writer_passw = properties.getProperty("g4val_writerpassw");
        reader_uname = properties.getProperty("g4val_readeruname");
        reader_passw = properties.getProperty("g4val_readerpassw");
        dbname = properties.getProperty("g4val_dbname");
        portnr = properties.getProperty("g4val_portnr");
        server = properties.getProperty("g4val_server");
        url = "jdbc:postgresql://" + getServer() + ":" + getPortnr() + "/" + getDbname();
        return getInstance();
    }
}
