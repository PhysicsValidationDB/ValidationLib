/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handler class for all interactions with the score table of the g4validation
 * database.
 *
 * @author wenzel
 */
public class ScoresServiceAdapterImpl extends ServiceAdapter implements ScoresServiceAdapter {
    private static ScoresServiceAdapterImpl instance;
    List<String> validscores;
    List<String> validtesttypes;

    /**
     *
     */
    private ScoresServiceAdapterImpl() {
    }
    /**
     *
     */

    /**
     *
     * @return
     */
    public static ScoresServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new ScoresServiceAdapterImpl();
        }
        return instance;
    }
    @Override
    public Scores getById(Integer id) {
        String sql = "SELECT SCORE,STYPE FROM PUBLIC.SCORES where SCOREID=?;";
        PreparedStatement ps;
        Scores dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Scores.create(id,result.getString(1),result.getString(2));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ScoresServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Scores> getAll() {
       String sql = "SELECT SCOREID,SCORE,STYPE FROM PUBLIC.SCORES ORDER BY SCOREID;";
        List<Scores> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                list.add(Scores.create(result.getInt(1), result.getString(2),result.getString(3)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ScoresServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    @Override
    public Scores getByTid(Integer tid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertScore(Scores score) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> getTestScores() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> getTestTypes() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param tid
     */
    @Override
    public void deleteScoreByTid(Integer tid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateScore(Scores score) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /*
    @Override
    public Scores getById(Integer id) {
        String sql = "select score,testtype,tid from public.scores where id =?;";
        Scores score = null;
        try {
            PreparedStatement ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();

            while (result.next()) {
                score = new Scores();
                score.setId(id);
                score.setScore(result.getString(1));
                score.setTestType(result.getString(2));
                score.setTid(result.getInt(3));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ScoresServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return score;
    }
     */
    /**
     *
     * @param tid
     * @return
     */
    /*
    @Override
    public Scores getByTid(Integer tid) {
        String sql = "select score,testtype,id from public.scores where tid =?;";
        Scores score = null;
        try {
            PreparedStatement ps = prepareStatement(sql);
            ps.setInt(1, tid);
            result = ps.executeQuery();

            while (result.next()) {
                score = new Scores();
                score.setTid(tid);
                score.setScore(result.getString(1));
                score.setTestType(result.getString(2));
                score.setId(result.getInt(3));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ScoresServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return score;
    }
     */
    /**
     *
     * @param score
     */
    /*
    @Override
    public void insertScore(Scores score) {

        String sql = "insert into scores (score,testtype,tid) values(?,?,?)";

        try {
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, score.getScore());
            ps.setString(2, score.getTestType());
            ps.setInt(3, score.getTid());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }

    }

    @Override
    public List<String> getTestScores() {
        List<String> list = new ArrayList<String>();
        try {
            String sql = "select code from testscores;";
            result = executeQuery(sql);

            while (result.next()) {
                list.add(result.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ScoresServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public List<String> getTestTypes() {
        List<String> list = new ArrayList<String>();
        try {
            String sql = "select code from testtypes;";
            result = executeQuery(sql);

            while (result.next()) {
                list.add(result.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ScoresServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public void deleteScoreByTid(Integer tid) {
        try {
            String sql = "delete from scores where tid=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, tid);
            Integer count = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ScoresServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
    }

    @Override
    public void updateScore(Scores score) {
        try {
            String sql = "update scores set score=?,testtype=? where id=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, score.getScore());
            ps.setString(2, score.getTestType());
            ps.setInt(3, score.getId());
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(ScoresServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
    }
     */
}
