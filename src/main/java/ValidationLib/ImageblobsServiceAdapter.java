/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;

/**
 * Abstract handler class for all interactions with the table
 * <code>imageblobs</code> of the database. Including treatment of large
 * objects.
 *
 * @author wenzel
 */
public interface ImageblobsServiceAdapter {

    /**
     *
     * @return @deprecated
     */
    @Deprecated
    public List<Integer> getImages();

    /**
     *
     * @return
     */
    public Integer getNumberOfImages();

    /**
     *
     * @return @deprecated
     */
    @Deprecated
    public Integer getMaxInd();

    /**
     *
     * @param im
     * @return
     */
    @Deprecated
    public Integer insertImage(Imageblobs im);

    /**
     *
     * @param bid
     * @return
     */
    public Imageblobs getImage(Integer bid);

    /**
     *
     * @param fname
     * @param tdid
     * @return
     */
    public List<Imageblobs> getByFileNameAndTdid(String fname, Integer tdid);

    /**
     *
     * @param fname
     * @param version
     * @param tdid
     * @return
     */
    public List<Imageblobs> getByFileNameAndVersionAndTdid(String fname, String version, Integer tdid);

    /**
     *
     * @param bid
     * @param out
     * @throws SQLException
     * @throws IOException
     */
    public void getBlob(Integer bid, OutputStream out) throws SQLException, IOException;

    /**
     *
     * @param bid
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public InputStream getIStream(Integer bid) throws SQLException, IOException;

    /**
     *
     * @param in
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public Integer setBlob(InputStream in) throws SQLException, IOException;

    /**
     *
     * @param imageblob
     */
    public void delete(Imageblobs imageblob);
   /**
     *
     * @param imageblobid
     */
    public void delete(Integer imageblobid);

    /**
     *
     * @param imageblob
     */
    public void dropBlob(Imageblobs imageblob);

}
