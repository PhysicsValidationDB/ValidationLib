/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class BeamServiceAdapterImpl extends ServiceAdapter implements BeamServiceAdapter {

    private static BeamServiceAdapterImpl instance;

    /**
     *
     */
    private BeamServiceAdapterImpl() {
    }

    public static BeamServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new BeamServiceAdapterImpl();
        }
        return instance;
    }

    @Override
    public Beam getById(Integer id) {
        String sql = "SELECT BNAME,REFID,PARTICLEIDS,MEANENERGY,DATATABLEIDS FROM PUBLIC.BEAM WHERE BID=? ;";
        String BeamName;
        Integer RefID;
        PreparedStatement ps;
        Beam dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                BeamName = result.getString(1);
                RefID = result.getInt(2);
                Array pidarray = result.getArray(3);
                ArrayList<Integer> pids = new ArrayList();
                if (pidarray != null) {
                    Integer[] pidsar = (Integer[]) pidarray.getArray();
                    for (int i = 0; i < pidsar.length; i++) {
                        pids.add(pidsar[i]);
                    }
                }
                Array MEarray = result.getArray(4);
                ArrayList<Float> MEs = new ArrayList();
                if (MEarray != null) {
                    Float[] MEsar = (Float[]) MEarray.getArray();
                    for (int i = 0; i < MEsar.length; i++) {
                        MEs.add(MEsar[i]);
                    }
                }
                Array dtarray = result.getArray(5);
                ArrayList<Integer> dts = new ArrayList();
                if (dtarray != null) {
                    Integer[] dtar = (Integer[]) dtarray.getArray();
                    for (int i = 0; i < dtar.length; i++) {
                        dts.add(dtar[i]);
                    }
                }
                dt = Beam.create(id, BeamName, RefID, pids, MEs, dts);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BeamServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Beam> getAll() {
        String sql = "SELECT BID,BNAME,REFID,PARTICLEIDS,MEANENERGY,DATATABLEIDS FROM PUBLIC.BEAM ORDER BY BID;";
        List<Beam> list = new ArrayList<>();
        try {
            Integer Bid;
            String BeamName;
            Integer RefID;
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                Bid= result.getInt(1);
                BeamName = result.getString(2);
                RefID = result.getInt(3);
                Array pidarray = result.getArray(4);
                ArrayList<Integer> pids = new ArrayList();
                if (pidarray != null) {
                    Integer[] pidsar = (Integer[]) pidarray.getArray();
                    pids.addAll(Arrays.asList(pidsar));
                }
                Array MEarray = result.getArray(5);
                ArrayList<Float> MEs = new ArrayList();
                if (MEarray != null) {
                    Float[] MEsar = (Float[]) MEarray.getArray();
                    boolean addAll = MEs.addAll(Arrays.asList(MEsar));
                }
                Array dtarray = result.getArray(6);
                ArrayList<Integer> dts = new ArrayList();
                if (dtarray != null) {
                    Integer[] dtar = (Integer[]) dtarray.getArray();
                    boolean addAll = dts.addAll(Arrays.asList(dtar));
                }
                 list.add(Beam.create(Bid, BeamName, RefID, pids, MEs, dts)); 
            }
        } catch (SQLException ex) {
            Logger.getLogger(BeamServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    @Override
    public int insert(Beam dt) {
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(Beam dt) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.BEAM WHERE BID=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getBid());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BeamServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int update(Beam dt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Integer> getBeamsInRange(
            Integer lowerBound,
            Integer upperBound) {
        List<Integer> list = new ArrayList<>();
        String sql = "SELECT BID FROM PUBLIC.BEAM";
        
        if((upperBound-lowerBound) > 0){
            sql = sql + " WHERE meanenergy[array_lower(meanenergy, 1)] >= ";
            sql = sql + lowerBound;
            sql = sql + " AND meanenergy[array_upper(meanenergy, 1)] <= ";
            sql = sql + upperBound;
        }
       
        sql = sql + ";";
        System.out.println(sql);
        try {
            getConnection();

        } catch (SQLException exc) {
            Logger.getLogger(BeamServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, exc);
        }
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();

        } catch (SQLException ex) {
            Logger.getLogger(BeamServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while (result.next()) {
                list.add(result.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(BeamServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }


}
