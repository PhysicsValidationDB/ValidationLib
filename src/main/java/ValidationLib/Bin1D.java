/*
---------------------------------------------------------------
     ___      ___ ___ _ ___ ___ 
    |   \ ___/ __/ __(_) __| _ \
    | |) / _ \__ \__ \ | _||   /
    |___/\___/___/___/_|___|_|_\
    
    Database of Scientific Simulated and Experimental Results
---------------------------------------------------------------
 */
//
// This class represents 1 bin in a 1D histogram as extracted from the datatable.
//
package ValidationLib;

/**
 *
 * @author wenzel
 */
public class Bin1D {

    private Float binmin;           // min edge of bin
    private Float binmax;           // max edge of bin
    private Float value;            // value
    private Float err_stat_plus;    // upper statistical error 
    private Float err_stat_minus;   // lower statistical error 
    private Float err_sys_plus;     // upper systematical error
    private Float err_sys_minus;    // lower systematical error 

    public Bin1D(Float bmin,
            Float bmax,
            Float val,
            Float err_stat_plus,
            Float err_stat_minus,
            Float err_sys_plus,
            Float err_sys_minus) {
        this.binmin = bmin;
        this.binmax = bmax;
        this.value = val;
        this.err_stat_plus = err_stat_plus;
        this.err_stat_minus = err_stat_minus;
        this.err_sys_plus = err_sys_plus;
        this.err_sys_minus = err_sys_minus;
    }

    /**
     * @return the binmin
     */
    public Float getBinmin() {
        return binmin;
    }

    /**
     * @param binmin the binmin to set
     */
    public void setBinmin(Float binmin) {
        this.binmin = binmin;
    }

    /**
     * @return the binmax
     */
    public Float getBinmax() {
        return binmax;
    }

    /**
     * @param binmax the binmax to set
     */
    public void setBinmax(Float binmax) {
        this.binmax = binmax;
    }

    /**
     * @return the values
     */
    public Float getValue() {
        return value;
    }

    /**
     * @param values the values to set
     */
    public void setValue(Float values) {
        this.value = values;
    }

    /**
     * @return the err_stat_plus
     */
    public Float getErr_stat_plus() {
        return err_stat_plus;
    }

    /**
     * @param err_stat_plus the err_stat_plus to set
     */
    public void setErr_stat_plus(Float err_stat_plus) {
        this.err_stat_plus = err_stat_plus;
    }

    /**
     * @return the err_stat_minus
     */
    public Float getErr_stat_minus() {
        return err_stat_minus;
    }

    /**
     * @param err_stat_minus the err_stat_minus to set
     */
    public void setErr_stat_minus(Float err_stat_minus) {
        this.err_stat_minus = err_stat_minus;
    }

    /**
     * @return the err_sys_plus
     */
    public Float getErr_sys_plus() {
        return err_sys_plus;
    }

    /**
     * @param err_sys_plus the err_sys_plus to set
     */
    public void setErr_sys_plus(Float err_sys_plus) {
        this.err_sys_plus = err_sys_plus;
    }

    /**
     * @return the err_sys_minus
     */
    public Float getErr_sys_minus() {
        return err_sys_minus;
    }

    /**
     * @param err_sys_minus the err_sys_minus to set
     */
    public void setErr_sys_minus(Float err_sys_minus) {
        this.err_sys_minus = err_sys_minus;
    }

}
