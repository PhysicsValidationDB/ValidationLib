/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.util.List;

/**
 * Abstract handler class for all interactions with the score table of the
 * g4validation database.
 *
 * @author wenzel
 */
public interface ScoresServiceAdapter {

    /**
     *
     * @param id
     * @return
     */
    public Scores getById(Integer id);
  /**
     *
     * @return
     */
    public List<Scores> getAll();
    /**
     *
     * @param tid
     * @return
     */
    public Scores getByTid(Integer tid);

    /**
     *
     * @param score
     */
    public void insertScore(Scores score);

    /**
     *
     * @return
     */
    public List<String> getTestScores();

    /**
     *
     * @return
     */
    public List<String> getTestTypes();

    /**
     *
     * @param tid
     */
    public void deleteScoreByTid(Integer tid);

    /**
     *
     * @param score
     */
    public void updateScore(Scores score);
}
