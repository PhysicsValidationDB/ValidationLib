/*
---------------------------------------------------------------
     ___      ___ ___ _ ___ ___ 
    |   \ ___/ __/ __(_) __| _ \
    | |) / _ \__ \__ \ | _||   /
    |___/\___/___/___/_|___|_|_\
    
    Database of Scientific Simulated and Experimental Results
---------------------------------------------------------------
 */
//This class represents 2 dimensional data points (extercated from the corresponding Datatables)
package ValidationLib;

/**
 *
 * @author wenzel
 */
public class DataPoint2D implements java.io.Serializable {

    private Float X;
    private Float Y;
    private Float ErrX;
    private Float ErrY;

    /**
     *
     * @param X
     * @param Y
     * @param ErrX
     * @param ErrY
     */
    public DataPoint2D(Float X, Float Y, Float ErrX, Float ErrY) {
        this.X = X;
        this.Y = Y;
        this.ErrX = ErrX;
        this.ErrY = ErrY;
    }

    public Float getX() {
        return X;
    }

    public void setX(Float X) {
        this.X = X;
    }

    public Float getY() {
        return Y;
    }

    public void setY(Float Y) {
        this.Y = Y;
    }

    public Float getErrX() {
        return ErrX;
    }

    public void setErrX(Float ErrX) {
        this.ErrX = ErrX;
    }

    public Float getErrY() {
        return ErrY;
    }

    public void setErrY(Float ErrY) {
        this.ErrY = ErrY;
    }
}
