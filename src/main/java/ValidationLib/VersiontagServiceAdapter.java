/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.util.List;

/**
 *
 * @author wenzel
 */
public interface VersiontagServiceAdapter {

    /**
     *
     * @param id
     * @return
     */
    public Versiontag getById(Integer id);

    /**
     *
     * @return
     */
    public List<Versiontag> getAll();

    /**
     *
     * @param wg
     * @return
     */
    public int insert(Versiontag wg);

    /**
     *
     * @param wg
     * @return
     */
    public int update(Versiontag wg);

    /**
     *
     * @param wg
     * @return
     */
    public int delete(Versiontag wg);

}
