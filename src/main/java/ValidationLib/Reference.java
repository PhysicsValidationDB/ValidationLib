/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;
import java.util.List;

/**
 * Reference to article retrieved from inspire or web pages we used to extract
 * data
 *
 * <p>
 * can be used standalone, or as base class for concrete implementations of a
 * service adapter.
 * </p>
 * <p>
 * </p> @author wenzel
 */
public class Reference implements Serializable {

    private static final long serialVersionUID = 1;
    private transient Integer refid;
    private Integer inspireid;
    private List<String> authors;
    private String title;
    private String journal;
    private String ern;//electronic-resource-num
    private String pages;
    private String volume;
    private Integer year;
    private String Abstract; //abstract
    private List<String> keywords;
    private String linkurl; //link to corresponding inspire record  
    // or experiment URL in case no inspire record available

    /**
     *
     */
    private Reference() {
    }

    public static Reference create() {
        return new Reference();
    }

    public static Reference create(String jsonstr) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Reference.class);
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the journal
     */
    public String getJournal() {
        return journal;
    }

    /**
     * @param journal the journal to set
     */
    public void setJournal(String journal) {
        this.journal = journal;
    }

    /**
     * @return the ern
     */
    public String getErn() {
        return ern;
    }

    /**
     * @param ern the ern to set
     */
    public void setErn(String ern) {
        this.ern = ern;
    }

    /**
     * @return the pages
     */
    public String getPages() {
        return pages;
    }

    /**
     * @param pages the pages to set
     */
    public void setPages(String pages) {
        this.pages = pages;
    }

    /**
     * @return the volume
     */
    public String getVolume() {
        return volume;
    }

    /**
     * @param volume the volume to set
     */
    public void setVolume(String volume) {
        this.volume = volume;
    }

    /**
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /**
     * @return the Abstract
     */
    public String getAbstract() {
        return Abstract;
    }

    /**
     * @param Abstract the Abstract to set
     */
    public void setAbstract(String Abstract) {
        this.Abstract = Abstract;
    }

    /**
     * @return the linkurl
     */
    public String getLinkurl() {
        return linkurl;
    }

    /**
     * @param linkurl the linkurl to set
     */
    public void setLinkurl(String linkurl) {
        this.linkurl = linkurl;
    }

    /**
     * @return the authors
     */
    public List getAuthors() {
        return authors;
    }

    /**
     * @param authors the authors to set
     */
    public void setAuthors(List authors) {
        this.authors = authors;
    }

    /**
     * @return the keywords
     */
    public List<String> getKeywords() {
        return keywords;
    }

    /**
     * @param keywords the keywords to set
     */
    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    @Override
    public String toString() {

        String returnString = "Inspire record id= " + getRefid() + "\n"
                + "Title=             " + getTitle() + "\n"
                + "Authors=           ";
        for (String auth : authors) {
            returnString = returnString + auth + ":";
        }
        returnString = returnString + "\n";
        returnString = returnString
                + "Journal=           " + getJournal() + "\n"
                + "Volume=            " + getVolume() + "\n"
                + "pages=             " + getPages() + "\n"
                + "year=              " + getYear() + "\n"
                + "DOI (ern)          " + getErn() + "\n"
                + "abstract=          " + getAbstract() + "\n"
                + "Keywords=          ";
        for (String kw : keywords) {
            returnString = returnString + kw + ":";
        }
        returnString = returnString + "Inspire link=      " + getLinkurl() + "\n";
        return returnString;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }

    /**
     * @return the inspireid
     */
    public Integer getInspireid() {
        return inspireid;
    }

    /**
     * @param inspireid the inspireid to set
     */
    public void setInspireid(Integer inspireid) {
        this.inspireid = inspireid;
    }

    /**
     * @return the refid
     */
    public Integer getRefid() {
        return refid;
    }

    /**
     * @param refid the refid to set
     */
    public void setRefid(Integer refid) {
        this.refid = refid;
    }
}
