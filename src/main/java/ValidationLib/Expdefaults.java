/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class Expdefaults implements Serializable {
    private Integer refid;
    private List<Integer> links;

    /**
     * @return the refid
     */
    public Integer getRefid() {
        return refid;
    }

    /**
     * @param refid the refid to set
     */
    public void setRefid(Integer refid) {
        this.refid = refid;
    }

    /**
     * @return the links
     */
    public List<Integer> getLinks() {
        return links;
    }

    /**
     * @param links the links to set
     */
    public void setLinks(List<Integer> links) {
        this.links = links;
    }


}


