/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author wenzel
 */
public interface ResultServiceAdapter {

    /**
     *
     * @param id
     * @param access
     * @return
     */
    public Result getById(Integer id, Integer access);

    /**
     * This method return the ID of all results in the database
     *
     * @return
     */
    public Set<Integer> getAll();

    /**
     *
     * handles the insert of an Entry in the Result table. It first inserts the
     * datatable or imageblobs that it refers to then it adds the entry to the
     * the results table. To make sure this is all done this is done in a
     * transaction that can be rolled back if any of the steps fails. The return
     * value is the TRID of the results inserted to the table or 0 in case the
     * insert failed.
     *
     * @param dt
     * @return
     */
    public int insert(Result dt);

    /**
     *
     * @param dt
     * @return
     */
    public int update(Result dt);

    /**
     * handles the deletion of an Entry in the Result table. It deletes the
     * Result and the datatable or imageblobs that it refers to. To make sure
     * this is all done this is done in a transaction that can be rolled back if
     * any of the steps fails.
     *
     * @param trid
     * @return
     */
    public boolean delete(Integer trid);

    /**
     * returns list of distinct references for experiments ()
     *
     * @return
     */
    public List<Integer> getRefs();

    public Set<Integer> getTests();

    public Integer getTotalExp();

    /**
     *
     * @param access
     * @return
     */
    public Integer getTotalTests(Integer access);

    /**
     * returns Number of resultsets associated with a reference
     *
     * @return
     */
    public Integer getNrofExpResultsbyRef(Integer iref);

    public Integer getNrofResultsbyTest(Integer itest, Integer access);

    //
    // The following methods are used to build the selection menus 
    //
    /**
     *
     * @param expid
     * @return
     */
    public Set<Integer> getBeamsbyExperiment(Integer expid);

    /**
     *
     * @param expid
     * @return
     */
    public Set<Integer> getObservablesbyExperiment(Integer expid);

    /**
     *
     * @param expid
     * @return
     */
    public Set<Integer> getReactionsbyExperiment(Integer expid);

    /**
     *
     * @param expid
     * @return
     */
    public Set<Integer> getSecondariesbyExperiment(Integer expid);

    /**
     *
     * @param expid
     * @return
     */
    public Set<Integer> getTargetsbyExperiment(Integer expid);

    /**
     *
     * @param expid
     * @return
     */
    public HashMap< String, HashSet<String>> getParValPairsbyExperiment(Integer expid);

    public HashMap< String, HashSet<String>> getParValPairs(List<Integer> Selection);
    //
    // The following methods are used to build the selection menus 
    //

    /**
     *
     * @param expid
     * @param access
     * @return
     */
    public Set<Integer> getModelsbyTest(Integer expid, Integer access);

    /**
     *
     * @param expid
     * @param access
     * @return
     */
    public Set<Integer> getMctoolsbyTest(Integer expid, Integer access);

    /**
     *
     * @param expid
     * @param access
     * @return
     */
    public Set<Integer> getVersiontagsbyTest(Integer expid, Integer access);

    /**
     *
     * @param expid
     * @param access
     * @return
     */
    public Set<Integer> getBeamsbyTest(Integer expid, Integer access);

    /**
     *
     * @param expid
     * @param access
     * @return
     */
    public Set<Integer> getAccessbyTest(Integer expid);

    /**
     *
     * @param expid
     * @param access
     * @return
     */
    public Set<Integer> getObservablesbyTest(Integer expid, Integer access);

    /**
     *
     * @param expid
     * @param access
     * @return
     */
    public Set<Integer> getReactionsbyTest(Integer expid, Integer access);

    /**
     *
     * @param expid
     * @param access
     * @return
     */
    public Set<Integer> getSecondariesbyTest(Integer expid, Integer access);

    /**
     *
     * @param expid
     * @param access
     * @return
     */
    public Set<Integer> getTargetsbyTest(Integer expid, Integer access);

    /**
     *
     * @param expid
     * @param access
     * @return
     */
    public HashMap< String, HashSet<String>> getParValPairsbyTest(Integer expid, Integer access);

    /**
     *
     * @param testlnk
     * @param referencelnk
     * @param mcdetaillnk
     * @param beamlnk
     * @param targetlnk
     * @param observablelnk
     * @param secondarylnk
     * @param reactionlnk
     * @return
     * @throws IOException
     */
    public List<Result> getbyMetadata(
            Integer testlnk,
            Integer referencelnk,
            //Integer mcdetaillnk, Mcdetail commented out because the relationship is not 1-to-1.
            Integer modellnk,
            Integer mctoollnk,
            Integer versiontaglnk,
            Integer beamlnk,
            Integer targetlnk,
            Integer observablelnk,
            Integer secondarylnk,
            Integer reactionlnk,
            Integer particlelnk)
            throws IOException;

    /**
     *
     * @param testlnk
     * @param referencelnk
     * @param mcdetaillnk
     * @param beamlnk
     * @param targetlnk
     * @param observablelnk
     * @param secondarylnk
     * @param reactionlnk
     * @param Parname
     * @param ParValue
     * @return
     * @throws IOException
     */
    public List<Result> getbyMetadataTest(
            Integer testlnk,
            Integer referencelnk,
            Integer modellnk,
            Integer mctoollnk,
            Integer versiontaglnk,
            Integer beamlnk,
            Integer targetlnk,
            Integer observablelnk,
            Integer secondarylnk,
            Integer reactionlnk,
            String Parname,
            String ParValue)
            throws IOException;

    public Set<Result> getSelected(
            Integer testlnk,
            Integer referencelnk,
            Set<Integer> selectedmodellnks,
            Set<Integer> selectedmctoollnks,
            Set<Integer> selectedversiontaglnks,
            Set<Integer> selectedbeamlnks,
            Set<Integer> selectedtargetlnks,
            Set<Integer> selectedobservablelnks,
            Set<Integer> selectedsecondarylnks,
            Set<Integer> selectedreactionlnks)
            throws IOException;

    /**
     *
     * @param testlnk
     * @param referencelnk
     * @param selectedmcdetaillnks
     * @param selectedbeamlnks
     * @param selectedtargetlnks
     * @param selectedobservablelnks
     * @param selectedsecondarylnks
     * @param selectedreactionlnks
     * @param selectedparticlelnks
     * @param selectedmctoollnks
     * @param selectedscoreslnks
     * @return
     * @throws IOException
     */
    /*
    public List<Integer> getSelectedlnks(
            Integer testlnk,
            Integer referencelnk,
            Set<Integer> selectedbeamlnks,
            Set<Integer> selectedmcdetaillnks,
            Set<Integer> selectedtargetlnks,
            Set<Integer> selectedobservablelnks,
            Set<Integer> selectedsecondarylnks,
            Set<Integer> selectedreactionlnks,
            Set<Integer> selectedparticlelnks,
            Set<Integer> selectedmctoollnks,
            Set<Integer> selectedscoreslnks
    )
            throws IOException;
     */
    /**
     *
     * @param testlnk
     * @param referencelnk
     * @param selectedmodellnks
     * @param selectedbeamlnks
     * @param selectedversiontaglnks
     * @param selectedtargetlnks
     * @param selectedobservablelnks
     * @param selectedsecondarylnks
     * @param selectedreactionlnks
     * @param selectedparticlelnks
     * @param selectedmctoollnks
     * @param selectedParValuePairs
     * @param selectedscoreslnks
     * @return
     * @throws IOException
     */
    public List<Integer> getSelectedlnks(
            Integer testlnk,
            Integer referencelnk,
            Set<Integer> selectedbeamlnks,
            Set<Integer> selectedmodellnks,
            Set<Integer> selectedmctoollnks,
            Set<Integer> selectedversiontaglnks,
            Set<Integer> selectedtargetlnks,
            Set<Integer> selectedobservablelnks,
            Set<Integer> selectedsecondarylnks,
            Set<Integer> selectedreactionlnks,
            Set<Integer> selectedparticlelnks,
            Set<Integer> selectedscoreslnks,
            HashMap<String, HashSet<String>> selectedParValuePairs
    )
            throws IOException;

    /**
     *
     * @param testlnk
     * @param referencelnk
     * @param selectedaccesslnks
     * @param selectedmodellnks
     * @param selectedbeamlnks
     * @param selectedversiontaglnks
     * @param selectedtargetlnks
     * @param selectedobservablelnks
     * @param selectedsecondarylnks
     * @param selectedreactionlnks
     * @param selectedparticlelnks
     * @param selectedmctoollnks
     * @param selectedParValuePairs
     * @param selectedscoreslnks
     * @return
     * @throws IOException
     */
    public List<Integer> getSelectedlnks1(
            Integer testlnk,
            Integer referencelnk,
            Set<Integer> selectedaccesslnks,
            Set<Integer> selectedbeamlnks,
            Set<Integer> selectedmodellnks,
            Set<Integer> selectedmctoollnks,
            Set<Integer> selectedversiontaglnks,
            Set<Integer> selectedtargetlnks,
            Set<Integer> selectedobservablelnks,
            Set<Integer> selectedsecondarylnks,
            Set<Integer> selectedreactionlnks,
            Set<Integer> selectedparticlelnks,
            Set<Integer> selectedscoreslnks,
            HashMap<String, HashSet<String>> selectedParValuePairs
    )
            throws IOException;
}
