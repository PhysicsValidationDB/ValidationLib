/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;


/* -------------------------------
 * DBConnection.java
 *  @author wenzel
 * -------------------------------
 *
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.Context;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/*
 Class providing a connection to the G4Validation Database
 The class is implemented as a simple singleton.
 The class checks if a connection pool is available and if that is the case
 it connects using the connection pool.
 The connection pool is usually started by the web application server
 (Tomcat or Glassfish)
 */
/**
 *
 * @author wenzel
 */
public class DBConnection {

    /**
     * @return the pooled
     */
    public static Boolean getPooled() {
        return pooled;
    }

    Connection c;
    static DataSource reader_source, writer_source;
    private static DBConnection instance = null;
    private static Boolean pooled = false;

    /**
     * Default constructor.
     */
    protected DBConnection() {
    }

    /*
     * method returns instance of DBConnection if no instance exists yet it will call
     * the protected constructor.
     */
    /**
     *
     * @return
     */
    public static DBConnection getInstance() {
        if (instance == null) {
            instance = new DBConnection();
            try {
                Context context = new InitialContext();
                reader_source = (DataSource) context.lookup("DataSource");
                System.out.println(reader_source.getClass().getName());
                writer_source = (DataSource) context.lookup("WriterSource");
                System.out.println(writer_source.getClass().getName());
                System.out.println("DBConnection getInstance: pooled reader/writer data source available");
                pooled = true;
            } catch (NamingException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.INFO, null, ex);
                System.out.println("DBConnection getInstance: No pooled data source available");
                pooled = false;
            }
        }
        return instance;
    }

    /*
     * method returns a connection to the data base.
     */
    /**
     *
     * @return
     */
    public Connection getReaderConnection() {
        if (pooled) {
            try {
                c = reader_source.getConnection();
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException cnfe) {
                System.err.println(DBConnection.class.getName() + ".getReaderConnection(): Couldn't find driver class:");
            }
            try {
                c = DriverManager.getConnection(DBProperties.getUrl(), DBProperties.getReader_uname(), DBProperties.getReader_passw());
            } catch (SQLException se) {
                System.out.println(DBConnection.class.getName() + ".getReaderConnection(): Couldn't connect: print out a stack trace and exit.");
                System.exit(1);
            }
        }
        return c;
    }

    /**
     *
     * @return
     */
    public Connection getWriterConnection() {
        if (pooled) {
            try {
                c = writer_source.getConnection();
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException cnfe) {
                System.err.println(DBConnection.class.getName() + ".getWriterConnection(): Couldn't find driver class:");
            }
            try {
                System.out.println("DBProperties.getUrl(): " + DBProperties.getUrl());
                c = DriverManager.getConnection(DBProperties.getUrl(), DBProperties.getWriter_uname(), DBProperties.getWriter_passw());
                System.out.println(DBProperties.getUrl());
            } catch (SQLException se) {
                System.out.println(DBConnection.class.getName() + ".getWriterConnection(): Couldn't connect: print out a stack trace and exit.");
                System.exit(1);
            }

            if (c == null) {
                System.out.println("We should never get here.");
                System.exit(1);
            }
        }
        return c;
    }
}
