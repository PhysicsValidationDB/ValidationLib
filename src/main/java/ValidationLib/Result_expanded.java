/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.List;

/**
 *
 * @author wenzel
 */
public class Result_expanded implements java.io.Serializable {

    private static long serialVersionUID = 1;

    private int trid;
    private Integer testlnk;
    private Test test;
    private Integer referencelnk;
    private Reference reference;
    private Integer modellnk;
    private Model model;
    private Integer mctoollnk;
    private Mctool mctool;
    private Integer versiontaglnk;
    private Versiontag versiontag;
    private Integer beamlnk;
    private Beam beam;
    private Integer targetlnk;
    private Material target;
    private Integer observablelnk;
    private Observable observable;
    private Integer secondarylnk;
    private Particle secondary;
    private Integer reactionlnk;
    private Reaction reaction;
    private Integer datatablelnk;
    private Datatable datatable;
    private Integer imageblobslnk;
    private Integer scoreslnk;
    private Scores scores;
    private Integer accesslnk;
    private Access access;
    private List<String> parnames;
    private List<String> parvalues;
    private Date modtime;

    public static Result_expanded create() {
        return new Result_expanded();
    }

    public static Result_expanded create(String jsonstr) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Result_expanded.class);
    }

    /**
     *
     */
    private Result_expanded() {
    }
    //

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {

        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }

    /**
     * @return the datatable
     */
    public Datatable getDatatable() {
        return datatable;
    }

    /**
     * @param datatable the datatable to set
     */
    public void setDatatable(Datatable datatable) {
        this.datatable = datatable;
    }

    /**
     * @return the parnames
     */
    public List<String> getParnames() {
        return parnames;
    }

    /**
     * @param parnames the parnames to set
     */
    public void setParnames(List<String> parnames) {
        this.parnames = parnames;
    }

    /**
     * @return the parvalue
     */
    public List<String> getParvalues() {
        return parvalues;
    }

    /**
     * @param parvalue the parvalue to set
     */
    public void setParvalues(List<String> parvalue) {
        this.parvalues = parvalue;
    }

    /**
     * @return the modtime
     */
    public Date getModtime() {
        return modtime;
    }

    /**
     * @param modtime the modtime to set
     */
    public void setModtime(Date modtime) {
        this.modtime = modtime;
    }

    /**
     * @return the testlnk
     */
    public Integer getTestlnk() {
        return testlnk;
    }

    /**
     * @param testlnk the testlnk to set
     */
    public void setTestlnk(Integer testlnk) {
        this.testlnk = testlnk;
    }

    /**
     * @return the referencelnk
     */
    public Integer getReferencelnk() {
        return referencelnk;
    }

    /**
     * @param referencelnk the referencelnk to set
     */
    public void setReferencelnk(Integer referencelnk) {
        this.referencelnk = referencelnk;
    }

    /**
     * @return the beamlnk
     */
    public Integer getBeamlnk() {
        return beamlnk;
    }

    /**
     * @param beamlnk the beamlnk to set
     */
    public void setBeamlnk(Integer beamlnk) {
        this.beamlnk = beamlnk;
    }

    /**
     * @return the targetlnk
     */
    public Integer getTargetlnk() {
        return targetlnk;
    }

    /**
     * @param targetlnk the targetlnk to set
     */
    public void setTargetlnk(Integer targetlnk) {
        this.targetlnk = targetlnk;
    }

    /**
     * @return the observablelnk
     */
    public Integer getObservablelnk() {
        return observablelnk;
    }

    /**
     * @param observablelnk the observablelnk to set
     */
    public void setObservablelnk(Integer observablelnk) {
        this.observablelnk = observablelnk;
    }

    /**
     * @return the secondarylnk
     */
    public Integer getSecondarylnk() {
        return secondarylnk;
    }

    /**
     * @param secondarylnk the secondarylnk to set
     */
    public void setSecondarylnk(Integer secondarylnk) {
        this.secondarylnk = secondarylnk;
    }

    /**
     * @return the reactionlnk
     */
    public Integer getReactionlnk() {
        return reactionlnk;
    }

    /**
     * @param reactionlnk the reactionlnk to set
     */
    public void setReactionlnk(Integer reactionlnk) {
        this.reactionlnk = reactionlnk;
    }

    /**
     * @return the imageblobslnk
     */
    public Integer getImageblobslnk() {
        return imageblobslnk;
    }

    /**
     * @param imageblobslnk the imageblobslnk to set
     */
    public void setImageblobslnk(Integer imageblobslnk) {
        this.imageblobslnk = imageblobslnk;
    }

    /**
     * @return the scoreslnk
     */
    public Integer getScoreslnk() {
        return scoreslnk;
    }

    /**
     * @param scoreslnk the scoreslnk to set
     */
    public void setScoreslnk(Integer scoreslnk) {
        this.scoreslnk = scoreslnk;
    }

    /**
     * @return the accesslnk
     */
    public Integer getAccesslnk() {
        return accesslnk;
    }

    /**
     * @param accesslnk the accesslnk to set
     */
    public void setAccesslnk(Integer accesslnk) {
        this.accesslnk = accesslnk;
    }

    /**
     * @return the trid
     */
    public int getTrid() {
        return trid;
    }

    /**
     * @param trid the trid to set
     */
    public void setTrid(int trid) {
        this.trid = trid;
    }

    /**
     * @return the test
     */
    public Test getTest() {
        return test;
    }

    /**
     * @param test the test to set
     */
    public void setTest(Test test) {
        this.test = test;
    }

    /**
     * @return the reference
     */
    public Reference getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(Reference reference) {
        this.reference = reference;
    }

    /**
     * @return the beam
     */
    public Beam getBeam() {
        return beam;
    }

    /**
     * @param beam the beam to set
     */
    public void setBeam(Beam beam) {
        this.beam = beam;
    }

    /**
     * @return the target
     */
    public Material getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(Material target) {
        this.target = target;
    }

    /**
     * @return the observable
     */
    public Observable getObservable() {
        return observable;
    }

    /**
     * @param observable the observable to set
     */
    public void setObservable(Observable observable) {
        this.observable = observable;
    }

    /**
     * @return the secondary
     */
    public Particle getSecondary() {
        return secondary;
    }

    /**
     * @param secondary the secondary to set
     */
    public void setSecondary(Particle secondary) {
        this.secondary = secondary;
    }

    /**
     * @return the reaction
     */
    public Reaction getReaction() {
        return reaction;
    }

    /**
     * @param reaction the reaction to set
     */
    public void setReaction(Reaction reaction) {
        this.reaction = reaction;
    }

    /**
     * @return the datatablelnk
     */
    public Integer getDatatablelnk() {
        return datatablelnk;
    }

    /**
     * @param datatablelnk the datatablelnk to set
     */
    public void setDatatablelnk(Integer datatablelnk) {
        this.datatablelnk = datatablelnk;
    }

    /**
     * @return the scores
     */
    public Scores getScores() {
        return scores;
    }

    /**
     * @param scores the scores to set
     */
    public void setScores(Scores scores) {
        this.scores = scores;
    }

    /**
     * @return the access
     */
    public Access getAccess() {
        return access;
    }

    /**
     * @param access the access to set
     */
    public void setAccess(Access access) {
        this.access = access;
    }

    /**
     * @return the model
     */
    public Model getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(Model model) {
        this.model = model;
    }

    /**
     * @return the mctool
     */
    public Mctool getMctool() {
        return mctool;
    }

    /**
     * @param mctool the mctool to set
     */
    public void setMctool(Mctool mctool) {
        this.mctool = mctool;
    }

    /**
     * @return the versiontag
     */
    public Versiontag getVersiontag() {
        return versiontag;
    }

    /**
     * @param versiontag the versiontag to set
     */
    public void setVersiontag(Versiontag versiontag) {
        this.versiontag = versiontag;
    }

    /**
     * @return the modellnk
     */
    public Integer getModellnk() {
        return modellnk;
    }

    /**
     * @param modellnk the modellnk to set
     */
    public void setModellnk(Integer modellnk) {
        this.modellnk = modellnk;
    }

    /**
     * @return the mctoollnk
     */
    public Integer getMctoollnk() {
        return mctoollnk;
    }

    /**
     * @param mctoollnk the mctoollnk to set
     */
    public void setMctoollnk(Integer mctoollnk) {
        this.mctoollnk = mctoollnk;
    }

    /**
     * @return the versiontaglnk
     */
    public Integer getVersiontaglnk() {
        return versiontaglnk;
    }

    /**
     * @param versiontaglnk the versiontaglnk to set
     */
    public void setVersiontaglnk(Integer versiontaglnk) {
        this.versiontaglnk = versiontaglnk;
    }

}
