/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class DatatypesServiceAdapterImpl extends ServiceAdapter implements DatatypesServiceAdapter {
    private static DatatypesServiceAdapterImpl instance;

    /**
     *
     */
    private DatatypesServiceAdapterImpl() {
    }

    public static DatatypesServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new DatatypesServiceAdapterImpl();
        }
        return instance;
    }
    /**
     *
     * @param id
     * @return
     */
    @Override
    public Datatypes getById(Integer id) {
        String sql = "SELECT DESCRIPTION FROM PUBLIC.DATATYPES WHERE DTYPE=? ;";
        PreparedStatement ps;
        Datatypes dt = null;
        try {
            ps = prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeQuery();
            while (result.next()) {
                dt = Datatypes.create(id, result.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatatypesServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return dt;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Datatypes> getAll() {

        String sql = "SELECT DTYPE,DESCRIPTION FROM PUBLIC.DATATYPES ORDER BY DTYPE;";
        List<Datatypes> list = new ArrayList<>();
        try {
            PreparedStatement ps = prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()) {
                list.add(Datatypes.create(result.getInt(1), result.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatatypesServiceAdapterImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return list;
    }

    @Override
    public int insert(Datatypes dt) {
        int executeUpdate = 0;
        try {
            String sql = "INSERT INTO PUBLIC.DATATYPES (DTYPE,DESCRIPTION) VALUES (?,?);";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getDtype());
            ps.setString(2, dt.getDescription());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DatatypesServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int update(Datatypes dt) {
        int executeUpdate = 0;
        try {
            String sql = "UPDATE PUBLIC.DATATYPES SET DESCRIPTION=? WHERE DTYPE=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setString(1, dt.getDescription());
            ps.setInt(2, dt.getDtype());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DatatypesServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

    @Override
    public int delete(Datatypes dt) {
        int executeUpdate = 0;
        try {
            String sql = "DELETE FROM PUBLIC.DATATYPES WHERE DTYPE=?;";
            PreparedStatement ps = prepareWriteStatement(sql);
            ps.setInt(1, dt.getDtype());
            executeUpdate = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DatatypesServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return executeUpdate;
    }

}
