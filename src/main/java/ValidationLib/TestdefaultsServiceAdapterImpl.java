/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenzel
 */
public class TestdefaultsServiceAdapterImpl extends ServiceAdapter implements TestdefaultsServiceAdapter {
    private static TestdefaultsServiceAdapterImpl instance;

    /**
     *
     */
    private TestdefaultsServiceAdapterImpl() {
    }

    public static TestdefaultsServiceAdapterImpl getInstance() {
        if (instance == null) {
            instance = new TestdefaultsServiceAdapterImpl();
        }
        return instance;
    }
    @Override
    public Testdefaults getByTestid(Integer testid) {
        String sql = "SELECT LINKS  FROM PUBLIC.TESTDEFAULTS WHERE TESTID=" + testid + ";";
        try {
            getConnection();
        } catch (SQLException exc) {
            Logger.getLogger(TestdefaultsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, exc);
        }
        Testdefaults datadef = new Testdefaults();
        Statement stmt = null;
        try {
            stmt = connection.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
        } catch (SQLException ex) {
            Logger.getLogger(TestdefaultsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        ResultSet result = null;
        try {
            result = stmt.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(TestdefaultsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while (result.next()) {
                datadef.setTestid(testid);
                Array ar = result.getArray(1);
                Integer[] iar = (Integer[]) ar.getArray();
                List<Integer> setoflinks = new ArrayList<Integer>();
                for (int i = 0; i < iar.length; i++) {
                    setoflinks.add(iar[i]);
                }
                datadef.setLinks(setoflinks);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestdefaultsServiceAdapterImpl.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            closeConnection();
        }
        return datadef;
    }
}
