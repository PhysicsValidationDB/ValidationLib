/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package ValidationLib;

/**
 * This class defines the domain model of the imageblobs table in the
 * validation database.
 *
 * @author wenzel
 */
public class Imageblobs {

    private Integer id;
    private long oid;
    private String mtype;
    private String fname;

    /**
     *
     * @return
     */
    public long getOid() {
        return oid;
    }

    /**
     *
     * @param oid
     */
    public void setOid(long oid) {
        this.oid = oid;
    }

    /**
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getFileName() {
        return fname;
    }

    /**
     *
     * @param fname
     */
    public void setFileName(String fname) {
        this.fname = fname;
    }

    /**
     *
     * @return
     */
    public String getMimeType() {
        return mtype;
    }

    /**
     *
     * @param mtype
     */
    public void setMimeType(String mtype) {
        this.mtype = mtype;
    }

}
