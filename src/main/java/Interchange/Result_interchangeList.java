/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */

 /*

 */
package Interchange;

import ValidationLib.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;
//import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 *
 * @author wenzel
 */
public class Result_interchangeList implements java.io.Serializable {

    

    private List<Result_interchange> ResultList;

    private Result_interchangeList() {
    }
    public static Result_interchangeList create() {
        return new Result_interchangeList();
    }
    public static Result_interchangeList create(String jsonstr) throws IOException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
//        String content2 = readFile(fname, StandardCharsets.UTF_8);
        return gson.fromJson(jsonstr, Result_interchangeList.class);
    }
    

    public static Result_interchangeList create(List<Result_interchange> ResultList) {
  
        return new Result_interchangeList(ResultList);
    }
    
    private Result_interchangeList(List<Result_interchange> ResultList) {
        this.ResultList = ResultList;
    }

    /**
     * @return the Result_interchangeList
     */
    public List<Result_interchange> getResultList() {
        return ResultList;
    }

    /**
     * @param ResultList the ResultList to set
     */
    public void setResult_interchangeList(List<Result_interchange> ResultList) {
        this.ResultList = ResultList;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }
}
