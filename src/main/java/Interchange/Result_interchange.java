/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package Interchange;

import ValidationLib.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;
import java.util.List;

/**
 *
 * @author wenzel
 */
public class Result_interchange implements java.io.Serializable {

    private static long serialVersionUID = 1;

    private reference reference;
    private mcdetail mcdetail;
    private beam beam;
    private String target;
    private observable observable;
    private reaction reaction;
    private Datatable datatable;
    private List<String> parnames;
    private List<String> parvalues;

    public static Result_interchange create() {
        return new Result_interchange();
    }

    public static Result_interchange create(String jsonstr) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, Result_interchange.class);
    }

    /**
     *
     */
    private Result_interchange() {
    }
    //

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {

        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }

    /**
     * @return the datatable
     */
    public Datatable getDatatable() {
        return datatable;
    }

    /**
     * @param datatable the datatable to set
     */
    public void setDatatable(Datatable datatable) {
        this.datatable = datatable;
    }

    /**
     * @return the parnames
     */
    public List<String> getParnames() {
        return parnames;
    }

    /**
     * @param parnames the parnames to set
     */
    public void setParnames(List<String> parnames) {
        this.parnames = parnames;
    }

    /**
     * @return the parvalue
     */
    public List<String> getParvalues() {
        return parvalues;
    }

    /**
     * @param parvalue the parvalue to set
     */
    public void setParvalues(List<String> parvalue) {
        this.parvalues = parvalue;
    }


    /**
     * @return the reference
     */
    public reference getreference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setreference(reference reference) {
        this.reference = reference;
    }

    /**
     * @return the mcdetail
     */
    public mcdetail getmcdetail() {
        return mcdetail;
    }

    /**
     * @param mcdetail the mcdetail to set
     */
    public void setmcdetail(mcdetail mcdetail) {
        this.mcdetail = mcdetail;
    }

    /**
     * @return the beam
     */
    public beam getbeam() {
        return beam;
    }

    /**
     * @param beam the beam to set
     */
    public void setbeam(beam beam) {
        this.beam = beam;
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * @return the observable
     */
    public observable getobservable() {
        return observable;
    }

    /**
     * @param observable the observable to set
     */
    public void setobservable(observable observable) {
        this.observable = observable;
    }

 

    /**
     * @return the reaction
     */
    public reaction getreaction() {
        return reaction;
    }

    /**
     * @param reaction the reaction to set
     */
    public void setreaction(reaction reaction) {
        this.reaction = reaction;
    }


}
