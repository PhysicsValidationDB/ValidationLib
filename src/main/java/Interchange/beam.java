/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package Interchange;

import ValidationLib.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author wenzel
 */
public class beam implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private transient int bid;
    private String Bname;                //name of the Beam or flux file  
    private Integer RefId;               //reference where beam is described
    private List<Integer> ParticleIds;   //Particle Content of Beam (each references PARTICLE(PDGID))
    private List<Float> MeanEnergy;      //kinetic Energy in MeV for each component
    private List<Integer> DataTableIds;  //pointer to histograms with fluxes

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     *
     */
    private beam() {
    }

    public static beam create() {
        return new beam();
    }

    public static beam create(int bid, String BNAME, Integer REFID, List<Integer> PARTICLEIDS, List<Float> MEANENERGY, List<Integer> DATATABLEIDS) {
        return new beam(bid, BNAME, REFID, PARTICLEIDS, MEANENERGY, DATATABLEIDS);
    }

    public static beam create(String jsonstr) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, beam.class);
    }

    /**
     *
     */
    private beam(int bid, String BNAME, Integer REFID, List<Integer> PARTICLEIDS, List<Float> MEANENERGY, List<Integer> DATATABLEIDS) {
        this.bid = bid;
        this.Bname = BNAME;
        this.RefId = REFID;
        this.ParticleIds = PARTICLEIDS;
        this.MeanEnergy = MEANENERGY;
    }

    /**
     *
     * @return
     */
    public int getBid() {
        return this.bid;
    }

    /**
     *
     * @param bid
     */
    public void setBid(int bid) {
        this.bid = bid;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }

    /**
     * @return the Bname
     */
    public String getBname() {
        return Bname;
    }

    /**
     * @param Bname the Bname to set
     */
    public void setBname(String Bname) {
        this.Bname = Bname;
    }

    /**
     * @return the RefId
     */
    public Integer getRefId() {
        return RefId;
    }

    /**
     * @param RefId the RefId to set
     */
    public void setRefId(Integer RefId) {
        this.RefId = RefId;
    }

    /**
     * @return the ParticleIds
     */
    public List<Integer> getParticleIds() {
        return ParticleIds;
    }

    /**
     * @return the ParticleIds
     */
    public List<String> getParticleNames() {

        List<String> pnames = new ArrayList<String>();
        ParticleServiceAdapterImpl psa = ParticleServiceAdapterImpl.getInstance();
        if (ParticleIds.size() != 0) {
            for (int id : ParticleIds) {
                Particle pa = psa.getById(id);
                pnames.add(pa.getPname());
            }
        }
        return pnames;
    }

    /**
     * @param ParticleIds the ParticleIds to set
     */
    public void setParticleIds(List<Integer> ParticleIds) {
        this.ParticleIds = ParticleIds;
    }

    /**
     * @return the MeanEnergy
     */
    public List<Float> getMeanEnergy() {
        return MeanEnergy;
    }

    /**
     * @param MeanEnergy the MeanEnergy to set
     */
    public void setMeanEnergy(List<Float> MeanEnergy) {
        this.MeanEnergy = MeanEnergy;
    }

    /**
     * @return the DataTableIds
     */
    public List<Integer> getDataTableIds() {
        return DataTableIds;
    }

    /**
     * @param DataTableIds the DataTableIds to set
     */
    public void setDataTableIds(List<Integer> DataTableIds) {
        this.DataTableIds = DataTableIds;
    }

}
