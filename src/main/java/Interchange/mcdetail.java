/*
------------------------------------------------------------------------------------
____   ____      .__  .__    .___       __  .__              .____    ._____.    
\   \ /   /____  |  | |__| __| _/____ _/  |_|__| ____   ____ |    |   |__\_ |__  
 \   Y   /\__  \ |  | |  |/ __ |\__  \\   __\  |/  _ \ /    \|    |   |  || __ \ 
  \     /  / __ \|  |_|  / /_/ | / __ \|  | |  (  <_> )   |  \    |___|  || \_\ \
   \___/  (____  /____/__\____ |(____  /__| |__|\____/|___|  /_______ \__||___  /
               \/             \/     \/                    \/        \/       \/ 

Java API to access the validation repository
------------------------------------------------------------------------------------

 */
package Interchange;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;

/**
 *
 * @author wenzel
 */
public class mcdetail implements java.io.Serializable {

    private static final long serialVersionUID = 1;
    private mctool mctool;
    private String versiontag;
    private String model;
    
    private mcdetail() {
    }

    public static mcdetail create() {
        return new mcdetail();
    }

    public static mcdetail create(mctool mctool, String versiontag, String model) {
        return new mcdetail(mctool,versiontag,model);
    }

    public static mcdetail create(String jsonstr) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        return gson.fromJson(jsonstr, mcdetail.class);
    }
   
    /**
     *
     * @param 
     * @param mctool
     * @param versiontag
     * @param model
     */
    private mcdetail(mctool mctool, String versiontag, String model) {
        this.mctool = mctool;
        this.versiontag = versiontag;
        this.model = model;
    }


    /**
     *
     * @return
     */
    public mctool getmctool() {
        return this.mctool;
    }

    /**
     *
     * @param mctool
     */
    public void setmctool(mctool mctool) {
        this.mctool = mctool;
    }

    /**
     *
     * @return
     */
    public String getVersiontag() {
        return this.versiontag;
    }

    /**
     *
     * @param versiontag
     */
    public void setVersiontag(String versiontag) {
        this.versiontag = versiontag;
    }

    /**
     *
     * @return
     */
    public String getModel() {
        return this.model;
    }

    /**
     *
     * @param model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     *
     * @return @throws UnsupportedEncodingException
     */
    public String toXML() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (XMLEncoder e = new XMLEncoder(out)) {
            e.writeObject(this);
        }
        String text = new String(out.toByteArray(), "UTF-8");
        return text;
    }

    /**
     *
     * @return
     */
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                .create();
        String json = gson.toJson(this);
        return json;
    }
}
